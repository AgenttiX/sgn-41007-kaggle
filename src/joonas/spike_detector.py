import scipy.signal
import matplotlib.pyplot as plt
from task1.dataloader import train_data
import numpy as np
from random import randint

def get_peak_heights(data: np.ndarray) -> np.ndarray:
    """ DOES NOT WORK YET"""
    narrowSpikeWidth = [2, 10]
    wideSpikeWidth = [15, 50]
    X = np.empty(data.shape, dtype=data.dtype)
    X[...] = data[...]
    spikeheights = np.zeros((data.shape[0], 14))
    for i in range(data.shape[0]):
        for j in range(12):
            x = X[i, j // 2+4, :]
            print(list(x))
            narrowSpikes, narrowProperties = scipy.signal.find_peaks(x, width=narrowSpikeWidth)

            print("Narrows:", narrowSpikes)
    return spikeheights


def test_get_pikes():
    data = train_data()
    X_train = data.x_data
    testSample = X_train[0, :, :]
    print(testSample)
    fig = plt.figure()
    plt.plot(testSample)
    plt.show()
    F = get_peak_heights(testSample)
    print(F)


def main():
    test_get_pikes()

if __name__ == "__main__":
    main()
