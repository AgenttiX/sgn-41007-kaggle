# from joblib import dump, load
import numpy as np

from sklearn import preprocessing
from sklearn.ensemble import VotingClassifier
from sklearn.ensemble import ExtraTreesClassifier
# from sklearn.model_selection import RandomizedSearchCV
# from sklearn.model_selection import train_test_split
from sklearn.metrics import accuracy_score
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC

# %%
# Load data

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])
        
X = np.abs(np.fft.fft(X, axis=2))


train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
        traincounter += 1
            
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

# %%
# SVM classifier

X = np.reshape(X, (len(y), 10*128))

SVM = SVC(
    C=20.966556655665567,
    gamma='scale',
    tol=0.01175,
    probability=True
).fit(X, y)
print("SVM omalle datalle haku antaa: ", accuracy_score(y, SVM.predict(X)))

puu = ExtraTreesClassifier(
    n_estimators=315,
    min_samples_split=3,
    min_samples_leaf=1,
    max_depth=83,
    n_jobs=-1
).fit(X, y)
print("Puu omalle datalle haku antaa: ", accuracy_score(y, puu.predict(X)))

naapurit = KNeighborsClassifier(
    n_neighbors=4,
    weights='distance',
    n_jobs=-1
).fit(X, y)
print("Puu omalle datalle haku antaa: ", accuracy_score(y, naapurit.predict(X)))

aanestys = VotingClassifier(
    estimators=[('SVM', SVM), ('puu', puu), ('naapurit', naapurit)],
    voting='hard',
    n_jobs=-1
)
aanestys.fit(X, y)    
print("Äänestys antaa: ", accuracy_score(y, aanestys.predict(X)))


# %%
# Test accuracy

X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, 1705):
    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
        
X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis=2))

X_kilpailu_koko = np.reshape(X_kilpailu, (1705, 10*128))

y_kilpailu = aanestys.predict(X_kilpailu_koko)
  
labels = list(le.inverse_transform(y_kilpailu))
with open("T_aanestys_SVM_puu_naapurit_fft.csv", "w") as fp:
    fp.write("# Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))
