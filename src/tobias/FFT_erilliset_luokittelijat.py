import numpy as np
from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from sklearn.ensemble import ExtraTreesClassifier

#%%

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
        
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])

X_va = np.empty((X.shape[0], 6, X.shape[2]))

for i in range(0, X.shape[0]):
    
    X_va[i] = X[i,4:10,:]
            
X_koko_fft = np.abs(np.fft.fft(X_va, axis = 2))

#%%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line    
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
    
groups = []

with open('groups.csv') as file:
    next(file)
    
    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])
        
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

#%% SVM haku

luok = ['W_X_SVM', 'W_Y_SVM', 'W_Z_SVM', 'A_X_SVM', 'A_Y_SVM', 'A_Z_SVM']
luokittelijat = {}
arvot = np.empty((X_koko_fft.shape[0], 9))

for idx, luokka in enumerate(luok):
                  
      data = X_koko_fft[:,idx,:]
    
      gkf = GroupKFold(n_splits=4).split(data, y, groups = groups)
    
      C  = np.geomspace(1e-2,1e2, 10000)
      gamma = ['scale', 'auto']
      probability = [True]
      class_weight = ['balanced']
      kernel = ['rbf', 'sigmoid', 'poly']
      degree = [n for n in range(1, 5)]
      
    
      random_grid= {'C' : C,
                   'gamma' : gamma,
                  'probability' : probability,
                  'class_weight' : class_weight,
                  'kernel' : kernel,
                  'degree' : degree
                   }   

      SVM = SVC()

      malli = RandomizedSearchCV(estimator = SVM, param_distributions = random_grid, n_iter = 10*220*4, cv = gkf, verbose=2,  n_jobs = 6)
      malli.fit(data, y) 

      print( luokka ,"SVM omalle datalle haku antaa: ", accuracy_score(y, malli.predict(data)))
      print(malli.best_params_)
      print(malli.best_score_)
      
      luokittelijat[luokka] = malli.best_estimator_ 
      prob = luokittelijat[luokka].predict_proba(data)
      arvot = arvot + prob
           

#%%

probat = luokittelijat['W_X_SVM'].predict_proba(X_koko_fft[:,0,:])

for i in range(1,6):
    
    tod = luokittelijat[luok[i]].predict_proba(X_koko_fft[:,i,:]) 
    probat  = np.concatenate([probat, tod], axis = 1)
      
#%%

testi = np.argmax(arvot, axis = 1) 
print("Puu_haku omalle datalle antaa: ", accuracy_score(y, testi))

#%%

X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, X_kilpailu.shape[0]):
        
    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])

X_va_k = np.empty((X_kilpailu.shape[0], 6, X_kilpailu.shape[2]))    
    
for i in range(0, X_kilpailu.shape[0]):
    
    X_va_k[i] = X_kilpailu[i,4:10,:]
    
X_koko_fft_k = np.abs(np.fft.fft(X_va_k, axis = 2))    

arvot_k = np.empty((X_koko_fft_k.shape[0], 9))

arvot_k = luokittelijat['W_X_SVM'].predict_proba(X_koko_fft_k[:,0,:])

#%%

for i in range(1,6):
    
    arvot_k = arvot_k + luokittelijat[luok[i]].predict_proba(X_koko_fft_k[:,i,:]) 
    
y_kilpailu = np.argmax(arvot_k, axis = 1)   
                        
labels = list(le.inverse_transform(y_kilpailu))

with open("NFFT_SVM_erikseen_probasumma_max.csv", "w") as fp:
    fp.write("# Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))
  
#%%    
    
    
gkf = GroupKFold(n_splits=4).split(probat, y, groups = groups)

n_estimators  = [n for n in range(5,300,2)]
max_features = ['sqrt', 'log2']
max_depth = [n for n in range(5,300,2)]
min_samples_split = [n for n in range(2,10)]
min_samples_leaf = [n for n in range(1,10)]
kriteeri = ['entropy', 'gini']


random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'criterion': kriteeri
               }  

puu = ExtraTreesClassifier()

puu_haku = RandomizedSearchCV(estimator = puu, param_distributions = random_grid, n_iter = 100*180, cv = gkf, verbose=2,  n_jobs = 6)

puu_haku.fit(probat, y) 

print("Puu_haku omalle datalle antaa: ", accuracy_score(y, puu_haku.predict(probat)))
print(puu_haku.best_params_)
print(puu_haku.best_score_)

paras_puu = puu_haku.best_estimator_

#%%

X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, X_kilpailu.shape[0]):
        
    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])

X_va_k = np.empty((X_kilpailu.shape[0], 6, X_kilpailu.shape[2]))    
    
for i in range(0, X_kilpailu.shape[0]):
    
    X_va_k[i] = X_kilpailu[i,4:10,:]
    
X_koko_fft_k = np.abs(np.fft.fft(X_va_k, axis = 2))    

probat_k = luokittelijat['W_X_SVM'].predict_proba(X_koko_fft_k[:,0,:])

for i in range(1,6):
    
    tod_k = luokittelijat[luok[i]].predict_proba(X_koko_fft_k[:,i,:]) 
    probat_k  = np.concatenate([probat_k, tod_k], axis = 1)
          
y_kilpailu = puu_haku.predict(probat_k)
  
labels = list(le.inverse_transform(y_kilpailu))

with open("NFFT_SVM_erikseen_puu_paattaa.csv", "w") as fp:
    fp.write("# Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))
