from joblib import dump     # , load
import numpy as np

from sklearn import preprocessing
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC


# %%

X = np.load('X_train_kaggle.npy')

X_a = np.empty([len(X), 3, 128])
X_abs = np.empty([len(X), 128])
X_wabs = np.empty([len(X), 128])

for i in range(0, len(X)):
    
    X_a[i,0] = X[i,7,:]
    X_a[i,1] = X[i,8,:]
    X_a[i,2] = X[i,9,:]
            
for i in range(0, len(X)):
            
    X_abs[i] = np.sqrt(X_a[i,0]**2 + X_a[i,1]**2 + X_a[i,2]**2)
    
    
X_abs_fft =  np.abs(np.fft.fft(X_abs, axis=1))

for i in range(0, len(X)):
    
    X_a[i,0] = X[i,4,:]
    X_a[i,1] = X[i,5,:]
    X_a[i,2] = X[i,6,:]
            
for i in range(0, len(X)):
            
    X_wabs[i] = np.sqrt(X_a[i,0]**2 + X_a[i,1]**2 + X_a[i,2]**2)
    
X_wabs_fft = np.abs(np.fft.fft(X_abs, axis=1))

    
# %%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
        traincounter += 1
            
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

# %% X_abs_ff SVM haku

X_train, X_test, y_train, y_test = train_test_split(X_abs_fft, y, test_size=0.1 )

C = np.linspace(1e-1, 42, 1000)
decision_function_shape = ['ovr', 'ovo']
kernel = [ 'rbf']
gamma = ['auto', 'scale']

random_grid = {'C': C,
               'decision_function_shape': decision_function_shape,                              
               'kernel': kernel,               
               'gamma': gamma}
SVM = SVC()

SVM_haku = RandomizedSearchCV(estimator = SVM, param_distributions = random_grid, n_iter = 12345, cv = 6, verbose=2, random_state=42, n_jobs = 6)
SVM_haku.fit(X_train, y_train) 

print("SVM haku antaa abs kiihtyvyydelle: ", accuracy_score(y_test, SVM_haku.predict(X_test)))
print("SVM abs kiihtyvyydelle omalle datalle haku antaa: ", accuracy_score(y_train, SVM_haku.predict(X_train)))
print(SVM_haku.best_params_)
print(SVM_haku.best_score_)

SVM_haku_paras_abs_fft = SVM_haku.best_estimator_

# %% Puuhaku ABS

n_estimators  = [n for n in range(5,325,5)]
max_features = ['sqrt', 'log2']
max_depth = [n for n in range(1, 123)]
min_samples_split = [n for n in range(2,11)]
min_samples_leaf = [n for n in range(1,12)]
bootstrap = [True, False]
kriteeri = ['gini', 'entropy']


random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap,
               'criterion': kriteeri
               }  

puu = ExtraTreesClassifier()

puu_haku_kaikki = RandomizedSearchCV(estimator = puu, param_distributions = random_grid, n_iter = 40*360, cv = 6, verbose=2, random_state=42, n_jobs = 6)

puu_haku_kaikki.fit(X_train, y_train) 

print("Puu_haku antaa: ", accuracy_score(y_test, puu_haku_kaikki.predict(X_test)))
print(puu_haku_kaikki.best_params_)
print(puu_haku_kaikki.best_score_)

puu_haku_paras_ABS = puu_haku_kaikki.best_estimator_

#%% Naapurihaku abs

n_neighbors = [n for n in range(2, 128, 1)]
weights = ['uniform', 'distance']
p = [n for n in range(1,3,1)]

random_grid = {'n_neighbors': n_neighbors,
               'weights': weights,
               'p': p}  

naapurit = KNeighborsClassifier()

naapurit_haku = RandomizedSearchCV(estimator = naapurit, param_distributions = random_grid, n_iter = 100*144, cv = 6, verbose=2, random_state=42, n_jobs = 6)
naapurit_haku.fit(X_train,y_train)

print("naapurit_haku antaa: ", accuracy_score(y_test, naapurit_haku.predict(X_test)))
print(naapurit_haku.best_params_)
print(naapurit_haku.best_score_)

paras_naapurit_abs = naapurit_haku.best_estimator_



#%% X_wabs_fft SVM haku


X_train, X_test, y_train, y_test = train_test_split(X_wabs_fft, y, test_size=0.1 )

C  = np.linspace(1e-1, 42, 1000)
decision_function_shape = ['ovr', 'ovo']
kernel = [ 'rbf']
gamma = ['auto', 'scale']

random_grid = {'C': C,
               'decision_function_shape': decision_function_shape,                             
               'kernel': kernel,               
               'gamma' : gamma}  

SVM = SVC()

SVM_haku = RandomizedSearchCV(estimator = SVM, param_distributions = random_grid, n_iter = 12345, cv = 6, verbose=2, random_state=42, n_jobs = 6)
SVM_haku.fit(X_train, y_train) 

print("SVM haku antaa: ", accuracy_score(y_test, SVM_haku.predict(X_test)))
print("SVM omalle datalle haku antaa: ", accuracy_score(y_train, SVM_haku.predict(X_train)))
print(SVM_haku.best_params_)
print(SVM_haku.best_score_)

SVM_haku_paras_wabs_fft = SVM_haku.best_estimator_

#%% Puuhaku wabs

n_estimators  = [n for n in range(5,125,5)]
max_features = ['sqrt', 'log2']
max_depth = [n for n in range(1, 64)]
min_samples_split = [n for n in range(2,7)]
min_samples_leaf = [n for n in range(1,7)]
bootstrap = [True, False]
kriteeri = ['gini', 'entropy']


random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap,
               'criterion': kriteeri
               }  

puu = ExtraTreesClassifier()

puu_haku_kaikki_wabs = RandomizedSearchCV(estimator = puu, param_distributions = random_grid, n_iter = 40*36, cv = 5, verbose=2, random_state=42, n_jobs = 6)

puu_haku_kaikki_wabs.fit(X_train, y_train) 

print("Puu_haku antaa: ", accuracy_score(y_test, puu_haku_kaikki.predict(X_test)))
print(puu_haku_kaikki.best_params_)
print(puu_haku_kaikki.best_score_)

puu_haku_paras_wabs = puu_haku_kaikki_wabs.best_estimator_

#%%

n_neighbors = [n for n in range(2, 128, 1)]
weights = ['uniform', 'distance']
p = [n for n in range(1,3,1)]

random_grid = {'n_neighbors': n_neighbors,
               'weights': weights,
               'p': p}  

naapurit = KNeighborsClassifier()

naapurit_haku = RandomizedSearchCV(estimator = naapurit, param_distributions = random_grid, n_iter = 10*144, cv = 6, verbose=2, random_state=42, n_jobs = 6)
naapurit_haku.fit(X_train,y_train)

print("naapurit_haku antaa: ", accuracy_score(y_test, naapurit_haku.predict(X_test)))
print(naapurit_haku.best_params_)
print(naapurit_haku.best_score_)

paras_naapurit_wabs = naapurit_haku.best_estimator_

"""
Puu_haku antaa:  0.9590643274853801
{'n_estimators': 275, 'min_samples_split': 7, 'min_samples_leaf': 1, 'max_features': 'sqrt', 'max_depth': 16, 'criterion': 'gini', 'bootstrap': False}
0.5953002610966057
Aika erikoista? Voisi testata tätä ehkä, submissioneita kuitenkin on monta.
"""

# %%

dump(puu_haku_kaikki_wabs, 'T_puu_haku_wabs_fft.joblib') 

# %%

X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, 1703):
    
    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
        
X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis = 2))

X_kilpailu_koko = np.reshape(X_kilpailu,(1705, 10*128))

y_kilpailu = puu_haku_kaikki.predict(X_kilpailu_koko)
labels = list(le.inverse_transform(y_kilpailu))

with open("T_puu_haku_fft.csv", "w") as fp:
    fp.write("# Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))
