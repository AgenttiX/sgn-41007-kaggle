import pickle

from joblib import dump
import numpy as np

from sklearn import preprocessing
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.preprocessing import LabelEncoder

# %%

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
    
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])
        
X = np.abs(np.fft.fft(X, axis=2))

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
        traincounter += 1
            
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)


# %% Puu parametrien haku
      
n_estimators = [n for n in range(5, 325, 5)]
max_features = ['sqrt', 'log2']
max_depth = [n for n in range(1, 123)]
min_samples_split = [n for n in range(2, 11)]
min_samples_leaf = [n for n in range(1, 12)]
bootstrap = [True, False]
kriteeri = ['gini', 'entropy']


random_grid = {
    'n_estimators': n_estimators,
    'max_features': max_features,
    'max_depth': max_depth,
    'min_samples_split': min_samples_split,
    'min_samples_leaf': min_samples_leaf,
    'bootstrap': bootstrap,
    'criterion': kriteeri
}

puu = ExtraTreesClassifier()

puu_haku_kaikki = RandomizedSearchCV(
    estimator=puu,
    param_distributions=random_grid,
    n_iter=40000,
    cv=5,
    verbose=2,
    random_state=42,
    n_jobs=6        # Should this be changed to -1?
)

X_kaikki = np.reshape(X, (len(y), 10*128))

puu_haku_kaikki.fit(X_kaikki, y) 

print("Puu_haku antaa: ", accuracy_score(y, puu_haku_kaikki.predict(X_kaikki)))
print(puu_haku_kaikki.best_params_)
print(puu_haku_kaikki.best_estimator_)
print(puu_haku_kaikki.best_score_)

# %% Tallennetaan haettu malli

tallennus = pickle.dumps(puu_haku_kaikki)
dump(puu_haku_kaikki, 'T_puu_haku_fft.joblib') 

# %%
X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, 1703):
    
    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
        
X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis=2))

X_kilpailu_koko = np.reshape(X_kilpailu, (1705, 10*128))

y_kilpailu = puu_haku_kaikki.predict(X_kilpailu_koko)
labels = list(le.inverse_transform(y_kilpailu))

with open("T_puu_haku_fft.csv", "w") as fp:
    fp.write("# Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))
