import numpy as np
import time

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
from tensorflow.keras import backend
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import StratifiedKFold

#%%

def create_network( alustus = 'zero', droppi = 0.5, neuroneja = 32, neurotasot = 1, optimoija = 'sgd', tasokerroin=1, aktivointi='relu',konv_neuroneita=32, konv_aktivointi = 'relu', konv_tasokerroin =2 , konv_tasot = 3):

    backend.clear_session()

    verkko = models.Sequential()

    verkko.add(layers.Conv2D(filters=konv_neuroneita, kernel_size = (3,3), activation = konv_aktivointi, input_shape = (10, 128, 1), padding = 'same' , kernel_initializer=alustus))    
    verkko.add(layers.MaxPooling2D(pool_size = (2,2)))
    

    uudet_konv_neuronit = int(konv_neuroneita * konv_tasokerroin)
    
    for i in range(1, konv_tasot):
        
        
        verkko.add(layers.Conv2D(filters = uudet_konv_neuronit, kernel_size = (3,3), activation = konv_aktivointi, padding = 'same',kernel_initializer=alustus))
        verkko.add(layers.MaxPooling2D(pool_size=(2, 2)))
        
        
        uudet_konv_neuronit = int(uudet_konv_neuronit * konv_tasokerroin)
        

    verkko.add(layers.Flatten())

    verkko.add(layers.Dense(units = neuroneja, activation = aktivointi,kernel_initializer=alustus))
    verkko.add(layers.Dropout(rate = droppi))
    

    uudet_neuronit = int(neuroneja * tasokerroin)

    for i in range(1, neurotasot):

        verkko.add(layers.Dense(units = uudet_neuronit, activation = aktivointi,kernel_initializer=alustus))
        verkko.add(layers.Dropout(rate = droppi))
        

        uudet_neuronit = int(uudet_neuronit * tasokerroin)

    verkko.add(layers.Dense(9, activation='softmax',kernel_initializer=alustus))
    
    verkko.compile(metrics=['accuracy'], loss='categorical_crossentropy', optimizer=optimoija)

    return verkko

#%%
    

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):

    X[n] = preprocessing.StandardScaler().fit_transform(X[n])

X = np.reshape(X, [X.shape[0], X.shape[1],X.shape[2], 1] )

#%%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line

    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])

groups = []

with open('groups_oma.csv') as file:
    next(file)

    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])

le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

gkf = GroupKFold(n_splits=2).split(X, y, groups)
y = to_categorical(y)

#%%

droppi = np.linspace(0.4, 0.8, 5)
neuroneja =   [n for n in range(1,129)]
neurotasot = np.arange(1,3)
optimoija = ['Adam', 'RMSprop', 'Nadam'] 
#tasokerroin = np.linspace(0.1, 1, 10)
aktivointi =  [ 'sigmoid', 'hard_sigmoid', 'relu']
#konv_tasokerroin = np.linspace(2,3,10)
konv_neuroneita = [n for n in range(1, 65)] 
# konv_tasot = np.arange(3,4)
alustus = ['lecun_uniform', 'glorot_normal', 'glorot_uniform', 'lecun_normal']

random_grid = {'droppi': droppi,
               'neuroneja': neuroneja,
               'neurotasot': neurotasot,
               'optimoija': optimoija,
               #'tasokerroin': tasokerroin,
               'aktivointi': aktivointi,
               'konv_neuroneita' : konv_neuroneita,
               #'konv_tasot' : konv_tasot,
               #'konv_tasokerroin' : konv_tasokerroin,
               'alustus' : alustus               
               }

#%%

neuroverkko = KerasClassifier(build_fn=create_network, epochs=200, verbose=0, batch_size=64)

neurohaku = RandomizedSearchCV(estimator=neuroverkko, param_distributions=random_grid, n_iter = 2542, cv=gkf, verbose=100000)

t = time.time()

stop = EarlyStopping(monitor='acc', patience=20, min_delta=0.01, verbose=2, mode='max')

neurohaku.fit(X, y, callbacks=[stop])

print("PARAS TULOS: ", neurohaku.best_score_, " PARAMETREILLA :", neurohaku.best_params_)

means = neurohaku.cv_results_['mean_test_score']
stds = neurohaku.cv_results_['std_test_score']
params = neurohaku.cv_results_['params']

zippi = zip(means, stds, params)
zipped = sorted(zippi, key=lambda x: x[0])

for mean, stdev, param in zipped:
    print("%f (%f) parametreilla: %r" % (mean, stdev, param))

print("Aikaa meni: ", (time.time() - t) / 60, " min")

#%%

datagen = ImageDataGenerator(width_shift_range = 0.5, fill_mode = 'reflect', horizontal_flip = True)

paras = ModelCheckpoint('paras_raaka.hdf5', monitor = 'val_acc', verbose = 1, save_best_only = True, mode = 'max', save_weights_only = True)


X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):

    X[n] = preprocessing.StandardScaler().fit_transform(X[n])
    
X = np.reshape(X, [X.shape[0], X.shape[1],X.shape[2], 1] )    

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)

testi_parametrit = {'optimoija': 'Adam', 'neurotasot': 1, 'neuroneja': 31, 'konv_neuroneita': 45, 'alustus': 'glorot_normal', 'aktivointi': 'hard_sigmoid'} 

#paras_neuroverkko = create_network(**neurohaku.best_params_)

paras_neuroverkko = create_network(**testi_parametrit)

#%%

historia = paras_neuroverkko.fit_generator(datagen.flow(X_train, y_train, batch_size = 64), 
                                           steps_per_epoch = int(np.ceil(X_train.shape[0] / float(64))),                                                                                   
                                           epochs=500, validation_data=(X_test, y_test), callbacks = [paras], verbose = 0)

plt.figure(1)
plt.figure(figsize=(20,10))
plt.plot(historia.history['acc'])
plt.plot(historia.history['val_acc'])
plt.title('Mallin tarkkuus raaka data ')
plt.ylabel('Tarkkuus')
plt.xlabel('aikakausi')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

plt.figure(2)
plt.figure(figsize=(20,10))
plt.plot(historia.history['loss'])
plt.plot(historia.history['val_loss'])
plt.title('Mallin häviö raaka data')
plt.ylabel('Häviö')
plt.xlabel('aikakausi')
plt.legend(['train', 'test'], loc='upper left')
plt.show()

#%%

X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, X_kilpailu.shape[0]):
    
    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
    
X_kilpailu = np.reshape(X_kilpailu, [X_kilpailu.shape[0], X_kilpailu.shape[1], X_kilpailu.shape[2], 1])

kilpaverkko = create_network(**testi_parametrit)

datagen = ImageDataGenerator(width_shift_range = 0.5, fill_mode = 'nearest', horizontal_flip = True)

kilpahistoria = kilpaverkko.fit_generator(datagen.flow(X, y, batch_size = 64), 
                                           steps_per_epoch = int(np.ceil(X_train.shape[0] / float(64))),                                                                                   
                                           epochs=400)
plt.figure(3)
plt.figure(figsize=(20,10))
plt.plot(kilpahistoria.history['acc'])
plt.title('Kilpaverkon tarkkuus')
plt.ylabel('Tarkkuus')
plt.xlabel('aikakausi')
plt.show()

plt.figure(4)
plt.figure(figsize=(20,10))
plt.plot(kilpahistoria.history['loss'])
plt.title('Kilpaverkon häviö')
plt.ylabel('Häviö')
plt.xlabel('aikakausi')
plt.show()

#%%

y_kilpailu_raaka = kilpaverkko.predict(X_kilpailu)
y_kilpailu_tulos = np.argmax(y_kilpailu_raaka, axis = 1)

labels = list(le.inverse_transform(y_kilpailu_tulos))

with open("raaka_konvoluutiot.csv", "w") as fp:
    
    fp.write("# Id,Surface\n")
             
    for i, label in enumerate(labels):
        
        fp.write("%d,%s\n" % (i, label))
  








