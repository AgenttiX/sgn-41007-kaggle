# from joblib import dump, load
import numpy as np

from sklearn import preprocessing
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import GroupKFold
from sklearn.model_selection import RandomizedSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
# from sklearn.ensemble import VotingClassifier

# %%
# Load data

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
    
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])
        
X = np.abs(np.fft.fft(X, axis = 2))

X = np.reshape(X, (X.shape[0], 10*128))

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
    
groups = []

with open('groups.csv') as file:
    next(file)
    
    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])
        
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

# %%
# SVM (SVC)

gkf = GroupKFold(n_splits=3).split(X, y, groups = groups)


C = np.linspace(1, 42, 1000)
gamma = ['scale', 'auto']

random_grid = {'C' : C,
               'gamma' : gamma}  
SVM = SVC()

SVM_haku = RandomizedSearchCV(estimator=SVM, param_distributions = random_grid, n_iter = 10*60*3, cv = gkf, verbose=2, random_state=42, n_jobs = 6)
SVM_haku.fit(X, y) 

print("SVM omalle datalle haku antaa: ", accuracy_score(y, SVM_haku.predict(X)))
print(SVM_haku.best_params_)
print(SVM_haku.best_score_)

paras_SVM = SVM_haku.best_estimator_ 


# %%
# Puuhaku (ExtraTreesClassifier)

gkf = GroupKFold(n_splits=3).split(X, y, groups = groups)

n_estimators  = [n for n in range(5,200,5)]
max_features = ['sqrt']
max_depth = [100]
min_samples_split = [n for n in range(2,10)]
min_samples_leaf = [n for n in range(1,3)]
bootstrap = [False]
kriteeri = ['gini', 'entropy']


random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap,
               'criterion': kriteeri
               }  

puu = ExtraTreesClassifier()

puu_haku = RandomizedSearchCV(estimator = puu, param_distributions = random_grid, n_iter = 10*450, cv = gkf, verbose=2, random_state=42, n_jobs = 6)

puu_haku.fit(X, y) 

print("Puu_haku omalle datalle antaa: ", accuracy_score(y, puu_haku.predict(X)))
print(puu_haku.best_params_)
print(puu_haku.best_score_)

paras_puu = puu_haku.best_estimator_

# %%
# KNeighborsClassifier

gkf = GroupKFold(n_splits=3).split(X, y, groups = groups)

n_neighbors = [n for n in range(2, 128, 1)]
weights = ['uniform', 'distance']
p = [n for n in range(1,3,1)]

random_grid = {'n_neighbors': n_neighbors,
               'weights': weights,
               'p': p}  

naapurit = KNeighborsClassifier()

naapurit_haku = RandomizedSearchCV(estimator = naapurit, param_distributions = random_grid, n_iter = 10*102, cv = gkf, verbose=2, random_state=42, n_jobs = 6)
naapurit_haku.fit(X,y)

print("Naapurit omalle datalle antaa: ", accuracy_score(y, naapurit_haku.predict(X)))
print(naapurit_haku.best_params_)
print(naapurit_haku.best_score_)

paras_naapurit = naapurit_haku.best_estimator_


# %%

# X_kilpailu = np.load('X_test_kaggle.npy')
#
# for n in range(0, 1705):
#
#    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
#
# X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis = 2))
#
# X_kilpailu_koko = np.reshape(X_kilpailu,(1705, 10*128))
#
# y_kilpailu = SVM_haku.predict(X_kilpailu_koko)
#
# labels = list(le.inverse_transform(y_kilpailu))
# with open("T_SVM_haku_fft_v2.csv", "w") as fp:
#    fp.write("# Id,Surface\n")
#    for i, label in enumerate(labels):
#        fp.write("%d,%s\n" % (i, label))
