import time

# from joblib import dump, load
import numpy as np

from sklearn import preprocessing
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RandomizedSearchCV
# from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC


# %%

X = np.load('X_train_kaggle.npy')

X_uusi = np.empty([len(X), 8, 128])

for i in range(0, len(X)):
    
    X_uusi[i,0] = X[i,7,:]
    X_uusi[i,1] = X[i,8,:]
    X_uusi[i,2] = X[i,9,:]
    
    X_uusi[i,4] = X[i,6,:]
    X_uusi[i,5] = X[i,5,:]
    X_uusi[i,6] = X[i,4,:]
            
for i in range(0, len(X)):
            
    X_uusi[i,3] = np.sqrt(X_uusi[i,0]**2 + X_uusi[i,1]**2 + X_uusi[i,2]**2)
    
for i in range(0, len(X)):
            
    X_uusi[i,7] = np.sqrt(X_uusi[i,6]**2 + X_uusi[i,5]**2 + X_uusi[i,4]**2)
    
# TESTAA MYÖS TOISINPÄIN! Ensin fft, sitten skaalaus. Nyt aika isoja
# ekoissa!
    
# MUISTA HÖSÖ ÄÄLIÖ TÄMÄ!!!!!!
    
# testaa samalla FFT se eka termi veks? Vakiolla ei ole väliä, muutoksilla on.
# myös ehkä vaan perinteiset keskiarvot sun muut voisi testiin ottaa    

# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!    


# %%

X_uusi = np.abs(np.fft.fft(X_uusi, axis = 2))

for i in range(0, X_uusi.shape[0]):
    X_uusi[i] = preprocessing.StandardScaler().fit_transform(X_uusi[i])

X_uusi = np.reshape(X_uusi,(X_uusi.shape[0], X_uusi.shape[1]*128))

# %%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
        traincounter += 1
            
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)


# %% Testaan eri algoja... SVM:

t = time.time()

C = np.linspace(5, 30, 200)
kernel = ['rbf']
gamma = ['auto']

random_grid = {
    'C': C,
    'kernel': kernel,
    'gamma' : gamma
}
SVM = SVC()

SVM_haku = RandomizedSearchCV(
    estimator=SVM,
    param_distributions=random_grid,
    n_iter=50,
    cv=5,
    verbose=2,
    random_state=42,
    # n_jobs=6
    n_jobs=-1
)
SVM_haku.fit(X_uusi, y) 

print("SVM omalle datalle haku antaa: ", accuracy_score(y, SVM_haku.predict(X_uusi)))
print(SVM_haku.best_params_)
print(SVM_haku.best_score_)

SVM_haku_paras = SVM_haku.best_estimator_

# %% Puuhaku

n_estimators = [n for n in range(5,200,5)]
max_features = ['sqrt']
max_depth = [100]
min_samples_split = [n for n in range(2,10)]
min_samples_leaf = [n for n in range(1,3)]
bootstrap = [False]
kriteeri = ['gini', 'entropy']


random_grid = {
    'n_estimators': n_estimators,
    'max_features': max_features,
    'max_depth': max_depth,
    'min_samples_split': min_samples_split,
    'min_samples_leaf': min_samples_leaf,
    'bootstrap': bootstrap,
    'criterion': kriteeri
}

puu = ExtraTreesClassifier()

puu_haku = RandomizedSearchCV(
    estimator=puu,
    param_distributions=random_grid,
    n_iter=3200,
    cv=5,
    verbose=2,
    random_state=42,
    # n_jobs=6
    n_jobs=-1
)

puu_haku.fit(X_uusi, y) 

print("Puu_haku omalle datalle antaa: ", accuracy_score(y, puu_haku.predict(X_uusi)))
print(puu_haku.best_params_)
print(puu_haku.best_score_)

puu_haku_paras = puu_haku.best_estimator_

# %%

n_neighbors = [n for n in range(2, 64, 1)]
weights = ['uniform', 'distance']
p = [n for n in range(1, 3, 1)]

random_grid = {'n_neighbors': n_neighbors,
               'weights': weights,
               'p': p}  

naapurit = KNeighborsClassifier(
    n_jobs=-1
)

naapurit_haku = RandomizedSearchCV(
    estimator=naapurit,
    param_distributions=random_grid,
    n_iter=600,
    cv=5,
    verbose=2,
    random_state=42,
    # n_jobs=6
    n_jobs=-1
)
naapurit_haku.fit(X_uusi, y)

print("Naapurit omalle datalle antaa: ", accuracy_score(y, naapurit_haku.predict(X_uusi)))
print(naapurit_haku.best_params_)
print(naapurit_haku.best_score_)

paras_naapurit = naapurit_haku.best_estimator_

print("Aikaa meni: ", time.time()-t, " s")
