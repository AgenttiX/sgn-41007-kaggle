from joblib import dump
import numpy as np

from sklearn import preprocessing
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RandomizedSearchCV
# from sklearn.model_selection import train_test_split
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder

# %%

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
    
# testaa?
# for x in X:
#     x = preprocessing.StandardScaler().fit_transform(x)
    
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])
        
X = np.abs(np.fft.fft(X, axis = 2))


train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
        traincounter += 1
            
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

# %%
X = np.reshape(X, (len(y), 10*128))

n_neighbors = [n for n in range(2, 128, 1)]
weights = ['uniform', 'distance']
p = [n for n in range(1, 3, 1)]

random_grid = {'n_neighbors': n_neighbors,
               'weights': weights,
               'p': p}  

naapurit = KNeighborsClassifier()

naapurit_haku = RandomizedSearchCV(
    estimator=naapurit,
    param_distributions=random_grid,
    n_iter=127*2*3,
    cv=5,
    verbose=2,
    random_state=42,
    n_jobs=6
)
naapurit_haku.fit(X, y)

print("naapurit_haku antaa: ", accuracy_score(y, naapurit_haku.predict(X)))
print(naapurit_haku.best_params_)
print(naapurit_haku.best_score_)

paras = naapurit_haku.best_estimator_

dump(paras, 'T_naapurit_haku.joblib') 
