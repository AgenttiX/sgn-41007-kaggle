import numpy as np
import time
from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from keras.utils import to_categorical
from keras import models
from keras import layers
from keras.wrappers.scikit_learn import KerasClassifier
from keras import backend
from keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint
from sklearn.model_selection import StratifiedKFold


def create_network(droppi, neuroneja, neurotasot, optimoija, tasokerroin, aktivointi):

    backend.clear_session()
    
    verkko = models.Sequential()
    
    verkko.add(layers.Dense(units = neuroneja, activation = aktivointi, input_dim = 10*128))
    verkko.add(layers.Dropout(rate = droppi))

    uudet_neuronit = int(neuroneja*tasokerroin)

    for i in range(1,neurotasot):
            
        verkko.add(layers.Dense(units = uudet_neuronit, activation = aktivointi))
        verkko.add(layers.Dropout(rate = droppi))

        uudet_neuronit = int(uudet_neuronit*tasokerroin)
       
    verkko.add(layers.Dense(9, activation= 'softmax'))
    
    verkko.summary()

    verkko.compile(metrics=['accuracy'], loss = 'categorical_crossentropy', optimizer = optimoija)
    
    return verkko


#%%assert isinstance(tasokerroin, )

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):

    X[n] = preprocessing.StandardScaler().fit_transform(X[n])

X_koko_fft = np.abs(np.fft.fft(X, axis=2))

#X_leikattu = np.empty((X_koko_fft.shape[0], 6, 64))

"""
for n in range(0, X_koko_fft.shape[0]):

    for i in range(0, X_koko_fft.shape[1]):

        X_koko_fft[n,i,0] =  X_koko_fft[n,i,0] / max(X_koko_fft[:,i,0])
        """

#for n in range(0, X_koko_fft.shape[0]):

    #X_leikattu[n] = X_koko_fft[n, 3:9, 1:65]

for n in range(0, 1703):

    X_koko_fft[n] = preprocessing.StandardScaler().fit_transform(X_koko_fft[n])

X_koko_fft = np.reshape(X_koko_fft, [X_koko_fft.shape[0], X_koko_fft.shape[1]*X_koko_fft.shape[2]] )

#%%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line    
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
    
groups = []

with open('groups.csv') as file:
    next(file)
    
    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])
        
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)
gkf = GroupKFold(n_splits = 2).split(X_koko_fft, y, groups)
y = to_categorical(y) 

#%%

droppi = [0.5]# np.linspace(0.3,0.9, 10)
neuroneja = np.linspace(32,256,200, dtype = int)
neurotasot = [n for n in range (1,3)]
optimoija =  ['Adam', 'Nadam', 'Adadelta', 'RMSprop', 'Adamax', 'Adagrad', 'sgd']
tasokerroin = [1] #np.linspace(0.1,1,10)
aktivointi = ['softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid']

random_grid= {'droppi' : droppi,
              'neuroneja' : neuroneja,
              'neurotasot' : neurotasot,
              'optimoija' : optimoija,
              'tasokerroin': tasokerroin,
              'aktivointi' : aktivointi

             }   

neuroverkko = KerasClassifier(build_fn = create_network, epochs = 200,  verbose = 0, batch_size = 64)

neurohaku = RandomizedSearchCV(estimator = neuroverkko, param_distributions = random_grid, n_iter = 3333, cv = gkf, verbose = 1000000)

t = time.time()

stop = [EarlyStopping(monitor='acc', patience=8, min_delta = 0.01, verbose = 10, mode = 'max')]

neurohaku.fit(X_koko_fft, y, callbacks = stop)

#%%

print("PARAS TULOS: " , neurohaku.best_score_, " PARAMETREILLA :", neurohaku.best_params_)

means = neurohaku.cv_results_['mean_test_score']
stds = neurohaku.cv_results_['std_test_score']
params = neurohaku.cv_results_['params']

zip = zip(means, stds, params)

zipped = sorted(zip, key = lambda  x: x[0])

for mean, stdev, param in zipped:
    print("%f (%f) parametreilla: %r" % (mean, stdev, param))

print("Aikaa meni: ", (time.time() - t) / 60, " min")

#%%

paras_neuroverkko = create_network(**neurohaku.best_params_)

paras = [ModelCheckpoint('paras_verkko.hdf5', monitor = 'val_acc', verbose = 1, save_best_only = True, mode = 'max')]

X_train, X_test, y_train, y_test = train_test_split(X_koko_fft, y, test_size = 0.2)

paras_neuroverkko.fit(X_train, y_train, epochs = 600, batch_size = 32, validation_data = (X_test, y_test), verbose = 0, callbacks = paras)

#%%

kilpaverkko = create_network(**neurohaku.best_params_)

kilpaverkko.load_weights('paras_verkko.hdf5')

seis = [EarlyStopping(monitor='acc', patience=8, min_delta = 0.01, verbose = 2, mode = 'max')]

kilpaverkko.fit(X_koko_fft,y, epochs = 200, batch_size = 32, callabacks = seis, verbose = 1)

#%%

X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, X_kilpailu.shape[0]):

    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])

#X_kilpailu_leikkattu = np.empty((X_kilpailu.shape[0], 10, 128))

X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis=2))

for n in range(0, X_kilpailu.shape[0]):

    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])

#for n in range(0, X_kilpailu.shape[0]):

 #   X_kilpailu_leikkattu[n] = X_kilpailu[n, :, 0:65]

X_koko_k = np.reshape(X_kilpailuk, (X_kilpailu.shape[0], X_kilpailu.shape[1] * X_kilpailu.shape[2]))

y_kilpailu = kilpaverkko.predict(X_koko_k)

y_kilpailu = np.argmax(y_kilpailu)

labels = list(le.inverse_transform(y_kilpailu))

with open("eka_neuroilu.csv", "w") as fp:
    fp.write("# Id,Surface\n")

    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))
        
        

