import time
import numpy as np

from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.ensemble import BaggingClassifier
from sklearn.feature_selection import SelectFromModel
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import RandomizedSearchCV
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import LinearSVC
# from sklearn.tree import DecisionTreeClassifier


# %%

X = np.load('X_train_kaggle.npy')

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
        traincounter += 1
            
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2 )

x_train_koko = np.reshape(X_train,(len(y_train), 10*128))
x_test_koko = np.reshape(X_test,(len(y_test), 10*128))

t = time.time()

maksimi = 0

# %%
for C in np.geomspace(1e-7, 1, num=42):
    SVM = LinearSVC(C=C, penalty = "l2", dual=False, multi_class = 'ovr').fit(x_train_koko, y_train)
    print("SVM with linear kernel : ", accuracy_score(y_test, SVM.predict(x_test_koko)), " C:", C)

    if accuracy_score(y_test, SVM.predict(x_test_koko)) > maksimi:
        maksimi =accuracy_score(y_test, SVM.predict(x_test_koko))
        print("UUSI PARAS! alkuperäisellä SVM:  " ,accuracy_score(y_test, SVM.predict(x_test_koko))," C:", C)
                          
    model = SelectFromModel(SVM, prefit=True)
    x_new_train = model.transform(x_train_koko)
    x_new_test = model.transform(x_test_koko)
    SVM = LinearSVC(C=C, penalty = "l2", dual=False, multi_class = 'ovr').fit(x_new_train, y_train)
    print("SVM select from model accu: ", accuracy_score(y_test, SVM.predict(x_new_test)), " C:",C)

    if accuracy_score(y_test, SVM.predict(x_new_test)) > maksimi:
        maksimi =accuracy_score(y_test, SVM.predict(x_new_test))
        print("UUSI PARAS! parannetulla SVM:  " ,accuracy_score(y_test, SVM.predict(x_new_test)), " C:",C)
        print("Ehostetun koko oli nyt: ", x_new_test.shape)

# %%

for C in np.geomspace(1e-7, 1e7, num = 42):     
    
    logReg = LogisticRegression(C=C, penalty = "l2", dual=False, multi_class = 'auto', solver = 'liblinear').fit(x_train_koko, y_train)
    print("LogReg:", accuracy_score(y_test, logReg.predict(x_test_koko)))

    if accuracy_score(y_test, logReg.predict(x_test_koko)) > maksimi:
        maksimi =accuracy_score(y_test, logReg.predict(x_test_koko))
        print("UUSI PARAS! alkuperäisellä logReg:  " ,accuracy_score(y_test, logReg.predict(x_test_koko))," C:", C)
                       
    
    model = SelectFromModel(logReg, prefit=True)    
    x_new_train = model.transform(x_train_koko)
    x_new_test = model.transform(x_test_koko)
    logReg = LogisticRegression(C=C, penalty = "l2", dual=False, multi_class = 'auto', solver = 'liblinear').fit(x_new_train, y_train)
    print("logReg select from model accu: ", accuracy_score(y_test, logReg.predict(x_new_test))," C:", C)

    if accuracy_score(y_test, logReg.predict(x_new_test)) > maksimi:
        maksimi =accuracy_score(y_test, logReg.predict(x_new_test))
        print("UUSI PARAS! parannetulla logReg:  " ,accuracy_score(y_test, logReg.predict(x_new_test)), " C:",C)
        print("Ehostetun koko oli nyt: ", x_new_test.shape)
  
# %%
        
for N in range(10, 420, 10):
    
    Tree = RandomForestClassifier(n_estimators=N)
    Tree.fit(x_train_koko, y_train)
    
    print("Tree RandomForest  accuracy over time:", accuracy_score(y_test, Tree.predict(x_test_koko)), "N: ", N)
    
    if accuracy_score(y_test, Tree.predict(x_test_koko)) > maksimi:
        maksimi = accuracy_score(y_test, Tree.predict(x_test_koko))
        print("UUSI PARAS! RandomForest : ", accuracy_score(y_test, Tree.predict(x_test_koko)), "N: ", N)
        
# %%
        
for N in range(10, 1000, 10):
    
    for kriteeri in ['gini','entropy']:
    
        TreeExtra = ExtraTreesClassifier(n_estimators=N, criterion = kriteeri)
        TreeExtra.fit(x_train_koko, y_train)
        
        print("Tree LisaPuuMetsa accuracy over time:", accuracy_score(y_test, TreeExtra.predict(x_test_koko)), "N: ", N," " , kriteeri)
        
        if accuracy_score(y_test, TreeExtra.predict(x_test_koko)) > maksimi:
            maksimi = accuracy_score(y_test, TreeExtra.predict(x_test_koko))
            print("UUSI PARAS! LisaPuuMetsa : ", accuracy_score(y_test, TreeExtra.predict(x_test_koko)), "N: ", N, " ", kriteeri)

# %%

for N in range(1, 42):
    for kriteeri in ['gini', 'entropy']:
        for I in range(10, 100, 10):
                 
            sakki = BaggingClassifier(base_estimator=ExtraTreesClassifier(criterion = kriteeri, n_estimators = I), random_state=0, n_estimators = N)
            sakki.fit(x_train_koko, y_train) 
            
            
            print("Säkki antaa: ", accuracy_score(y_test, sakki.predict(x_test_koko)), "N: ", N, "puunkoko: " , I, " ", kriteeri)
            
            if accuracy_score(y_test, sakki.predict(x_test_koko)) > maksimi:
                
                maksimi = accuracy_score(y_test, sakki.predict(x_test_koko))
                print("UUSI PARAS! Säkki antaa : ", accuracy_score(y_test, bc.predict(x_test_koko)), "N: ", N)
            
# %%
t = time.time()
      
n_estimators = [n for n in range(5,255,5)]
max_features = ['auto', 'sqrt']
max_depth = [n for n in range(1, 132)]
min_samples_split = [n for n in range(2,10)]
min_samples_leaf = [n for n in range(1,7)]
bootstrap = [True, False]
kriteeri = ['gini', 'entropy']

random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
#               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap,
               'criterion': kriteeri }

puu = ExtraTreesClassifier()

puu_haku = RandomizedSearchCV(estimator = puu, param_distributions = random_grid, n_iter = 4200, cv = 3, verbose=2, random_state=42, n_jobs = 6)
puu_haku.fit(x_train_koko, y_train) 

print("Puu_haku antaa: ", accuracy_score(y_test, puu_haku.predict(x_test_koko)))
print(puu_haku.best_params_)

#%%
      
n_estimators  = [n for n in range(5,235,5)]
max_features = ['auto', 'sqrt']
max_depth = [n for n in range(1, 74)]
min_samples_split = [n for n in range(2,10)]
min_samples_leaf = [n for n in range(1,10)]
bootstrap = [True, False]
kriteeri = ['gini', 'entropy']

random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap,
               'criterion': kriteeri }  

puu = ExtraTreesClassifier()

puu_haku_kaikki = RandomizedSearchCV(estimator = puu, param_distributions = random_grid, n_iter = 42000, cv = 5, verbose=2, random_state=42, n_jobs = 6)

X_kaikki = np.reshape(X,(len(y), 10*128))

puu_haku_kaikki.fit(X_kaikki, y) 

print("Puu_haku antaa: ", accuracy_score(y, puu_haku.predict(X_kaikki)))
print(puu_haku.best_params_)



# %%

X_kilpailu = np.load('X_test_kaggle.npy')
X_kilpailu_koko = np.reshape(X_kilpailu,(1705, 10*128))

y_kilpailu = puu_haku_kaikki.predict(X_kilpailu_koko)
  
labels = list(le.inverse_transform(y_kilpailu))
with open("ekaT_submission.csv", "w") as fp:
    fp.write("Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))

print("Aikaa meni: ", time.time()-t)
