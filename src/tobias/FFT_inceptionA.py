import numpy as np
siemen = 42
np.random.seed(siemen)

import time

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
from tensorflow.keras import backend
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing.image import ImageDataGenerator
from sklearn.model_selection import StratifiedKFold
import typing as tp
from tensorflow.keras.layers import LeakyReLU
from tensorflow.keras.callbacks import ReduceLROnPlateau


#%% DATA FFT quaterionit

X = np.load('X_train_kaggle.npy')

for n in range(0, X.shape[0]):

    X[n] = preprocessing.StandardScaler().fit_transform(X[n])

X_a = np.empty([X.shape[0], 3, 128])        
X_w = np.empty([X.shape[0], 3, 128])     
   
X_abs = np.empty([X.shape[0], 1, 128])
X_wabs = np.empty([X.shape[0], 1, 128])

for i in range(0, X.shape[0]):
    
    X_a[i,0] = X[i,7,:]
    X_a[i,1] = X[i,8,:]
    X_a[i,2] = X[i,9,:]
            
for i in range(0, X.shape[0]):
            
    X_abs[i] = np.sqrt(X_a[i,0]**2 + X_a[i,1]**2 + X_a[i,2]**2)
        
for i in range(0, len(X)):
    
    X_w[i,0] = X[i,4,:]
    X_w[i,1] = X[i,5,:]
    X_w[i,2] = X[i,6,:]
            
for i in range(0, len(X)):
            
    X_wabs[i] = np.sqrt(X_a[i,0]**2 + X_a[i,1]**2 + X_a[i,2]**2)
    
#X = np.concatenate((X, X_abs, X_wabs), axis = 1)  

X_fft = np.abs(np.fft.fft(X, axis=2))    

X_leikattu = np.empty((X.shape[0], 10, 64))

for n in range(0, X_fft.shape[0]):

    X_leikattu[n] = X_fft[n, :, 1:65]
            
X_koko_fft = np.reshape(X_leikattu, [X_leikattu.shape[0], X_leikattu.shape[1], X_leikattu.shape[2], 1] )

#%%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line

    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])

groups = []

with open('groups_oma.csv') as file:
    next(file)

    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])

le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

gkf = GroupKFold(n_splits=2).split(X, y, groups)
y = to_categorical(y)

#%% Luodaan inception tyyppinen verkko
    
def luo_inception(konv_neuroneja = 16, aktivointi = 'relu', neuroneja = 32, moduuleita = 1, alustus = 'glorot_uniform', optimoija = 'sgd', 
                  syva_aktivointi = 'relu', normalisointi = False, droppi = 0.5):
    
    backend.clear_session()
        
    sisaan = layers.Input(shape = (10,64,1))
      
    eka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(sisaan)
    eka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(eka_putki)
        
    toka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(sisaan)
    toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(toka_putki)
    toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(toka_putki)
        
    kolmas_putki = layers.MaxPooling2D((3,3), strides = (1,1), padding = 'same')(sisaan)
    kolmas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(kolmas_putki)
    
    neljas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(sisaan)
        
    ulos = layers.concatenate([eka_putki, toka_putki, kolmas_putki, neljas_putki], axis = 3)
    
    if normalisointi:
        
        ulos = layers.BatchNormalization()(ulos)
        
    for i in range(1,moduuleita):
        
            
        eka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(ulos)
        eka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(eka_putki)
                
        toka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(ulos)
        toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(toka_putki)
        toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(toka_putki)
        
        kolmas_putki = layers.MaxPooling2D((3,3), strides = (1,1), padding = 'same')(ulos)        
        kolmas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(kolmas_putki)
            
        neljas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(ulos)
        
        ulos = layers.concatenate([eka_putki, toka_putki, kolmas_putki, neljas_putki], axis = 3)
        
        if normalisointi:
            
            ulos = layers.BatchNormalization()(ulos)
                                
    ulos = layers.GlobalAveragePooling2D()(ulos)           
                        
    ulos = layers.Dense(neuroneja, activation = syva_aktivointi, kernel_initializer=alustus)(ulos)
    
    if not normalisointi:
        
        ulos = layers.Dropout(rate = droppi)(ulos)
        
    else:
        
        ulos = layers.Dropout(rate = droppi * 0.5)(ulos)
        
            
    ulos = layers.Dense(9, activation = 'softmax', kernel_initializer=alustus)(ulos)
    
    verkko = models.Model(inputs = sisaan, outputs = ulos)
    
#    verkko.summary()
        
    verkko.compile(metrics=['accuracy'], loss='categorical_crossentropy', optimizer=optimoija)
            
    return verkko


#%%    

neuroneja = [30]  
optimoija = ['RMSprop', 'Adam', 'Nadam'] #  ['SGD', 'RMSprop', 'Adagrad', 'Adadelta', 'Adam', 'Adamax', 'Nadam']
#syva_aktivointi =  ['softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid', 'LeakyRelu']
konv_neuroneita = np.arange(10,101,10) 
moduuleita = np.arange(1,7)
normalisointi = [True, False]


random_grid = {
               'neuroneja': neuroneja,
              # 'neurotasot': neurotasot,
               'optimoija': optimoija,               
#               'aktivointi' : aktivointi, 
               #'aktivointi': aktivointi,
               'konv_neuroneja' : konv_neuroneita,
               #'konv_tasot' : konv_tasot,               
 #              'alustus' : alustus,
               #'leveys' : leveys  
               'moduuleita' : moduuleita,
               'normalisointi' : normalisointi
               }
#%%

neuroverkko = KerasClassifier(build_fn=luo_inception, epochs=100, verbose=0, batch_size=64)

neurohaku = RandomizedSearchCV(estimator=neuroverkko, param_distributions=random_grid, n_iter = 125, cv=gkf, verbose=100000)

t = time.time()

stop = EarlyStopping(monitor='acc', patience=10, min_delta=0.01, verbose=2, mode='max')

neurohaku.fit(X_koko_fft, y, callbacks=[stop])

print("PARAS TULOS: ", neurohaku.best_score_, " PARAMETREILLA :", neurohaku.best_params_)

#%% Testaa eka piste toka piste

means = neurohaku.cv_results_['mean_test_score']
stds = neurohaku.cv_results_['std_test_score']
params = neurohaku.cv_results_['params']
eka_piste = neurohaku.cv_results_['split0_test_score']
toka_piste = neurohaku.cv_results_['split1_test_score']

zippi = zip(means, stds, eka_piste, toka_piste, params)
zipped = sorted(zippi, key=lambda x: x[0])

for mean, stdev, eka_piste, toka_piste, param in zipped:
    print("%f (%f) tulokset:   %f  ,  %f    parametreilla: %r" % (mean, stdev, eka_piste, toka_piste ,param))

print("Aikaa meni: ", (time.time() - t) / 60, " min")
   
#%%

#%% Muista zoomit!! zoom_range [-0.1, +0.1]
genarvot = [         
#            {'width_shift_range' : 0.75, 'height_shift_range' : 0.75 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
#            {'width_shift_range' : 0.50, 'height_shift_range' : 0.50 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
#            {'width_shift_range' : 0.35, 'height_shift_range' : 0.35 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
#            {'width_shift_range' : 0.25, 'height_shift_range' : 0.25 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
            {'width_shift_range' : 0.10, 'height_shift_range' : 0.0 ,'fill_mode' : 'reflect', 'horizontal_flip' : False, 'vertical_flip' : False},
            {'width_shift_range' : 0.03, 'height_shift_range' : 0.0 ,'fill_mode' : 'reflect', 'horizontal_flip' : False, 'vertical_flip' : False},                                                
            {'width_shift_range' : 0.0, 'height_shift_range' : 0.0 ,'fill_mode' : 'reflect', 'horizontal_flip' : False, 'vertical_flip' : False},   
            ]

gkf = GroupKFold(n_splits=2).split(X, y, groups)

#%%

train_id, test_id = next(gkf)
X_train = X_koko_fft[train_id]
X_test = X_koko_fft[test_id]
y_train = y[train_id]
y_test = y[test_id]
 
#%%

parhaat = zipped[-1:]

parhaat_acc = []
parhaat_acc_param = []
parhaat_gene = []

#for mean, _, _, _, testi_parametrit in parhaat:
    
for gen in genarvot:
        
    t = time.time()
    
    verkko =  {'optimoija': 'Nadam', 'normalisointi': True, 'neuroneja': 30, 'moduuleita': 3, 'konv_neuroneja': 30}
    
    paras_neuroverkko = luo_inception(**verkko)
    
    datagen = ImageDataGenerator(**gen)                    
    
    oppiminen = ReduceLROnPlateau(monitor = 'acc', factor = 0.6, patience = 8, min_lr = 1e-9, min_delta = 0.01, verbose = 1)
                    
    historia = paras_neuroverkko.fit_generator(datagen.flow(X_train, y_train, batch_size = 64), 
                                               steps_per_epoch = int(np.ceil(X_train.shape[0] / float(64))),                                                                                   
                                               epochs=125, validation_data=(X_test, y_test), verbose = 0,
                                               callbacks = [oppiminen])
    
    #Tähän pohjatulos CV
    pohja = mean        
    
    plt.figure()        
    plt.figure(figsize=(30,15))
 
    plt.plot(historia.history['acc'])
    plt.plot(historia.history['val_acc'])
    plt.title('Raaka mallin tarkkuus raaka data, parametreilla: ' + str(verkko) + ' gen: ' + str(gen))
    plt.ylabel('Tarkkuus')
    plt.xlabel('aikakausi')
    plt.legend(['train', 'test'], loc='upper left')
    plt.grid()
    plt.show()
    
    plt.figure()
    plt.figure(figsize=(30,15))
    plt.plot(historia.history['loss'])
    plt.plot(historia.history['val_loss'])
    plt.title('Raaka mallin häviö raaka data, parametreilla: ' + str(verkko) + ' gen: ' + str(gen))
    plt.ylabel('Häviö')
    plt.xlabel('aikakausi')
    plt.legend(['train', 'test'], loc='upper left')
    plt.grid()    
    plt.show()
    
    print("PARAS VAL_ACC: " , max(historia.history['val_acc']))    
    print("std: ", np.std(historia.history['val_acc']))
    print("mean: ", np.mean(historia.history['val_acc']))
    print("Aikaa meni generaattorin fittiin: ", (time.time() - t) / 60, " min")



#%%

datagen = ImageDataGenerator(width_shift_range = 0.0, fill_mode = 'constant', cval = 0)    


kilpaparametrit =  {'optimoija': 'Nadam', 'normalisointi': True, 'neuroneja': 30, 'moduuleita': 3, 'konv_neuroneja': 30}
kilpaverkko = luo_inception(**kilpaparametrit)
                            
oppiminen = ReduceLROnPlateau(monitor = 'acc', factor = 0.6, patience = 5, min_lr = 1e-9, min_delta = 0.01, verbose = 1)
#


kilpahistoria = kilpaverkko.fit_generator(datagen.flow(X_koko_fft, y, batch_size = 64), 
                                           steps_per_epoch = int(np.ceil(X_koko_fft.shape[0] / float(64))),                                                                                   
                                           epochs=100, verbose = 1, callbacks = [oppiminen])



kilpaverkko.save('14_2_v2_kaggle_incA_FFT_puh.5h')

#
#plt.figure(3)
#plt.figure(figsize=(30,15))
#plt.plot(kilpahistoria.history['acc'])
#plt.title('Kilpaverkon tarkkuus parametreilla: ' + str(testi_parametrit) + ' gen: ' + str(genarvot))
#plt.ylabel('Tarkkuus')
#plt.xlabel('aikakausi')
#plt.grid()
#plt.show()
#
#plt.figure(4)
#plt.figure(figsize=(30,15))
#plt.plot(kilpahistoria.history['loss'])
#plt.title('Kilpaverkon häviö parametreilla: ' + str(testi_parametrit) + ' gen: ' + str(genarvot))
#plt.ylabel('Häviö')
#plt.xlabel('aikakausi')
#plt.grid()
#plt.show()


#%% DEG Ladataan ja muunnetaan kisadata

#X_kisa = np.load('X_test_kaggle.npy')
#
#X_deg_kisa = np.empty((X_kisa.shape[0], 9, X_kisa.shape[2]))
#
#for n in range(0, X_kisa.shape[0]):
#    
#    X_deg_kisa[n,3:,:] = X_kisa[n,4:,:]
#    
#    for i in range(0, X_kisa.shape[2]):
#        
#        qx = X[n,0,i]
#        qy = X[n,1,i]
#        qz = X[n,2,i]
#        qw = X[n,3,i]
#    
#        roll,pitch,yaw = quaterion_to_euler_angle(qx, qy, qz, qw)
#        
#        X_deg_kisa[n,0,i] = roll
#        X_deg_kisa[n,1,i] = pitch
#        X_deg_kisa[n,2,i] = yaw
#            
#for n in range(0, X_deg.shape[0]):
#
#    X_deg_kisa[n] = preprocessing.StandardScaler().fit_transform(X_deg[n])
#
#X_kisa_fft = np.abs(np.fft.fft(X_deg_kisa, axis=2))
#
#X_leikattu = np.empty((X_kisa_fft.shape[0], 9, 64))
#
#for n in range(0, X_kisa_fft.shape[0]):
#
#    X_leikattu[n] = X_kisa_fft[n, :, 1:65]
#    
#for n in range(0, 1703):
#
#    X_leikattu[n] = preprocessing.StandardScaler().fit_transform(X_leikattu[n])    
#
#X_kisa_fft = np.reshape(X_leikattu, [X_leikattu.shape[0], X_leikattu.shape[1], X_leikattu.shape[2], 1] )

#%% QUATERION Ladataan ja muunnetaan kisadata

X_kisa = np.load('X_test_kaggle.npy')
#
#for n in range(0, X_kisa.shape[0]):
#    
#    X_kisa[n] = preprocessing.StandardScaler().fit_transform(X_kisa[n])
    
X_kisa_fft = np.abs(np.fft.fft(X_kisa, axis=2))    

X_leikattu = np.empty((X_kisa_fft.shape[0], 10, 64))

for n in range(0, X_kisa_fft.shape[0]):

    X_leikattu[n] = X_kisa_fft[n, :, 1:65]
    
X_kisa_fft = np.reshape(X_leikattu, [X_leikattu.shape[0], X_leikattu.shape[1], X_leikattu.shape[2], 1] )

##%%

y_kilpailu = kilpaverkko.predict(X_kisa_fft)

y_kilpailu_tulos = np.argmax(y_kilpailu, axis = 1)

labels = list(le.inverse_transform(y_kilpailu_tulos))

with open("14_2_v2_FFT_raaka_incA.csv", "w") as fp:
    
    fp.write("# Id,Surface\n")
             
    for i, label in enumerate(labels):
        
        fp.write("%d,%s\n" % (i, label))



 
















    