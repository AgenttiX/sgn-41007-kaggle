import numpy as np
import time

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from keras.utils import to_categorical
from keras import models
from keras import layers
from keras.wrappers.scikit_learn import KerasClassifier
from keras import backend
from keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from keras.callbacks import ModelCheckpoint


def create_network( droppi = 0.5, neuroneja = 32, neurotasot = 1, optimoija = 'sgd', tasokerroin=1, aktivointi='relu', konv_neuroneita = 32, konv_aktivointi = 'relu', konv_tasokerroin = 1, konv_tasot = 1):

    backend.clear_session()

    verkko = models.Sequential()

    verkko.add(layers.Conv2D(filters=konv_neuroneita, kernel_size = (3,3), activation = konv_aktivointi, input_shape = (10, 65, 1)))
    verkko.add(layers.MaxPooling2D(pool_size = (1,2)))

    uudet_konv_neuronit = int(konv_neuroneita * konv_tasokerroin)

    for i in range(1, konv_tasot):

        verkko.add(layers.Conv2D(filters = uudet_konv_neuronit, kernel_size = (3,3), activation = konv_aktivointi))
        verkko.add(layers.MaxPooling2D(pool_size=(1, 2)))
        uudet_konv_neuronit = int(uudet_konv_neuronit * konv_tasokerroin)

    verkko.add(layers.Flatten())

    verkko.add(layers.Dense(units = neuroneja, activation = aktivointi, input_dim = 10 * 65 * konv_neuroneita))
    verkko.add(layers.Dropout(rate = droppi))

    uudet_neuronit = int(neuroneja * tasokerroin)

    for i in range(1, neurotasot):

        verkko.add(layers.Dense(units = uudet_neuronit, activation = aktivointi))
        verkko.add(layers.Dropout(rate = droppi))

        uudet_neuronit = int(uudet_neuronit * tasokerroin)

    verkko.add(layers.Dense(9, activation='softmax'))

    verkko.summary()

    verkko.compile(metrics=['accuracy'], loss='categorical_crossentropy', optimizer=optimoija)

    return verkko

X = np.load('X_train_kaggle.npy')

X_koko_fft = np.abs(np.fft.fft(X, axis=2))

X_leikattu = np.empty((X_koko_fft.shape[0], 10, 65))

for n in range(0, X_koko_fft.shape[0]):

    for i in range(0, X_koko_fft.shape[1]):
        X_koko_fft[n, i, 0] = X_koko_fft[n, i, 0] / max(X_koko_fft[:, i, 0])

for n in range(0, X_koko_fft.shape[0]):
    X_leikattu[n] = X_koko_fft[n, :, 0:65]

for n in range(0, 1703):
    X_leikattu[n] = preprocessing.StandardScaler().fit_transform(X_leikattu[n])

X_koko_fft = np.reshape(X_leikattu, [X_koko_fft.shape[0], X_leikattu.shape[1] , X_leikattu.shape[2], 1])

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line

    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])

groups = []

with open('groups.csv') as file:
    next(file)

    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])

le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)
gkf = GroupKFold(n_splits=2).split(X_koko_fft, y, groups)
y = to_categorical(y)

#droppi = np.linspace(0.3, 0.9, 10)
neuroneja = np.linspace(32, 512, 5, dtype=int)
#¤neurotasot = [n for n in range(1, 2)]
optimoija = ['Adam', 'Nadam', 'Adadelta', 'RMSprop', 'Adamax', 'Adagrad']
#tasokerroin = np.linspace(0.1, 1, 10)
#aktivointi = ['softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid']
konv_tasokerroin = np.linspace(1, 2, 5)
konv_neuroneita = np.linspace(8, 64, 10, dtype = int)
konv_tasot = np.linspace(1,5, dtype = int)

random_grid = {#'droppi': droppi,
               'neuroneja': neuroneja,
               #'neurotasot': neurotasot,
               'optimoija': optimoija,
               #'tasokerroin': tasokerroin,
               #'aktivointi': aktivointi,
               'konv_neuroneita' : konv_neuroneita,
               'konv_tasot' : konv_tasot,
                'konv_tasokerroin' : konv_tasokerroin
               }

neuroverkko = KerasClassifier(build_fn=create_network, epochs=300, verbose=1, batch_size=64)

neurohaku = RandomizedSearchCV(estimator=neuroverkko, param_distributions=random_grid, n_iter = 10, cv=gkf, verbose=10, n_jobs=1)

t = time.time()

stop = [EarlyStopping(monitor='acc', patience=15, min_delta=0.01, verbose=2, mode='max')]

neurohaku.fit(X_koko_fft, y, callbacks=stop)

print("PARAS TULOS: ", neurohaku.best_score_, " PARAMETREILLA :", neurohaku.best_params_)

means = neurohaku.cv_results_['mean_test_score']
stds = neurohaku.cv_results_['std_test_score']
params = neurohaku.cv_results_['params']

zip = zip(means, stds, params)
zipped = sorted(zip, key=lambda x: x[0])

for mean, stdev, param in zipped:
    print("%f (%f) parametreilla: %r" % (mean, stdev, param))

print("Aikaa meni: ", (time.time() - t) / 60, " min")

paras_neuroverkko = create_network(**neurohaku.best_params_)

paras = [ModelCheckpoint('paras_verkko.hdf5', monitor='val_acc', verbose=1, save_best_only=True, mode='max')]

X_train, X_test, y_train, y_test = train_test_split(X_koko_fft, y, test_size=0.2)

paras_neuroverkko.fit(X_train, y_train, epochs=1000, batch_size=32, validation_data=(X_test, y_test), verbose=1, callbacks=paras)








