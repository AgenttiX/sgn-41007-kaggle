import numpy as np

from sklearn import preprocessing
from sklearn.discriminant_analysis import LinearDiscriminantAnalysis
from sklearn.ensemble import VotingClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.linear_model import LogisticRegression
from sklearn.metrics import accuracy_score
from sklearn.model_selection import cross_val_score
from sklearn.model_selection import GroupKFold
from sklearn.model_selection import RandomizedSearchCV
from sklearn.neighbors import KNeighborsClassifier
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC

# %%
# Load data

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
    
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])

# # kulmanopeus normi
#
# X_ang = np.empty((X.shape[0], 1, X.shape[2]))
#
# for i in range(0, X.shape[0]):
#
#    X_ang[i] = np.sqrt(X[i,4]**2 + X[i,5]**2 + X[i,6]**2)
#
# # kiihtyvyys normi
#
# X_ac =  np.empty((X.shape[0], 1, X.shape[2]))
#
# for i in range(0, X.shape[0]):
#
#    X_ac[i] = np.sqrt(X[i,7]**2 + X[i,8]**2 + X[i,9]**2)
#
# X_koko = np.concatenate([X, X_ang, X_ac], axis = 1)
        
X_koko_fft = np.abs(np.fft.fft(X, axis = 2))

# nyqvist taajuudet pois ja alun vakioarvot jotka isoja pois

X_fft_leikattu = np.empty((X.shape[0], X_koko_fft.shape[1], 63)) 

for n in range(0, X.shape[0]):
    
    X_fft_leikattu[n,:,:] = X_koko_fft[n,:,1:64]
    
X_koko_fft = np.reshape(X_fft_leikattu, (X.shape[0], X_fft_leikattu.shape[1]*X_fft_leikattu.shape[2]))

# X_koko_fft = np.reshape(X_koko_fft, (X.shape[0], X_koko_fft.shape[1]*X_koko_fft.shape[2]))

# %%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
    
groups = []

with open('groups.csv') as file:
    next(file)
    
    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])
        
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

# %%
# SVM

gkf = GroupKFold(n_splits=3).split(X_koko_fft, y, groups = groups)

C  = np.linspace(1,42, 1234)
gamma = np.linspace(1e-7, 1e-1, 1234)
probability = [False]

random_grid = {'C' : C,
               'gamma' : gamma,
               'probability' : probability}   

SVM = SVC()

SVM_haku = RandomizedSearchCV(estimator = SVM, param_distributions = random_grid, n_iter = 2*2800, cv = gkf, verbose=2, random_state=42, n_jobs = 6)
SVM_haku.fit(X_koko_fft, y) 

print("SVM omalle datalle haku antaa: ", accuracy_score(y, SVM_haku.predict(X_koko_fft)))
print(SVM_haku.best_params_)
print(SVM_haku.best_score_)

paras_SVM = SVM_haku.best_estimator_ 
print("CV validation: ", np.mean(cross_val_score(paras_SVM, X_koko_fft, y, cv=10, verbose = 2)))

# %% TESTATAAN kagglessa. Submitit hyvä käyttää..
#
# X_kilpailu = np.load('X_test_kaggle.npy')
#
# for n in range(0, X_kilpailu.shape[0]):
#
#    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
#
# X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis = 2))
#
# X_kilpailu_leikattu = np.empty((X_kilpailu.shape[0], X_kilpailu.shape[1], 63))
#
# for n in range(0, X_kilpailu.shape[0]):
#
#    X_kilpailu_leikattu[n,:,:] = X_kilpailu[n,:,1:64]
#
# X_kilpailu_koko = np.reshape(X_kilpailu_leikattu, (X_kilpailu.shape[0], X_kilpailu.shape[1]*X_kilpailu_leikattu.shape[2]))
#
# y_kilpailu = paras_SVM.predict(X_kilpailu_koko)
#
# labels = list(le.inverse_transform(y_kilpailu))
#
# with open("T_SVM_haku_fft_v2.csv", "w") as fp:
#
#    fp.write("# Id,Surface\n")
#
#    for i, label in enumerate(labels):
#
#        fp.write("%d,%s\n" % (i, label))


# %% Puuhaku (ExtraTreesClassifier)

gkf = GroupKFold(n_splits=3).split(X_koko_fft, y, groups = groups)

n_estimators  = [n for n in range(5,300,3)]
max_features = ['sqrt']
max_depth = [123]
min_samples_split = [n for n in range(2,7)]
min_samples_leaf = [n for n in range(1,5)]
bootstrap = [False]
kriteeri = ['entropy', 'gini']


random_grid = {'n_estimators': n_estimators,
               'max_features': max_features,
               'max_depth': max_depth,
               'min_samples_split': min_samples_split,
               'min_samples_leaf': min_samples_leaf,
               'bootstrap': bootstrap,
               'criterion': kriteeri
               }  

puu = ExtraTreesClassifier()

puu_haku = RandomizedSearchCV(estimator = puu, param_distributions = random_grid, n_iter = 100*60, cv = gkf, verbose=2, random_state=42, n_jobs = 6)

puu_haku.fit(X_koko_fft, y) 

print("Puu_haku omalle datalle antaa: ", accuracy_score(y, puu_haku.predict(X_koko_fft)))
print(puu_haku.best_params_)
print(puu_haku.best_score_)

paras_puu = puu_haku.best_estimator_
print("CV validation: ", np.mean(cross_val_score(paras_puu, X_koko_fft, y, cv=10, verbose = 2)))

# %%

gkf = GroupKFold(n_splits=3).split(X_koko_fft, y, groups = groups)

n_neighbors = [n for n in range(2, 128, 1)]
weights = ['uniform', 'distance']

random_grid = {'n_neighbors': n_neighbors,
               'weights': weights
               }  

naapurit = KNeighborsClassifier()

naapurit_haku = RandomizedSearchCV(estimator = naapurit, param_distributions = random_grid, n_iter = 10*60, cv = gkf, verbose=2, random_state=42, n_jobs = 6)
naapurit_haku.fit(X_koko_fft,y)

print("Naapurit omalle datalle antaa: ", accuracy_score(y, naapurit_haku.predict(X_koko_fft)))
print(naapurit_haku.best_params_)
print(naapurit_haku.best_score_)

paras_naapurit = naapurit_haku.best_estimator_
print("CV validation: ", np.mean(cross_val_score(paras_naapurit, X_koko_fft, y, cv=10, verbose = 2)))

# %%

gkf = GroupKFold(n_splits=3).split(X_koko_fft, y, groups = groups)

solver = ['svd']

random_grid = {'solver': solver}

LD = LinearDiscriminantAnalysis()

LD_haku = RandomizedSearchCV(param_distributions = random_grid, estimator = LD, cv= gkf, verbose = 2, n_jobs = 6)
LD_haku.fit(X_koko_fft,y)

print("LD omalle datalle: ", accuracy_score(y, LD_haku.predict(X_koko_fft)))
print(LD_haku.best_params_)
print(LD_haku.best_score_)

LD_paras = LD_haku.best_estimator_
print("CV validation: ", np.mean(cross_val_score(LD_paras, X_koko_fft, y, cv=10, verbose = 2)))

# %% Logreg haku

gkf = GroupKFold(n_splits=3).split(X_koko_fft, y, groups = groups)

C = np.linspace(1e-6,1e-2,1234)
solver = ['lbfgs']
multi_class = ['ovr']
max_iter = [1234]

random_grid = {'C' : C,
               'solver' : solver,
               'multi_class' : multi_class,
               'max_iter' : max_iter
               }

logreg = LogisticRegression()

logreg_haku = RandomizedSearchCV(estimator = logreg, param_distributions = random_grid, n_iter = 5*216, verbose=2, cv = gkf, n_jobs = 6)
logreg_haku.fit(X_koko_fft,y)

print("Logreg haku omalle datalle antaa: ",  accuracy_score(y, logreg_haku.predict(X_koko_fft)))
print(logreg_haku.best_params_)
print(logreg_haku.best_score_)

paras_logreg = logreg_haku.best_estimator_

print("CV validation: ", np.mean(cross_val_score(paras_logreg, X_koko_fft, y, cv=10)))

# %% Yhdistetään luokittelijat äänestyksellä

aanestys = VotingClassifier( estimators =[('SVM', paras_SVM), ('logreg', paras_logreg), ('puu', paras_puu)], voting = 'hard') #KOVAA!
aanestys.fit(X_koko_fft, y)    

print("Äänestys antaa: ", accuracy_score(y, aanestys.predict(X_koko_fft)))
print("CV validation: ", cross_val_score(aanestys, X_koko_fft, y, cv=10, verbose = 2))

# %%
#
# X_kilpailu = np.load('X_test_kaggle.npy')
#
# for n in range(0, X_kilpailu.shape[0]):
#
#    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
#
# X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis = 2))
#
# X_kilpailu_leikattu = np.empty((X_kilpailu.shape[0], X_kilpailu.shape[1], 63))
#
# for n in range(0, X_kilpailu.shape[0]):
#
#    X_kilpailu_leikattu[n,:,:] = X_kilpailu[n,:,1:64]
#
# X_kilpailu_koko = np.reshape(X_kilpailu_leikattu, (X_kilpailu.shape[0], X_kilpailu.shape[1]*X_kilpailu_leikattu.shape[2]))
#
# y_kilpailu = aanestys.predict(X_kilpailu_koko)
#
# labels = list(le.inverse_transform(y_kilpailu))
#
# with open("SVM_logreg_puu_aanestaa.csv", "w") as fp:
#
#    fp.write("# Id,Surface\n")
#
#    for i, label in enumerate(labels):
#
#        fp.write("%d,%s\n" % (i, label))
