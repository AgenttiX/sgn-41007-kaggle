import numpy as np

from sklearn.metrics import accuracy_score
from sklearn.model_selection import train_test_split
from sklearn.preprocessing import LabelEncoder
from sklearn.svm import SVC
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing

# %%

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
    
# testaa?    
# for x in X:
#   x = preprocessing.StandardScaler().fit_transform(x)     
    
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])
        
X = np.abs(np.fft.fft(X, axis = 2))


train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line
    traincounter = 0
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
        traincounter += 1
            
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

# %% Linear SVC parametrien haku kokeilu

X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.1 )
X_train = np.reshape(X_train,(len(y_train), 10*128))
X_test = np.reshape(X_test,(len(y_test), 10*128))


C = np.linspace(1e-1, 10, 1000)
tol = np.linspace(1e-5,1e-2, 1000)
decision_function_shape = ['ovr', 'ovo']
kernel = ['poly', 'rbf', 'sigmoid']
degree = [n for n in range(2,5,1)]
gamma = ['auto', 'scale']

random_grid = {'C': C,
               'decision_function_shape': decision_function_shape,               
               'degree': degree,
               'kernel': kernel,
               'tol': tol,
               'gamma': gamma}

SVM = SVC()

SVM_haku = RandomizedSearchCV(
    estimator=SVM,
    param_distributions=random_grid,
    n_iter=1234*6,
    cv=3,
    verbose=2,
    random_state=42,
    n_jobs=6
)
SVM_haku.fit(X_train, y_train) 

print("SVM haku antaa: ", accuracy_score(y_test, SVM_haku.predict(X_test)))
print("SVM omalle datalle haku antaa: ", accuracy_score(y_train, SVM_haku.predict(X_train)))
print(SVM_haku.best_params_)
print(SVM_haku.best_estimator_)
print(SVM_haku.best_score_)
print(SVM_haku.scorer_ )

# %%

X_koko = np.reshape(X, (len(y), 10*128))

C = np.linspace(1e-1, 21, 10000)
tol = np.linspace(1e-5, 1e-1, 10000)
decision_function_shape = ['ovr']
kernel = ['rbf']
gamma = ['scale']

random_grid = {'C': C,
               'decision_function_shape': decision_function_shape,                              
               'kernel': kernel,
               'tol' : tol,
               'gamma' : gamma}  
SVM = SVC()

SVM_haku_koko = RandomizedSearchCV(
    estimator=SVM,
    param_distributions=random_grid,
    n_iter=2,
    cv=3,
    verbose=2,
    random_state=42,
    n_jobs=2
)
SVM_haku_koko.fit(X_koko, y) 

# %%

print("SVM omalle datalle haku antaa: ", accuracy_score(y, SVM_haku_koko.predict(X_koko)))
print(SVM_haku_koko.best_params_)
print(SVM_haku_koko.best_estimator_)
print(SVM_haku_koko.best_score_)

# %%

X_kilpailu = np.load('X_test_kaggle.npy')

for n in range(0, 1705):
    
    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
        
X_kilpailu = np.abs(np.fft.fft(X_kilpailu, axis=2))

X_kilpailu_koko = np.reshape(X_kilpailu, (1705, 10*128))

y_kilpailu = SVM_haku_koko.predict(X_kilpailu_koko)
  
labels = list(le.inverse_transform(y_kilpailu))
with open("T_SVM_haku_fft.csv", "w") as fp:
    fp.write("# Id,Surface\n")
    for i, label in enumerate(labels):
        fp.write("%d,%s\n" % (i, label))
