import numpy as np
siemen = 42
np.random.seed(siemen)

import time

from sklearn.preprocessing import LabelEncoder
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
from tensorflow.keras.utils import to_categorical
from tensorflow.keras import models
from tensorflow.keras import layers
from tensorflow.keras.wrappers.scikit_learn import KerasClassifier
from tensorflow.keras import backend
from tensorflow.keras.callbacks import EarlyStopping
from sklearn.model_selection import train_test_split
from tensorflow.keras.callbacks import ModelCheckpoint
import matplotlib.pyplot as plt
from tensorflow.keras.preprocessing.image import ImageDataGenerator
import typing as tp
from tensorflow.keras.callbacks import ReduceLROnPlateau


def quaterion_to_euler_angle(qx: float, qy: float, qz: float, qw: float) -> tp.Tuple[float, float, float]:
    # https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

    # roll (x-axis rotation)
    sinr_cosp = 2.0 * (qw * qx + qy * qz)
    cosr_cosp = 1.0 - 2.0 * (qx * qx + qy * qy)
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    # pitch (y-axis rotation)
    sinp = 2.0 * (qw * qy - qz * qx)
    # mask replaces if-clause: if (np.abs(sinp) >= 1): (...) else: (...)
    mask = np.abs(sinp) >= 1
    pitch = \
        mask * np.copysign(np.pi / 2, sinp) + \
        (~mask * np.arcsin(sinp))
    # yaw (z-axis rotation)
    siny_cosp = 2.0 * (qw * qz + qx * qy)
    cosy_cosp = 1.0 - 2.0 * (qy * qy + qz * qz)
    yaw = np.arctan2(siny_cosp, cosy_cosp)

    return roll, pitch, yaw

#%%
X = np.load('X_train_kaggle.npy')

X_deg = np.empty((X.shape[0], 3, X.shape[2]))

for n in range(0, X.shape[0]):
    
    for i in range(0, X.shape[2]):
        
        qx = X[n,0,i]
        qy = X[n,1,i]
        qz = X[n,2,i]
        qw = X[n,3,i]
        
        roll,pitch,yaw = quaterion_to_euler_angle(qx, qy, qz, qw)
        
        X_deg[n,0,i] = roll
        X_deg[n,1,i] = pitch
        X_deg[n,2,i] = yaw

X_a = np.empty([X.shape[0], 3, 128])        
X_w = np.empty([X.shape[0], 3, 128])     
   
X_abs = np.empty([X.shape[0], 1, 128])
X_wabs = np.empty([X.shape[0], 1, 128])

for i in range(0, X.shape[0]):
    
    X_a[i,0] = X[i,7,:]
    X_a[i,1] = X[i,8,:]
    X_a[i,2] = X[i,9,:]
            
for i in range(0, X.shape[0]):
            
    X_abs[i] = np.sqrt(X_a[i,0]**2 + X_a[i,1]**2 + X_a[i,2]**2)
        
for i in range(0, len(X)):
    
    X_w[i,0] = X[i,4,:]
    X_w[i,1] = X[i,5,:]
    X_w[i,2] = X[i,6,:]
            
for i in range(0, len(X)):
            
    X_wabs[i] = np.sqrt(X_a[i,0]**2 + X_a[i,1]**2 + X_a[i,2]**2)
    
X = np.concatenate((X, X_abs, X_wabs), axis = 1)  
    
#for n in range(0, X.shape[0]):
#
#    X[n] = preprocessing.StandardScaler().fit_transform(X[n])

X = np.reshape(X, [X.shape[0], X.shape[1],X.shape[2], 1] )

#%%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line

    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])

groups = []

with open('groups_oma.csv') as file:
    next(file)

    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])

le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)

gkf = GroupKFold(n_splits=2).split(X, y, groups)
y = to_categorical(y)

#%% Luodaan inception tyyppinen verkko
    
def luo_inception(konv_neuroneja = 16, aktivointi = 'relu', neuroneja = 32, moduuleita = 1, alustus = 'glorot_uniform', optimoija = 'sgd',
                  syva_aktivointi = 'relu', droppi = 0.6, normalisointi = False ):
    
    backend.clear_session()
    
    sisaan = layers.Input(shape = (12,128,1))
    
    eka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(sisaan)
    eka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer=alustus)(eka_putki)
        
    toka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(sisaan)
    toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(toka_putki)
    toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(toka_putki)
        
    kolmas_putki = layers.MaxPooling2D((3,3), strides = (1,1), padding = 'same')(sisaan)
    kolmas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(kolmas_putki)
    
    neljas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(sisaan)
        
    ulos = layers.concatenate([eka_putki, toka_putki, kolmas_putki, neljas_putki], axis = 3)
    
    if normalisointi:
        
        ulos = layers.BatchNormalization()(ulos)
        
    for i in range(1,moduuleita):
        
            
        eka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(ulos)
        eka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(eka_putki)
                
        toka_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(ulos)
        toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(toka_putki)
        toka_putki = layers.Conv2D(konv_neuroneja, (3,3), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(toka_putki)
        
        kolmas_putki = layers.MaxPooling2D((3,3), strides = (1,1), padding = 'same')(kolmas_putki)
        kolmas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(ulos)
                
        neljas_putki = layers.Conv2D(konv_neuroneja, (1,1), padding = 'same', activation = aktivointi, kernel_initializer = alustus)(ulos)
        
        ulos = layers.concatenate([eka_putki, toka_putki, kolmas_putki, neljas_putki], axis = 3)
        
        if normalisointi:
        
            ulos = layers.BatchNormalization()(ulos)
        
    ulos = layers.GlobalAveragePooling2D()(ulos)
            
    ulos = layers.Dense(neuroneja, activation = syva_aktivointi, kernel_initializer = alustus)(ulos)
    
    if not normalisointi:
    
        ulos = layers.Dropout(rate = droppi)(ulos)
        
    else:
        
        ulos = layers.Dropout(rate = droppi*0.5)(ulos)
    
    # TESTAA PIRUUTTAAN ILMAN SYVÄÄ VÄLIVERKKOA!
    ulos = layers.Dense(9, activation = 'softmax', kernel_initializer = alustus)(ulos)
    
    verkko = models.Model(inputs = sisaan, outputs = ulos)
    
#    verkko.summary()
        
    verkko.compile(metrics=['accuracy'], loss='categorical_crossentropy', optimizer=optimoija)
    
    return verkko


#%%    

neuroneja = [30] #np.arange(20,61,10)
optimoija = ['RMSprop']
#syva_aktivointi =  ['softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid']
konv_neuroneita = np.arange(10,171,10) #[n for n in range(1, 60, 5)] 
#alustus =  ['lecun_uniform', 'glorot_normal', 'glorot_uniform', 'lecun_normal']
moduuleita = np.arange(1,6)
normalisointi = [True, False]

random_grid = {
#                'droppi' : droppi, 
               'neuroneja': neuroneja,
               'optimoija': optimoija,               
               'konv_neuroneja' : konv_neuroneita,
#               'aktivointi' : aktivointi

 #              'alustus' : alustus,
               'moduuleita' : moduuleita,
               'normalisointi': normalisointi
               }
#%%

neuroverkko = KerasClassifier(build_fn=luo_inception, epochs=100, verbose=0, batch_size=64)

neurohaku = RandomizedSearchCV(estimator=neuroverkko, param_distributions=random_grid, n_iter = 275, cv=gkf, verbose=100000)

t = time.time()

stop = EarlyStopping(monitor='acc', patience=10, min_delta=0.01, verbose=2, mode='max')

neurohaku.fit(X, y, callbacks=[stop])

print("PARAS TULOS: ", neurohaku.best_score_, " PARAMETREILLA :", neurohaku.best_params_)

#%%

means = neurohaku.cv_results_['mean_test_score']
stds = neurohaku.cv_results_['std_test_score']
params = neurohaku.cv_results_['params']
eka_piste = neurohaku.cv_results_['split0_test_score']
toka_piste = neurohaku.cv_results_['split1_test_score']

zippi = zip(means, stds, eka_piste, toka_piste, params)
zipped = sorted(zippi, key=lambda x: x[0])

for mean, stdev, eka_piste, toka_piste, param in zipped:
    print("%f (%f) tulokset:   %f  ,  %f    parametreilla: %r" % (mean, stdev, eka_piste, toka_piste ,param))

print("Aikaa meni: ", (time.time() - t) / 60, " min")
   
#%%


#%% Muista zoomit!! zoom_range [-0.1, +0.1]
genarvot = [         
            {'width_shift_range' : 0.75, 'height_shift_range' : 0.75 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
            {'width_shift_range' : 0.50, 'height_shift_range' : 0.50 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
            {'width_shift_range' : 0.35, 'height_shift_range' : 0.35 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
            {'width_shift_range' : 0.25, 'height_shift_range' : 0.25 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
            {'width_shift_range' : 0.10, 'height_shift_range' : 0.10 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},
            {'width_shift_range' : 0.0, 'height_shift_range' : 0.0 ,'fill_mode' : 'reflect', 'horizontal_flip' : True, 'vertical_flip' : True},                                                            
            {'width_shift_range' : 0.0, 'height_shift_range' : 0.0 ,'fill_mode' : 'reflect', 'horizontal_flip' : False, 'vertical_flip' : False}                                                
            ]

gkf = GroupKFold(n_splits=2).split(X, y, groups)

train_id, test_id = next(gkf)
X_train = X[train_id]
X_test = X[test_id]
y_train = y[train_id]
y_test = y[test_id]
 
#%%

parhaat = zipped[-3:]

parhaat_acc = []
parhaat_acc_param = []
parhaat_gene = []

for mean, _, _, _, testi_parametrit in parhaat:
    
    for gen in genarvot:
        
        t = time.time()
        
        paras_neuroverkko = luo_inception(**testi_parametrit)
        
        datagen = ImageDataGenerator(**gen)                    
        
        oppiminen = ReduceLROnPlateau(monitor = 'acc', factor = 0.7, patience = 5, min_lr = 1e-10, min_delta = 0.01, verbose = 1)
                        
        historia = paras_neuroverkko.fit_generator(datagen.flow(X_train, y_train, batch_size = 64), 
                                                   steps_per_epoch = int(np.ceil(X_train.shape[0] / float(64))),                                                                                   
                                                   epochs=100, validation_data=(X_test, y_test), verbose = 0,
                                                   callbacks = [oppiminen])
        
        pohja = mean
        
        plt.figure()
        plt.figure(figsize=(30,15))     
        plt.plot(historia.history['acc'])
        plt.plot(historia.history['val_acc'])
        plt.title('Raaka mallin tarkkuus raaka data, parametreilla: ' + str(testi_parametrit) + ' gen: ' + str(gen))
        plt.ylabel('Tarkkuus')
        plt.xlabel('aikakausi')
        plt.legend(['train', 'test'], loc='upper left')
        plt.grid()
        plt.show()
        
        plt.figure()
        plt.figure(figsize=(30,15))
        plt.plot(historia.history['loss'])
        plt.plot(historia.history['val_loss'])
        plt.title('Raaka mallin häviö raaka data, parametreilla: ' + str(testi_parametrit) + ' gen: ' + str(gen))
        plt.ylabel('Häviö')
        plt.xlabel('aikakausi')
        plt.legend(['train', 'test'], loc='upper left')
        plt.grid()    
        plt.show()
                
        print("PARAS VAL_ACC: " , max(historia.history['val_acc']))    
        print("std: ", np.std(historia.history['val_acc']))
        print("mean: ", np.mean(historia.history['val_acc']))
        print("Aikaa meni generaattorin fittiin: ", (time.time() - t) / 60, " min")



#%%

#kilpaverkko = luo_inception(**testi_parametrit)

#kilpaverkko.summary()

#paras_kilpa = ModelCheckpoint('paras_kilpailuverkko_inceptionA.hdf5', monitor = 'acc', verbose = 1, save_best_only = True, mode = 'max', save_weights_only = True)

#kilpahistoria = kilpaverkko.fit_generator(datagen.flow(X, y, batch_size = 64), 
#                                           steps_per_epoch = int(np.ceil(X.shape[0] / float(64))),                                                                                   
#                                           epochs=90)
##%%
#plt.figure(3)
#plt.figure(figsize=(20,10))
#plt.plot(kilpahistoria.history['acc'])
#plt.title('Kilpaverkon tarkkuus')
#plt.ylabel('Tarkkuus')
#plt.xlabel('aikakausi')
#plt.grid()
#plt.show()
#
#plt.figure(4)
#plt.figure(figsize=(20,10))
#plt.plot(kilpahistoria.history['loss'])
#plt.title('Kilpaverkon häviö')
#plt.ylabel('Häviö')
#plt.xlabel('aikakausi')
#plt.grid()
#plt.show()

# !!!!!!!!!!!!!!!!!!!!!!!
# TALLENNA JOKAINEN KAGGLE PALAUTETTU VERKKO!!!!

#kilpaverkko.save('kaggle_13_2_raaka_inceptionA.h5')

#%% Kilpailu submit

#X_kilpailu = np.load('X_test_kaggle.npy')
#
#for n in range(0, X_kilpailu.shape[0]):
#    
#    X_kilpailu[n] = preprocessing.StandardScaler().fit_transform(X_kilpailu[n])
#    
#X_kilpailu = np.reshape(X_kilpailu, [X_kilpailu.shape[0], X_kilpailu.shape[1], X_kilpailu.shape[2], 1])
#
#y_kilpailu = kilpaverkko.predict(X_kilpailu)
#
#y_kilpailu_tulos = np.argmax(y_kilpailu, axis = 1)
#
#labels = list(le.inverse_transform(y_kilpailu_tulos))
#
#with open("raaka_inceptionA.csv", "w") as fp:
#    
#    fp.write("# Id,Surface\n")
#             
#    for i, label in enumerate(labels):
#        
#        fp.write("%d,%s\n" % (i, label))
#
#
#
#





 
















    