import time

import numpy as np
# from sklearn.metrics import accuracy_score
from sklearn.preprocessing import LabelEncoder
# from sklearn.svm import SVC
from sklearn.model_selection import RandomizedSearchCV
from sklearn import preprocessing
from sklearn.model_selection import GroupKFold
# from sklearn.ensemble import ExtraTreesClassifier
# from keras.layers.normalization import BatchNormalization
# from keras.models import Sequential
from keras.utils import to_categorical
# from keras.callbacks import ModelCheckpoint
# from keras.layers import Conv2D, MaxPooling2D, Flatten, Dropout
from keras import models
from keras import layers
from keras.wrappers.scikit_learn import KerasClassifier


def create_network(droppi, neuroneja, neurotasot) -> models.Sequential:
    
    verkko = models.Sequential()
    
    verkko.add(layers.Dense(units = neuroneja, activation = 'sigmoid', input_dim = 768))
    verkko.add(layers.Dropout(rate = droppi))
    
    for i in range(1, neurotasot):
        verkko.add(layers.Dense(units = neuroneja, activation = 'relu'))    
        verkko.add(layers.Dropout(rate = droppi))
       
    verkko.add(layers.Dense(9, activation= 'softmax'))
    
    verkko.summary()
    
    verkko.compile(metrics=['accuracy'], loss = 'categorical_crossentropy', optimizer = 'sgd')
    
    return verkko


#%%

X = np.load('X_train_kaggle.npy')

for n in range(0, 1703):
        
    X[n] = preprocessing.StandardScaler().fit_transform(X[n])

X_va = np.empty((X.shape[0], 6, X.shape[2]))

for i in range(0, X.shape[0]):
    
    X_va[i] = X[i,4:10,:]
            
X_koko_fft = np.abs(np.fft.fft(X_va, axis = 2))

for n in range(0, 1703):
        
    X_koko_fft[n] = preprocessing.StandardScaler().fit_transform(X_koko_fft[n])

X_koko_fft =np.reshape(X_koko_fft, [X_va.shape[0], X_va.shape[1]*X_va.shape[2]] )

#%%

train_labels = []

with open('y_train_final_kaggle.csv') as file:
    next(file)  # skip first line    
    
    for row in file:
        fields = row.rstrip().split(',')
        train_labels.append(fields[1])
    
groups = []

with open('groups.csv') as file:
    next(file)
    
    for row in file:
        fields = row.strip().split(',')
        groups.append(fields[1])
        
le = LabelEncoder()
le.fit(train_labels)
y = le.transform(train_labels)
y = to_categorical(y) 

#%%

gkf = GroupKFold(n_splits=3).split(X_koko_fft, y, groups = groups)

droppi = np.linspace(0,0.9,10)
neuroneja = np.geomspace(8,512,10, dtype=int)
neurotasot = [n for n in range (1,4)]

random_grid= {'droppi' : droppi,
              'neuroneja' : neuroneja,
              'neurotasot' : neurotasot
             }   

neuroverkko = KerasClassifier(build_fn = create_network, epochs = 100, batch_size = 10, verbose = 1) 

neurohaku = RandomizedSearchCV(estimator = neuroverkko, param_distributions = random_grid, n_iter = 150, cv = gkf, verbose=2,  n_jobs = 1)

t = time.time()
neurohaku.fit(X_koko_fft, y) 

#%%

print("PARAS TULOS: " , neurohaku.best_score_, " PARAMETREILLA :", neurohaku.best_params_)

means = neurohaku.cv_results_['mean_test_score']
stds = neurohaku.cv_results_['std_test_score']
params = neurohaku.cv_results_['params']

for mean, stdev, param in zip(means, stds, params):
    
    print("%f (%f) parametreilla: %r" % (mean, stdev, param))

print("Aikaa meni: ", time.time()-t)    





