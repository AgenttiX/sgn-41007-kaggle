import typing as tp

import numpy as np
import sklearn.ensemble
import sklearn.svm

from mika import dataloader, features
from mika import splitting
# from task1 import splitting
import alpi.tools.feature_extraction as feature_extraction


def optimize_estimator(
        estimator: sklearn.base.BaseEstimator,
        params: tp.Dict[str, any],
        data: dataloader.Data,
        x_data: np.ndarray = None,
        trials: int = 10,
        n_splits: int = 4):
    """Estimator optimization using nested cross-validation"""

    if x_data is None:
        x_data = data.x_data

    non_nested_scores = np.zeros((trials,))
    nested_scores = np.zeros((trials,))

    best_params = []

    for i in range(trials):
        inner_cv = splitting.split_by_group(data, n_splits=n_splits)
        outer_cv = splitting.split_by_group(data, n_splits=n_splits)

        clf = sklearn.model_selection.GridSearchCV(
            estimator=estimator,
            param_grid=params,
            cv=inner_cv
        )
        clf.fit(x_data, data.labels)

        non_nested_scores[i] = clf.best_score_
        best_params.append(clf.best_params_)

        nested_score = sklearn.model_selection.cross_val_score(clf, X=x_data, y=y, cv=outer_cv)
        nested_scores[i] = nested_score.mean()

    return best_params, nested_scores, non_nested_scores


# def data_multiplier(data: dataloader.Data, noise_adders: tp.List[callable]):
#     x_data = data.x_data
#
#     noises = []
#     for adder in noise_adders:
#         noises.append(adder(x_data))
#
#     new_x_data = np.concatenate(x_data, *noises, axis=0)
#     new_labels = np.repeat(data.labels, TODO)
#
#     sample_ids_by_group = data.sample_ids_by_group()
#
#     samples = 0
#     for group in sample_ids_by_group:
#         sample_array = np.array(list(sample_ids_by_group[group]))
#
#         for i in range(1, len(noise_adders)+1):
#             sample_ids_by_group[group].update(set(sample_array * i))
#
#         samples += len(sample_ids_by_group[group])
#
#     if samples != x_data.size * (len(noise_adders) + 1):
#         raise RuntimeError
#
#     return new_x_data,


class MultiEstimator(sklearn.base.BaseEstimator):
    def __init__(self):
        self.estimators: tp.List[tp.Tuple[str, any, callable]] = [
            (
                "Basic RandomForest",
                sklearn.ensemble.RandomForestClassifier(
                    n_estimators=100,
                    n_jobs=-1
                ),
                feature_extraction.do_nothing
            ),
            (
                "Mean+std RandomForest",
                sklearn.ensemble.RandomForestClassifier(
                    n_estimators=100,
                    n_jobs=-1
                ),
                features.means_stdevs
            ),
            (
                "Orientation RandomForest",
                sklearn.ensemble.RandomForestClassifier(
                    n_estimators=100,
                    n_jobs=-1
                ),
                feature_extraction.orientation_to_eluer_angles
            ),
            (
                "FFT RandomForest",
                sklearn.ensemble.RandomForestClassifier(
                    n_estimators=100,
                    n_jobs=-1
                ),
                feature_extraction.fft
            ),
            (
                "FFT ExtraTrees",
                sklearn.ensemble.ExtraTreesClassifier(
                    n_jobs=-1
                )
            ),
            (
                "FFT SVM",
                sklearn.svm.SVC(
                    gamma="scale",
                    probability=True
                ),
                feature_extraction.fft
            ),
            (
                "Spectrogram RandomForest",
                sklearn.ensemble.RandomForestClassifier(
                    n_estimators=100,
                    n_jobs=-1
                ),
                feature_extraction.spectrogram
            ),
        ]

    @staticmethod
    def __flatten(x: np.ndarray) -> np.ndarray:
        return x.reshape(x.shape[0], -1)

    def fit(self, X: np.ndarray, y: np.ndarray):
        for name, clf, feature in self.estimators:
            clf.fit(self.__flatten(feature(X)), y)

        return self

    # def get_params(self, deep=True):
    #     params = []
    #     for name, clf, feature in self.estimators:
    #         params.append(clf.get_params(deep))
    #
    #     return {"asdf": params}

    def predict(self, X: np.ndarray) -> np.ndarray:
        return np.argmax(self.predict_proba(X), axis=1)

    def predict_proba(self, X: np.ndarray) -> np.ndarray:
        probas = []

        for name, clf, feature in self.estimators:
            probas.append(clf.predict_proba(self.__flatten(feature(X))))

        probas_arr = np.array(probas)
        # print(probas_arr)
        probas_mean = np.mean(probas_arr, axis=0)

        return probas_mean

    def score(self, X: np.ndarray, y: np.ndarray) -> np.ndarray:
        total_count = X.shape[0]
        predictions = self.predict(X)
        correct_count = np.sum(predictions == y)

        return correct_count / total_count

    # def set_params(self, params):
    #     for i, (name, clf, feature) in enumerate(self.estimators):
    #         clf.set_params(params[i])


def main():
    data = dataloader.train_data()

    splits = splitting.split_by_group(data, n_splits=10)

    # rf = sklearn.ensemble.RandomForestClassifier(n_estimators=100)

    scores: np.ndarray = sklearn.model_selection.cross_val_score(MultiEstimator(), X=data.x_data, y=data.labels, cv=splits)
    # score = sklearn.model_selection.cross_val_score(voting, X=feat, y=data.labels, cv=splits)
    print(scores.round(2))
    print("Mean: {:.3f}".format(scores.mean()))
    print("Min: {:.3f}".format(scores.min()))
    print("Max: {:.3f}".format(scores.max()))
    print("Std:{:.3f}".format(scores.std()))


if __name__ == "__main__":
    main()
