import os.path
import logging
import typing as tp

import numpy as np
import pandas.io.parsers

import sklearn.preprocessing

FOLDER = os.path.join(os.path.dirname(os.path.dirname((os.path.abspath(__file__)))), "data")
GROUPS_FILE = "groups.csv"
X_TEST_FILE = "X_test_kaggle.npy"
X_TRAIN_FILE = "X_train_kaggle.npy"
Y_TRAIN_FILE = "y_train_final_kaggle.csv"

logger = logging.getLogger(__name__)


class Sample:
    """An individual sample"""
    def __init__(self,
                 i: int,
                 x: np.ndarray, y: np.ndarray = None,
                 group_data: np.ndarray = None,
                 label: int = None):
        if x.ndim != 2 or (y is not None and y.ndim != 1) or (group_data is not None and group_data.ndim != 1):
            raise ValueError("Invalid sample data")

        self.i = i
        self.label = label
        self.x_data = x
        self.group_data = group_data
        self.y_data = self.y_data = self.group_data[[0, 2]] if self.group_data is not None else y

        self.orient_x = self.x_data[0, :]
        self.orient_y = self.x_data[1, :]
        self.orient_z = self.x_data[2, :]
        self.orient_w = self.x_data[3, :]
        self.ang_vel_x = self.x_data[4, :]
        self.ang_vel_y = self.x_data[5, :]
        self.ang_vel_z = self.x_data[6, :]
        self.lin_acc_x = self.x_data[7, :]
        self.lin_acc_y = self.x_data[8, :]
        self.lin_acc_z = self.x_data[9, :]

        self.surface = self.y_data[1]
        self.group = self.group_data[1] if self.group_data is not None else None


class Data:
    """A dataset such as training or testing"""
    def __init__(self, folder: str, x_file: str, y_file: str = None, groups_file: str = None):
        x_path = os.path.join(folder, x_file)
        self.x_data = np.load(x_path)
        self.x_data.setflags(write=False)

        self.sample_count = self.x_data.shape[0]

        if not self.x_data.ndim == 3 or not self.x_data.shape[1] == 10:
            raise ValueError("Invalid x data")

        # For easy access to individual sensors
        self.orient_x = self.x_data[:, 0, :]
        self.orient_y = self.x_data[:, 1, :]
        self.orient_z = self.x_data[:, 2, :]
        self.orient_w = self.x_data[:, 3, :]
        self.ang_vel_x = self.x_data[:, 4, :]
        self.ang_vel_y = self.x_data[:, 5, :]
        self.ang_vel_z = self.x_data[:, 6, :]
        self.lin_acc_x = self.x_data[:, 7, :]
        self.lin_acc_y = self.x_data[:, 8, :]
        self.lin_acc_z = self.x_data[:, 9, :]

        self.y_data: np.ndarray = None
        self.group_data: np.ndarray = None
        self.groups: np.ndarray = None
        self.surfaces: np.ndarray = None
        self.labels: np.ndarray = None

        if groups_file:
            groups_path = os.path.join(folder, groups_file)

            # The pandas.io.parsers.read_csv is fast
            # https://softwarerecs.stackexchange.com/questions/7463/fastest-python-library-to-read-a-csv-file
            self.group_data = pandas.io.parsers.read_csv(
                groups_path,
                engine="c",
                # dtype={
                #     "Id": np.int_,
                #     "Group Id": np.int_,
                #     "Surface": str
                # }
            ).values
            self.group_data.setflags(write=False)

            self.groups = self.group_data[:, 1]
            self.y_data = self.group_data[:, [0, 2]]
        elif y_file:
            y_path = os.path.join(folder, y_file)

            self.y_data = pandas.io.parsers.read_csv(
                y_path,
                engine="c",
                # dtype={
                #     "Id": np.int_,
                #     "Surface": str
                # }
            ).values

        if self.y_data is not None:
            if self.x_data.shape[0] != self.y_data.shape[0]:
                raise ValueError("x and y data counts do not match")

            self.y_data.setflags(write=False)

            self.surfaces = self.y_data[:, 1]

            self.label_encoder = sklearn.preprocessing.LabelEncoder()
            self.label_encoder.fit(self.surfaces)

            self.labels = self.label_encoder.transform(self.surfaces)
            self.labels.setflags(write=False)

    # Overrides

    def __getitem__(self, i: int) -> Sample:
        """This function enables the retrieval of measurements as meas = data[i]"""
        return self.get_sample(i)

    # Metadata

    def classes(self) -> tp.Set[str]:
        """Get all classes, aka. surfaces as strings"""
        classes = set()
        for i in range(self.y_data.shape[0]):
            classes.add(self.y_data[i, 1])
        return classes

    def groups_dict(self) -> tp.Dict[int, str]:
        """Get all measurement group indices and the corresponding class names"""
        data = {}
        for i in range(self.group_data.shape[0]):
            data[self.group_data[i, 1]] = self.group_data[i, 2]
        return data

    def groups_by_label(self) -> tp.Dict[int, tp.Set[int]]:
        data = {}
        for name, groups in self.groups_by_label_name().items():
            data[self.label_encoder.transform([name])[0]] = groups
        return data

    def groups_by_label_name(self) -> tp.Dict[str, tp.Set[int]]:
        """Get measurement groups grouped by the label (=surface)"""
        data = {}
        for i in range(self.group_data.shape[0]):
            if self.group_data[i, 2] not in data:
                data[self.group_data[i, 2]] = set()
            data[self.group_data[i, 2]].add(self.group_data[i, 1])
        return data

    def unique_groups(self) -> tp.Set[int]:
        """Get the groups that are unique to their label (=surface)

        In other words, get the wood measurement group
        """
        unique = set()
        for label, label_groups in self.groups_by_label_name().items():
            if len(label_groups) == 1:
                unique.update(label_groups)

        return unique

    # Sample getters

    def get_x_y(
            self,
            train_inds: np.ndarray,
            test_inds: np.ndarray,
            x_data: np.ndarray = None) -> tp.Tuple[np.ndarray, np.ndarray, np.ndarray, np.ndarray]:

        if x_data is None:
            x_data = self.x_data
        x_train = x_data[train_inds, ...]
        x_test = x_data[test_inds, ...]
        y_train = self.labels[train_inds, ...]
        y_test = self.labels[test_inds, ...]

        # Note: the order is the same as in Scikit-learn
        return x_train, x_test, y_train, y_test

    def get_sample(self, i: int) -> Sample:
        """Get a single sample by id"""
        sample_group = None
        sample_y = None

        if self.group_data is not None:
            sample_group = self.group_data[i, :]
        elif self.y_data is not None:
            sample_y = self.y_data[i, :]

        return Sample(
            i=i,
            x=self.x_data[i, :, :], y=sample_y,
            group_data=sample_group, label=self.labels[i])

    def samples(self) -> tp.List[Sample]:
        """Get all samples as a list"""
        lst = []
        for i in range(self.x_data.shape[0]):
            lst.append(self.get_sample(i))
        return lst

    def sample_ids_by_group(self) -> tp.Dict[int, tp.Set[int]]:
        """Get samples sorted into groups"""
        data = {}
        for i in range(self.group_data.shape[0]):
            sample_id = self.group_data[i, 0]
            group_id = self.group_data[i, 1]
            if group_id not in data:
                data[group_id] = {sample_id}
            else:
                data[group_id].add(sample_id)
        return data


def train_data():
    return Data(folder=FOLDER, x_file=X_TRAIN_FILE, y_file=Y_TRAIN_FILE, groups_file=GROUPS_FILE)


def test_data():
    return Data(folder=FOLDER, x_file=X_TEST_FILE)


if __name__ == "__main__":
    train = train_data()
    train.samples()
    test = test_data()

    # print(train.labels)

    for surface, groups in train.groups_by_label_name().items():
        print(surface, groups)
