import numpy as np

import sklearn.preprocessing


# Feature extractors

def reshaped(data: np.ndarray) -> np.ndarray:
    """For task 1 part 4a"""
    if data.ndim != 3:
        raise ValueError("Invalid data")

    return np.reshape(
        data,
        newshape=(data.shape[0], data.shape[1] * data.shape[2])
    )


def normed(data) -> np.ndarray:
    """Get normed data (=normalisoitu)

    :param data: input data (if not given, use self.x_data)
    :return: normed data
    """
    normed = np.zeros_like(data)
    scaler = sklearn.preprocessing.StandardScaler()
    for i in range(data.shape[0]):
        normed[i, ...] = scaler.fit_transform(data[i, ...])
    return normed


def fft(data: np.ndarray) -> np.ndarray:
    """Get FFT transformed x data"""
    ret = np.zeros_like(data)
    scaler = sklearn.preprocessing.StandardScaler()

    for i in range(data.shape[0]):
        ret[i, ...] = scaler.fit_transform(data[i, ...])

    return np.abs(np.fft.fft(data, axis=2))


def means(data: np.ndarray) -> np.ndarray:
    """For task 1 part 4b"""
    return np.mean(data, axis=2)


def stdevs(data: np.ndarray) -> np.ndarray:
    """For task 1 part 4c"""
    if data.ndim != 3:
        raise ValueError("Invalid data")
    return np.std(data, axis=2)


def means_stdevs(data: np.ndarray):
    """For task 1 part 4c"""
    return np.concatenate((means(data), stdevs(data)), axis=1)
