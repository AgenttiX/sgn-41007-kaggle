import os
import typing as tp

import numpy as np

# Remove this line if using TensorFlow
os.environ["KERAS_BACKEND"] = "plaidml.keras.backend"

import sklearn.model_selection

import keras
import keras.wrappers.scikit_learn
# import keras.backend
# import keras.layers
# import keras.models

# from task1 import dataloader, features, splitting


def create_network(
        drop: float,
        neuron_count: int,
        neuron_levels: int,
        optimoija: str,
        tasokerroin,
        aktivointi: str):
    """Originally by Tobias"""

    # This prevents garbage accumulation in the backend
    keras.backend.clear_session()

    net = keras.models.Sequential()

    net.add(keras.layers.Dense(units=neuron_count, activation=aktivointi, input_dim=10 * 128))
    net.add(keras.layers.Dropout(rate=drop))

    new_neuron_count = int(neuron_count * tasokerroin)

    for i in range(1, neuron_levels):
        net.add(keras.layers.Dense(units=new_neuron_count, activation=aktivointi))
        net.add(keras.layers.Dropout(rate=drop))

        new_neuron_count = int(new_neuron_count * tasokerroin)

    net.add(keras.layers.Dense(9, activation='softmax'))

    net.summary()

    net.compile(metrics=['accuracy'], loss='categorical_crossentropy', optimizer=optimoija)

    print(type(net))

    return net


def main():
    drop = [0.5]
    neuron_counts = np.linspace(32, 256, 200, dtype=np.int_)
    neuron_levels = [n for n in range (1,3)]
    optimizers = ['Adam', 'Nadam', 'Adadelta', 'RMSprop', 'Adamax', 'Adagrad', 'sgd']
    tasokerroin = [1]
    activations = ['softplus', 'softsign', 'relu', 'tanh', 'sigmoid', 'hard_sigmoid']

    grid = {
        "drop": drop,
        "neuron_count": neuron_counts,
        "neuron_level": neuron_levels,
        "optimizer": optimizers,
        "tasokerroin": tasokerroin,
        "activation": activations,
    }

    net = keras.wrappers.scikit_learn.KerasClassifier(
        build_fn=create_network,
        epochs=200,
        verbose=0,
        batch_size=64
    )

    neuro_search = sklearn.model_selection.RandomizedSearchCV(
        estimator=net,
        param_distributions=grid,
        n_iter=3333,
        # cv=gkf,
        verbose=1000000
    )


if __name__ == "__main__":
    main()
