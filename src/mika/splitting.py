"""Updated version

Old version is in task1/splitting.py
"""

import typing as tp
import numpy as np

import sklearn.model_selection

from mika import dataloader


class EvenSplitter:
    """

    Note to self: attempt to use the same API as
    https://scikit-learn.org/stable/modules/generated/sklearn.model_selection.GroupShuffleSplit.html

    However, this returns values instead of indices
    """
    def __init__(self, groups_by_label: tp.Dict[int, tp.Set[int]], n_splits: int, test_size: float):
        self.groups_by_label = groups_by_label
        self.n_splits = n_splits
        self.test_size = test_size

    def split(self, splittable_groups):
        splitter = sklearn.model_selection.ShuffleSplit(n_splits=self.n_splits, test_size=self.test_size)

        splits = []

        for i in range(self.n_splits):
            splits.append((set(), set()))

        for label, groups_of_label in self.groups_by_label.items():
            splittable_groups_of_label = groups_of_label.intersection(splittable_groups)

            if not splittable_groups_of_label:
                continue

            # Note! scikit splitters return indices on the lists they are given
            splittable_groups_of_label_arr = np.array(list(splittable_groups_of_label))
            splits_of_this_group = splitter.split(splittable_groups_of_label_arr)

            for i, split in enumerate(splits_of_this_group):
                if not len(split[0]) or not len(split[1]):
                    raise RuntimeError("At least one group should be assigned in both categories for each label", split[0], split[1])

                train_groups = splittable_groups_of_label_arr[split[0]]
                test_groups = splittable_groups_of_label_arr[split[1]]

                splits[i][0].update(set(train_groups.flatten()))
                splits[i][1].update(set(test_groups.flatten()))

        split_arrays = []
        for split in splits:
            if not split[0].isdisjoint(split[1]):
                raise RuntimeError("Splits should be disjoint", split[0], split[1])

            if len(split[0]) + len(split[1]) != len(splittable_groups):
                raise RuntimeError("Count of groups should match")

            split_arrays.append((np.array(list(split[0])), np.array(list(split[1]))))

        if len(split_arrays) != self.n_splits:
            raise RuntimeError("Split count does not match")

        # for split in split_arrays:
        #     print(split[0])
        #     print(split[1])

        # For testing
        # validate_group_splits(self.groups_by_label, split_arrays)

        return split_arrays


def validate_group_splits(groups_by_label, splits: tp.List[tp.Tuple[np.ndarray, np.ndarray]]):
    labels_by_group = {}

    for label, groups in groups_by_label.items():
        for group in groups:
            labels_by_group[group] = label

    for split in splits:
        train_counts = np.zeros(len(groups_by_label))
        test_counts = np.zeros(len(groups_by_label))

        for group in split[0]:
            train_counts[labels_by_group[group]] += 1

        for group in split[1]:
            test_counts[labels_by_group[group]] += 1

        print(train_counts)
        print(test_counts)
        print()


def split_by_group(data: dataloader.Data, n_splits: int = 1, test_size: float = 0.1) \
        -> tp.List[tp.Tuple[np.ndarray, np.ndarray]]:

    return split_by_group_manual(data.sample_ids_by_group(), data.groups_by_label(), n_splits, test_size)


def split_by_group_manual(ids_by_group: tp.Dict[int, tp.Set[int]], groups_by_label: tp.Dict[int, tp.Set[int]], n_splits: int = 1, test_size: float = 0.1) \
        -> tp.List[tp.Tuple[np.ndarray, np.ndarray]]:
    """Split samples in the data by their groups

    The target test_size is only 0.1 to ensure that the actual test size is close to 0.2
    (this is necessary due to the Evensplitter's ShuffleSplit that wants to assign too many samples to testing

    "Assign all samples of each block to train, validation or test set"
    Similar to sklearn.model_selection.GroupKFold but with splitting of
    individual groups if the label has only one group.
    Returns a list of tuples (sample_ids_train, sample_ids_test)
    """
    sample_count = 0
    for group, samples in ids_by_group.items():
        sample_count += len(samples)

    unique_groups = set()
    for label, groups in groups_by_label.items():
        if len(groups) == 1:
            unique_groups.update(groups)

    # ids_by_group = data.sample_ids_by_group()
    # groups_list = list(ids_by_group.keys())

    # unique_groups = data.unique_groups()    # The groups that have to be split
    assignable_groups = set(ids_by_group.keys()) - unique_groups

    if not assignable_groups.isdisjoint(unique_groups):
        raise RuntimeError("Individually splittable groups should not be included in the primary splitting")

    assignable_groups_list = list(assignable_groups)

    splits = []

    # Handle those groups the labels of which have several groups

    # splitter = sklearn.model_selection.ShuffleSplit(n_splits=n_splits, test_size=test_size)
    #
    # for group_inds_train, group_inds_test in splitter.split(assignable_groups_list):
    #     # print(group_inds_train, group_inds_test)
    #
    #     samples_train = set()
    #     samples_test = set()
    #     for group_ind in group_inds_train:
    #         samples_train.update(ids_by_group[assignable_groups_list[group_ind]])
    #
    #     for group_ind in group_inds_test:
    #         samples_test.update(ids_by_group[assignable_groups_list[group_ind]])
    #
    #     if not samples_train.isdisjoint(samples_test):
    #         raise RuntimeError("There should not be same samples in training and testing:", samples_train.intersection(samples_test))
    #
    #     splits.append((samples_train, samples_test))

    splitter = EvenSplitter(n_splits=n_splits, test_size=test_size, groups_by_label=groups_by_label)
    for groups_train, groups_test in splitter.split(splittable_groups=assignable_groups_list):
        samples_train = set()
        samples_test = set()

        for group in groups_train:
            samples_train.update(ids_by_group[group])

        for group in groups_test:
            samples_test.update(ids_by_group[group])

        splits.append((samples_train, samples_test))

    # Handle those groups the labels of which have only one group
    unique_splitter = sklearn.model_selection.ShuffleSplit(n_splits=n_splits, test_size=test_size)

    for group in unique_groups:
        samples_of_group = list(ids_by_group[group])

        for i, (sample_inds_train, sample_inds_test) in enumerate(unique_splitter.split(samples_of_group)):
            for sample_ind in sample_inds_train:
                splits[i][0].add(samples_of_group[sample_ind])
            for sample_ind in sample_inds_test:
                splits[i][1].add(samples_of_group[sample_ind])

    split_arrays = []

    for samples_train, samples_test in splits:
        if not samples_train.isdisjoint(samples_test):
            raise RuntimeError("There should not be same samples in training and testing:", samples_train.intersection(samples_test))

        samples_train_arr = np.array(list(samples_train))
        samples_test_arr = np.array(list(samples_test))

        np.random.shuffle(samples_train_arr)
        np.random.shuffle(samples_test_arr)

        if samples_train_arr.size + samples_test_arr.size != sample_count:
            raise RuntimeError("Training + test sample count should match the total sample count")

        split_arrays.append((samples_train_arr, samples_test_arr))

    return split_arrays


def main():
    data = dataloader.train_data()

    import time
    start_time = time.perf_counter()
    splits = split_by_group(data, n_splits=10)
    print(time.perf_counter() - start_time)
    print()

    for split in splits:
        print(split[0].size, split[1].size, split[0].size + split[1].size)
        print(split[0].size / data.x_data.shape[0], split[1].size / data.x_data.shape[0])
        print()


if __name__ == "__main__":
    main()
