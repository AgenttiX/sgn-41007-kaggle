import unittest

from mika import dataloader


class DataTest(unittest.TestCase):
    @classmethod
    def setUpClass(cls):
        cls.train = dataloader.train_data()
        cls.test = dataloader.test_data()

    def test_classes(self):
        self.train.classes()

    # def test_groups(self):
    #     self.train.groups()

    def test_groups_by_label(self):
        self.train.groups_by_label()

    def test_unique_groups(self):
        self.train.unique_groups()

    def test_get_sample(self):
        self.train.get_sample(0)

    def test_samples(self):
        self.train.samples()

    def test_sample_ids_by_group(self):
        self.train.sample_ids_by_group()
