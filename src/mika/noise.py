import typing as tp
import numpy as np


def data_multiplier(
        x: np.ndarray,
        y: np.ndarray,
        noise_adders: tp.List[callable],
        splits: tp.List[tp.Tuple[np.ndarray, np.ndarray]] = None,
        noise_adder_params: tp.List[tp.Dict[str, any]] = None) \
        -> tp.Tuple[np.ndarray, np.ndarray, tp.List[tp.Tuple[np.ndarray, np.ndarray]]]:
    """Multiply the amount of data by concatenating it with noisier versions

    The given noise adders should not change the order of the data!

    Notes on the design:
    - The multiplying has to be done for the non-split dataset to avoid computing the noise multiple times
    - The splitting of the multiplied dataset should result in having the same sample and
        its noisier versions in either train or test set, not both
    """
    sample_count = x.shape[0]
    new_sample_count = sample_count * (len(noise_adders) + 1)

    noises = []
    for i, adder in enumerate(noise_adders):
        if noise_adder_params is None:
            params = {}
        else:
            params = noise_adder_params[i]

        noises.append(adder(x, **params))

    new_x = np.concatenate((x, *noises), axis=0)
    new_y = np.repeat(y, repeats=len(noise_adders)+1, axis=0)

    if not (new_x.shape[0] == new_y.shape[0] == new_sample_count):
        raise RuntimeError("Sample counts do not match")

    if splits is None:
        new_splits = None
    else:
        new_splits = []
        for split in splits:
            new_split_train_parts = []
            new_split_test_parts = []

            for i in range(0, len(noise_adders)+1):
                new_split_train_parts.append(split[0] + i*sample_count)
                new_split_test_parts.append(split[1] + i*sample_count)

            new_split_train = np.array(new_split_train_parts).flatten()
            new_split_test = np.array(new_split_test_parts).flatten()
            np.random.shuffle(new_split_train)
            np.random.shuffle(new_split_test)

            new_splits.append((
                new_split_train,
                new_split_test
            ))

        # Verify splits
        if len(new_splits) != len(splits):
            raise RuntimeError("Split count should not change")

        for split in new_splits:
            train_samples = set(split[0])
            test_samples = set(split[1])

            if not train_samples.isdisjoint(test_samples):
                raise RuntimeError("Train and test samples should not overlap")

            if len(train_samples) + len(test_samples) != new_sample_count:
                raise RuntimeError("")

    return new_x, new_y, new_splits


def white_noise(data: np.ndarray, multiplier: float = 0.01) -> np.ndarray:
    # Count standard deviations for each sensor per sample
    std = np.std(data, axis=2)
    # Compute the averages of these per-sensor standard deviations
    # std_means = np.mean(std, axis=0)
    # print(std_means)

    ret = data + np.random.rand(*data.shape) * std[..., np.newaxis] * multiplier

    if ret.shape != data.shape:
        raise RuntimeError("Output shape should match the input")

    return ret


def gaussian_noise(data: np.ndarray, multiplier: float = 0.01, width: float = 1.0) -> np.ndarray:
    std = np.std(data, axis=2)

    ret = data + np.random.normal(scale=width, size=data.shape) * std[..., np.newaxis] * multiplier

    if ret.shape != data.shape:
        raise RuntimeError("Output shape should match the input")

    return ret


def test_data_multiplier():
    from mika import dataloader
    from mika import splitting

    data = dataloader.train_data()

    splits = splitting.split_by_group(data, n_splits=5)

    new_x, new_y, new_splits = data_multiplier(
        data.x_data, data.labels, noise_adders=[white_noise, gaussian_noise], splits=splits
    )
    for split in new_splits:
        total_size = split[0].size + split[1].size
        print(total_size, split[0].size / total_size, split[1].size / total_size)


def test_noise_adders():
    from mika import dataloader
    from mika import features

    d = dataloader.train_data()

    noisy = white_noise(d.x_data)
    relative_diffs = np.mean(features.means(d.x_data - noisy) / features.means(d.x_data), axis=0)
    print(relative_diffs)

    gauss = gaussian_noise(d.x_data)
    relative_gauss_diffs = np.mean(features.means(d.x_data - gauss) / features.means(d.x_data), axis=0)
    print(relative_gauss_diffs)


if __name__ == "__main__":
    # test_noise_adders()
    test_data_multiplier()
