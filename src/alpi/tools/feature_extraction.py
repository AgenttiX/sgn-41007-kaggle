import numpy as np
from alpi.tools.read_data import get_reordered_data_and_labels, get_data_and_labels
from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, KFold
from sklearn.metrics import accuracy_score
from scipy import signal
import typing as tp

def do_nothing(data: np.ndarray) -> np.ndarray:
    return data

def spectrogram(data: np.ndarray) -> np.ndarray:

    def extract_spectrograms_for_sample(sample: np.ndarray):
        assert sample.shape == (10,128)
        out = np.empty((10,16,5))
        for i in range(10):
            f, t, spectro = signal.spectrogram(sample[i,...],
                                               nperseg=30,   # window size
                                               noverlap=10)  # window overlap
            assert spectro.shape == (16,5)
            out[i,:,:] = spectro
        return out

    out = np.empty((data.shape[0],10,16,5))
    for i in range(data.shape[0]):
        out[i] = extract_spectrograms_for_sample(data[i,:,:])

    return out

def mean_chunks_over_dimension(data: np.ndarray, chunks: int, axis: int,
                               allow_uneven_split=False,
                               function=np.mean):
    """
    Divide some dimension in array to meaned chunks.

    For example from array[1703,10,128] to array[1703,10,8]

    :param data:
    :param chunks:              Number of mean chunks to be taken.
    :param axis:
    :param allow_uneven_split:  If chunk number and dimension does not match,
                                it can be allowed. (Remainder is cropped off.)
    :param function:            You can plug here also np.max if you want...
    :return:
    """
    if not allow_uneven_split:
        # Following assertion can be removed is seen necessary.
        assert data.shape[axis] %  chunks == 0 # chunks should divide dimension.


    new_shape = list(data.shape)
    new_shape[axis] = chunks

    assing_shape = [slice(None) for i in range(len(data.shape))]
    assing_shape[axis] = None  # to be replaced later
    mean_shape = list(assing_shape)  # Copy list

    out = np.empty(new_shape, dtype=data.dtype)
    chunk_size = data.shape[axis] // chunks
    for c in range(chunks):
        assing_shape[axis] = c
        mean_shape[axis] = slice(c*chunk_size, (c+1)*chunk_size)
        out[assing_shape] = function(data[mean_shape], axis=axis)

    return out



def spectrogram_means(data):
    """Wrapper to divide spectro gram to chunks."""
    return mean_chunks_over_dimension(spectrogram(data), chunks=4, axis=2)

def spectrogram_maxima(data: np.ndarray):
    """Wrapper to split data and apply max function for splits."""
    return mean_chunks_over_dimension(spectrogram(data), chunks=4, axis=2,
                                      function=np.max)
def spectrogram_argmaxima(data: np.ndarray):
    """Wrapper to split data and apply max function for splits."""
    return mean_chunks_over_dimension(spectrogram(data), chunks=4, axis=2,
                                      function=np.argmax)


def fft(data: np.ndarray) -> np.ndarray:
    """
    Take fft of full data
    """
    assert data.shape[1:3] == (10,128)
    return np.abs(np.fft.fft(data, axis=2)[:,:64])

def integral_of_accerelation(data: np.ndarray) -> np.ndarray:
    """
    Integrate accerelation.
    """
    return np.cumsum(data[:,7:10,:], axis=2)

def norms_of_angvel_and_acc(data: np.ndarray) -> np.ndarray:
    """
    Take norm of angular velocity and accerelation. Note! norm of angular
    momentum is not very meaningful, also we do not know the object shape.
    """
    assert data.shape[1:3] == (10,128)
    out = np.empty((data.shape[0],2,128))
    # out[:,0,:] = np.sqrt(data[:,0,:]**2 + data[:,1,:]**2 + data[:,2,:]**2 +
    #                    data[:,3,:]**2)
    out[:,0,:] = np.sqrt(data[:,4,:]**2 + data[:,5,:]**2 + data[:,6,:]**2)
    out[:,1,:] = np.sqrt(data[:,7,:]**2 + data[:,8,:]**2 + data[:,9,:]**2)
    return out

def orientation_to_eluer_angles(data: np.ndarray):
    """
    Convert quaterion angles in data to euler angles.
    """
    assert data.shape[1:3] == (10,128)
    out = np.empty((data.shape[0],3,128))
    for i in range(data.shape[0]):
        angles = quaterion_to_euler_angle(data[i,0,:], data[i,1,:], data[i,2,:],
                                          data[i,3,:])
        out[i,0,:] = angles[0]
        out[i,1,:] = angles[1]
        out[i,2,:] = angles[2]

    return out

def quaterion_to_euler_angle(qx: tp.Union[float, np.ndarray],
                             qy: tp.Union[float, np.ndarray],
                             qz: tp.Union[float, np.ndarray],
                             qw: tp.Union[float, np.ndarray]) \
        -> tp.Tuple[tp.Union[float, np.ndarray],
                    tp.Union[float, np.ndarray],
                    tp.Union[float, np.ndarray]]:
    """
    Convert quaterion to euler angle
    :param qx:
    :param qy:
    :param qz:
    :param qw:
    :return:
    """
    # https://en.wikipedia.org/wiki/Conversion_between_quaternions_and_Euler_angles

    # roll (x-axis rotation)
    sinr_cosp = 2.0 * (qw * qx + qy * qz)
    cosr_cosp = 1.0 - 2.0 * (qx * qx + qy * qy)
    roll = np.arctan2(sinr_cosp, cosr_cosp)

    # pitch (y-axis rotation)
    sinp = 2.0 * (qw * qy - qz * qx)
    # mask replaces if-clause: if (np.abs(sinp) >= 1): (...) else: (...)
    mask = np.abs(sinp) >= 1
    pitch = \
        mask * np.copysign(np.pi / 2, sinp) + \
        (~mask * np.arcsin(sinp))
    # yaw (z-axis rotation)
    siny_cosp = 2.0 * (qw * qz + qx * qy)
    cosy_cosp = 1.0 - 2.0 * (qy * qy + qz * qz)
    yaw = np.arctan2(siny_cosp, cosy_cosp)

    return roll, pitch, yaw

