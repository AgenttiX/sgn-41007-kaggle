import numpy as np
import matplotlib.pyplot as plt

from alpi.tools.read_data import get_reordered_data_and_labels


def plot_5_samples(data, y_labels, group_labels, class_names, title="",
                   first_item=0):
    # print("y_labels:", y_labels)
    # print("classes:", class_names)

    samples = np.arange(first_item, first_item+5)
    fig, axs = plt.subplots(3, 5, figsize=(15, 7),
                            num=str(np.random.rand()))
    axs[0][0].set_ylabel("Orientations")
    axs[1][0].set_ylabel("Angular Velocity")
    axs[2][0].set_ylabel("Linear Acceleration")
    for i, s in enumerate(samples):
        axs[0][i].plot(data[s,0,:], label="X")
        axs[0][i].plot(data[s,1,:], label="Y")
        axs[0][i].plot(data[s,2,:], label="Z")
        axs[0][i].plot(data[s,3,:], label="W")
        axs[0][i].legend()
        axs[0][i].set_title("{}/1703, (g:{}) \n{}"
                     .format(s, group_labels[s], class_names[y_labels[s]]))
        axs[0][i].set_ylim((-1, 0.3))

        axs[1][i].plot(data[s,4,:], label="X")
        axs[1][i].plot(data[s,5,:], label="Y")
        axs[1][i].plot(data[s,6,:], label="Z")
        axs[1][i].legend()
        axs[1][i].set_ylim((-0.4, 0.4))

        axs[2][i].plot(data[s,7,:], label="X")
        axs[2][i].plot(data[s,8,:], label="Y")
        axs[2][i].plot(data[s,9,:], label="Z")
        axs[2][i].set_ylim((-20, 20))
        axs[2][i].legend()

    plt.suptitle(title)

def plot_spectrogram(data, y_labels, group_labels, class_names, title="",
                     first_item=0):
    from scipy import signal
    labels = [
        "Orientations x",
        "Orientations y",
        "Orientations z",
        "Orientations w",
        "Angular Velocity x",
        "Angular Velocity y",
        "Angular Velocity z",
        "Linear Acceleration x",
        "Linear Acceleration y",
        "Linear Acceleration z",
    ]

    fig, axs_2d_tuple = plt.subplots(3, 4, figsize=(15, 7),
                            num=str(np.random.rand()))
    axs = [ax for pair in axs_2d_tuple for ax in pair]  # flatten 2d tuple to 1d

    sample = data[first_item,...]
    assert sample.shape[1] == 128

    for i, l in enumerate(labels):
        f, t, Sxx = signal.spectrogram(sample[i,...], nperseg=30, noverlap=10)
        axs[i].pcolormesh(t, f, Sxx)
        axs[i].set_ylabel('Frequency [Hz]')
        axs[i].set_xlabel('Time multiplied by Hervanta constant')
        axs[i].set_title(l)

    fig.suptitle("{} for {}/1703, (g:{}) \n{}"
                 .format(title, first_item, group_labels[first_item],
                         class_names[y_labels[first_item]]))


def main():
    # data, y_labels, group_labels, class_names
    x, y, g, n = get_reordered_data_and_labels(
                                            data_path="../X_train_kaggle.npy",
                                            groups_csv_path="../groups.csv")
    sample_idx = np.random.randint(0, len(y)-5)  # random sample

    # Plot real space for 5 samples
    plot_5_samples(x, y, g, n, title="Real space", first_item=sample_idx)

    # Plot fft transform for 5 samples
    x_fft = np.fft.fft(x, axis=2)
    plot_5_samples(x_fft, y, g, n, title="Fourier", first_item=sample_idx)

    # Plot spectrograms for 1 sample
    plot_spectrogram(x, y, g, n, title="Spectrogram", first_item=sample_idx)

    plt.show()


if __name__ == "__main__":
    main()
