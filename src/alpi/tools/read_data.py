import typing as tp

import numpy as np


"""
This file contains functions for getting data in numpy arrays. Most considerable
functions:
    get_data_and_labels()           Get data in original order
    get_reordered_data_and_labels() Get data in order in which different 
                                    measurement groups are as far as possible 
                                    from each other.

"""


def get_class_names(name="groups.csv") \
        -> tp.Tuple[tp.List[str], tp.List[int], int]:
    """ Returns label names and the number of labels."""
    y_labels_str = set()
    measurement_groups = set()
    num_data = 0
    #
    with open(name) as file:
        next(file)  # skip first line
        for line in file:
            # print("'",line.strip(),"'")
            # input()
            parts = line.strip().split(",")
            if len(parts) == 3:
                y_labels_str.add(parts[2])
                measurement_groups.add(int(parts[1]))
                num_data += 1
            elif len(parts) == 0:
                break
            else:
                raise Exception("Wrong amount of columns ({} != {}) in file.\n"+
                                "Did you give groups.csv?".format(len(parts), 3)
                                )

    group_ids = list(measurement_groups)
    # groups must be in growing order without gaps in numbering
    assert(group_ids[-1] == len(group_ids) - 1 + group_ids[0])
    
    return list(y_labels_str), group_ids, num_data


def get_class_indices(classes: tp.List[str], 
                      num_data: int,
                      group_ids: tp.List[int],
                      name="groups.csv") \
        -> tp.Tuple[np.ndarray, np.ndarray]:
    """Reads data and and returns labels as integers.
        :return:
            y_labels:       [4  2  2  2  1   0 6 ...]
            group_labels:   [13 31 20 31 22  1 34...]
    """
    y_labels = np.empty(num_data, dtype=np.int8)
    group_labels = np.empty(num_data, dtype=np.int8)
    with open(name) as file:
        next(file)  # skip first line
        for i, line in enumerate(file):
            # print("'",line.strip(),"'")
            # input()
            parts = line.strip().split(",")
            if len(parts) == 3:
                y_labels[i] = classes.index(parts[2])
                group_labels[i] = group_ids.index(int(parts[1]))
            elif len(parts) == 0:
                break
            else:
                raise Exception("Wrong amount of columns ({} != {}) in file.\n"+
                                "Did you give groups.csv?".format(len(parts), 3)
                                )
    # make sure indexing to start from 0, though course data is already in that format
    group_labels -= group_ids[0]
    return y_labels, group_labels


def reorder_data(data, y_labels, group_labels):
    """Reorders data so that measurements from different groups are as close as
    possible.
    So if originally groups were in list so:
        [13 31 20 31 22 ...]
    Now they are in order
        [0 1 2 3 ... 34 35 0 1 2 ... 34 35 0 1 2 3 ... ... 21 34 3 7 ]
    """

    # I assume quicksort does the required scrambling for items in the same
    # group.
    ids = np.argsort(group_labels, kind="quicksort")
    y_labels_sort = y_labels[ids]
    group_labels_sort = group_labels[ids]

    # Find all iterators that are on the boundary of different measurement
    # groups
    iters = [0, ]
    itr = enumerate(group_labels_sort)
    _, g_old = next(itr)
    for i,g in itr:
        if g > g_old:
            iters.append(i)
        g_old = g

    # group must be on range [0, N-1]
    assert(group_labels_sort[iters[0]] == 0)

    # The following does the reordering. It iterates one value from each
    # measurement group and adds in list. This is done again and again until all
    # groups are empty.
    iters_upper = iters[1:].copy()
    iters_upper.append(len(group_labels_sort))
    iters_count = group_labels_sort[-1]
    itr_out = 0
    new_order = np.full(len(y_labels), fill_value=100000)
    while iters_count > 0:
        for i in range(len(iters)):
            if iters[i] is not None:
                x = iters[i]
                y = iters_upper[i]
                if iters[i] < iters_upper[i]:
                    new_order[itr_out] = iters[i]
                    iters[i] += 1
                    itr_out += 1
                else:
                    iters[i] = None
                    iters_count -= 1

    data_out = data[ids, ...][new_order, ...]
    y_labels_out = y_labels_sort[new_order]
    group_labels_out = group_labels_sort[new_order]

    return data_out, y_labels_out, group_labels_out


def get_list_of_single_groups(group_labels, y_labels, label_names,
                              print_stuff=False):

    num_labels = np.max(y_labels+1)
    num_groups = np.max(group_labels+1)

    samples_by_label = np.zeros(num_labels, dtype=np.int_)  # labels by class
    samples_by_group = np.zeros(num_groups, dtype=np.int_)  # labels by group
    groups_by_label = [[] for i in range(num_labels)]

    for i in range(len(y_labels)):
        samples_by_label[y_labels[i]] += 1
        samples_by_group[group_labels[i]] += 1

    for i in range(num_groups):
        index_of_group = group_labels.tolist().index(i)
        corresponding_label = y_labels[index_of_group]
        groups_by_label[corresponding_label].append(i)

    y_labels_that_have_single_group = []
    for i, grouplist in enumerate(groups_by_label):
         if len(grouplist) == 1:
            y_labels_that_have_single_group.append(i)

    if print_stuff:
        print_group_and_label_statistics(samples_by_label, samples_by_group,
                                         groups_by_label, group_labels,
                                         y_labels, label_names)
    return y_labels_that_have_single_group


def split_wood_label(y_labels, group_labels, print_info=False,
                     label_names=None):
    assert len(y_labels.shape) == 1
    assert np.min(y_labels) == 0
    assert np.min(group_labels) == 0

    wood_label_idx = get_list_of_single_groups(group_labels, y_labels,
                                               label_names,
                                               print_stuff=print_info)[0]

    ids = np.nonzero(y_labels == wood_label_idx)[0]
    second_half = ids[len(ids)//2:]

    new_label_for_wood = np.max(group_labels) + 1
    new_group_labels = group_labels.copy()
    new_group_labels[second_half] = new_label_for_wood


    # Assert wrong behaviour
    single_groups_on_new_data = get_list_of_single_groups(new_group_labels,
                                                          y_labels,
                                                          label_names,
                                                          print_stuff=print_info
                                                          )
    assert(len(single_groups_on_new_data) == 0)
    if print_info:
        # Proof that on y label 0 (wood) there's only one corresponding group
        print("Single group was: ", wood_label_idx)
        print("\n\n\n\n\n####### NEW GROUP SPLIT ##########")
        print("Single group with new data: ", single_groups_on_new_data)

    return new_group_labels



def print_group_and_label_statistics(samples_by_label, samples_by_group,
                                     groups_by_label, group_labels,
                                     y_labels, label_names):
    print("Number of measurements by label:\n    ", end="")
    for i, samplecount in enumerate(samples_by_label):
        print("{}:{}, ".format(i, samplecount), end="")
        if (i+1)%10 == 0:
            print("\n    ", end="")
    print("\n")
    print("Number of measurements by group:\n", end="")
    for i, samplecount in enumerate(samples_by_group):
        if label_names is not None:
            index_of_group = group_labels.tolist().index(i)
            corresponding_label = y_labels[index_of_group]
            print("     group label:{:2}, count:{:2}, y_label:{:2}, class_name:{}"
                  .format(i, samplecount, corresponding_label,
                          label_names[corresponding_label]))
        else:
            print("     {}:{}".format(i, samplecount))
        if (samplecount <= 1):
            print("<<<<<<<<<<<<<<<<<<<<<<<<<<<")

    print("Number of groups by label:\n", end="")
    for i, grouplist in enumerate(groups_by_label):
        if label_names is not None:
            print("     y label:{:2}, groups:{}, class_name:{}"
                  .format(i, grouplist,
                          label_names[i]))
        else:
            print("     {}:{}".format(i, label_names))


def get_data_and_labels(data_path="X_train_kaggle.npy",
                        groups_csv_path="groups.csv", split_wood=False,
                        print_info=False) \
        -> tp.Tuple[np.ndarray, np.ndarray, np.ndarray, tp.List[str]]:
    """
    Reads the csv data of competition. Returns floor labels, group labels and
    names corresponding floor labels. Indexing of both labels starts from 0.

    Return in original order.

    :param data_name:
    :return:
            X_train         array with shape (1703, 10, 128)        (reordered)
            y_labels:       [ 5  7  7  7  0  4...]
            group_labels:   [13 31 20 31 22  1...]
    """
    class_names, group_ids, num_data = get_class_names(groups_csv_path)
    y_labels, group_labels = get_class_indices(class_names, num_data,
                                               group_ids, groups_csv_path)
    data = np.load(data_path)

    if split_wood:
        group_labels = split_wood_label(y_labels, group_labels,
                                        print_info=print_info,
                                        label_names=class_names)

    return data, y_labels, group_labels, class_names


def get_reordered_data_and_labels(data_path="X_train_kaggle.npy",
                       groups_csv_path="groups.csv") \
        -> tp.Tuple[np.ndarray, np.ndarray, np.ndarray, tp.List[str]]:
    """
    Reads the csv data of competition. Returns floor labels, group labels and
    names corresponding floor labels. Indexing of both labels starts from 0.

    Also reorder data so that measurements from are as far as possible from each
    other.
    :param data_name:
    :return:
            X_train         array with shape (1703, 10, 128)        (reordered)
            y_labels_r:       [4  2  2  2  1   0 6 ...]               (reordered)
            group_labels_r:   [0 1 2 3 4 ... 34 35 0 1 2 ...]         (reordered)
    """
    class_names, group_ids, num_data = get_class_names(groups_csv_path)
    y_labels, group_labels = get_class_indices(class_names, num_data,
                                               group_ids, groups_csv_path,)
    data = np.load(data_path)
    data_r, y_labels_r, group_labels_r = reorder_data(data, y_labels,
                                                      group_labels)

    return data_r, y_labels_r, group_labels_r, class_names


def print_debug(data_path="X_train_kaggle.npy",
                groups_csv_path="groups.csv"):
    class_names, group_ids, num_data = get_class_names(groups_csv_path)
    y_labels, group_labels = get_class_indices(class_names, num_data,
                                               group_ids, groups_csv_path)
    data = np.load(data_path)
    data_r, y_labels_out, group_labels_out = reorder_data(data, y_labels,
                                                         group_labels)
    print("\ny_labels" , y_labels.shape)
    print(y_labels[:40])
    print(y_labels[-40:])
    print("\ny_labels_out" , y_labels_out.shape)
    print(y_labels_out[:40])
    print(y_labels_out[-40:])
    print("\ngroup_labels" , group_labels.shape)
    print(group_labels[:40])
    print(group_labels[-40:])
    print("\ngroup_labels_out" , group_labels_out.shape)
    print(group_labels_out[:40])
    print(group_labels_out[-40:])


def main():
    data1, y_labels1, group_labels1, class_names1 = get_data_and_labels(
                                            data_path="../X_train_kaggle.npy",
                                            groups_csv_path="../groups.csv",
                                            split_wood=True)
    data2, y_labels2, group_labels2, class_names2 = \
        get_reordered_data_and_labels(
            data_path="../X_train_kaggle.npy",
            groups_csv_path="../groups.csv")
    print("----------\n", data1.shape, y_labels1.shape, group_labels1.shape,
          class_names1)


if __name__ == "__main__":
    main()
