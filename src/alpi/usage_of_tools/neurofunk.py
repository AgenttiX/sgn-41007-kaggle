import numpy as np
from sklearn.model_selection import train_test_split
from keras.models import Sequential, load_model
from keras.layers.core import Dense, Dropout, Activation, Flatten
from keras.layers.convolutional import Conv2D, MaxPooling2D
from keras.preprocessing.image import ImageDataGenerator
from keras.utils import to_categorical

from os import path as path

def load_data():
    from task1 import dataloader
    from mika.splitting import split_by_group


    data = dataloader.train_data()
    X = data.x_data
    y = data.labels

    X = X[:,:,:,np.newaxis]

    y = to_categorical(y, num_classes=9)  # dtype='float32'

    #splitmethod = "raw"
    splitmethod = "mikas_splitter"

    if splitmethod == "raw":
        #fold = KFold(n_splits=4,random_state=123).split(X=X,y=y)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    elif splitmethod == "mikas_splitter":
        fold = split_by_group(data, n_splits=4)
        train_inds, test_inds = fold[0]
        X_train = X[train_inds, ...]
        y_train = y[train_inds, ...]
        X_test = X[test_inds, ...]
        y_test = y[test_inds, ...]
    else:
        assert False

    return X_train, X_test, y_train, y_test


def create_model(summary=False):
    filepath = "untrained_model.hdf5"

    if path.isfile(filepath):
        model = load_model(filepath)
    else:
        model = Sequential()

        N = 32
        # Number of feature maps
        w, h = 10, 5
        # Conv. window size
        model.add(Conv2D(N, (w, h),
                         input_shape=(10, 128,1),
                         activation = "relu",
                         padding = "same"))
        model.add(Conv2D(N, (w, h),
                         activation = "relu",
                         padding = "same"))
        model.add(MaxPooling2D(pool_size=(1, 4)))
        model.add(Dropout(0.25))

        model.add(Conv2D(2*N, (w, h),
                         activation = "relu",
                         padding = "same"))
        model.add(Conv2D(2*N, (w, h),
                         activation = "relu"))
        model.add(MaxPooling2D((1,4)))
        model.add(Dropout(0.25))

        model.add(Flatten())
        model.add(Dense(128, activation = "relu"))
        model.add(Dense(9, activation = "sigmoid"))

    if summary:
        model.summary()
    return model


def data_augmentation(X_train):
    datagen = ImageDataGenerator(
        featurewise_center=False,  # set input mean to 0 over the dataset
        samplewise_center=False,  # set each sample mean to 0
        featurewise_std_normalization=False,  # divide inputs by std of the dataset
        samplewise_std_normalization=False,  # divide each input by its std
        zca_whitening=False,  # apply ZCA whitening
        zca_epsilon=1e-06,  # epsilon for ZCA whitening
        rotation_range=0,  # randomly rotate images in the range (degrees, 0 to 180)
        # randomly shift images horizontally (fraction of total width)
        width_shift_range=0.3,
        # randomly shift images vertically (fraction of total height)
        height_shift_range=0.0,
        shear_range=0.,  # set range for random shear
        zoom_range=0.,  # set range for random zoom
        channel_shift_range=0.,  # set range for random channel shifts
        # set mode for filling points outside the input boundaries
        fill_mode='nearest',
        cval=0.,  # value used for fill_mode = "constant"
        horizontal_flip=True,  # randomly flip images
        vertical_flip=False,  # randomly flip images
        # set rescaling factor (applied before any other transformation)
        rescale=None,
        # set function that will be applied on each input
        preprocessing_function=None,
        # image data format, either "channels_first" or "channels_last"
        data_format=None,
        # fraction of images reserved for validation (strictly between 0 and 1)
        validation_split=0.0)

    return datagen

def train_model(X_train, X_test, y_train, y_test, new_model, generator=None):

    epochs=10000
    batch_size=20
    filepath = "model_cifar10_aug.hdf5"
    overwrite = True



    if path.isfile(filepath) and not overwrite:
        model = load_model(filepath)
    else:
        model = new_model
        # Compile and train
        model.compile(loss="categorical_crossentropy",
                      optimizer="sgd",
                      metrics = ["accuracy"])
        if generator is not None:
            generator.fit(X_train)
            model.fit_generator(generator.flow(X_train, y_train),
                                epochs=epochs,
                                validation_data = (X_test, y_test))
        else:
            model.fit(X_train, y_train,
                      epochs=epochs,
                      batch_size=batch_size,
                      validation_data=(X_test, y_test))

        model.save(filepath)



    return model



def main():

    new_model = create_model(summary=True)

    X_train, X_test, y_train, y_test = load_data()
    aug = data_augmentation(X_train)

    trained_model = train_model(X_train, X_test, y_train, y_test, new_model,
                                generator=aug)

    scores_real = trained_model.evaluate(X_test, y_test, verbose=1)
    for i, name in enumerate(trained_model.metrics_names):
        print("    Scores {}".format(name))
        print("        (real)   {:.3f}, ".format(scores_real[i]))



    # conf = (("model_raw_data.hdf5", None),
    #         ("model_generator.hdf5", get_generator()))
    #
    #
    # for filepath, gen in conf:
    #     print(filepath)
    #     x_train, x_test, y_train, y_test = load_data()
    #     model = p4(filepath)
    #     p5(x_train, x_test, y_train, y_test, model, filepath, gen)
    #     print("")

if __name__ == "__main__":
    main()
