"""
This file contains example usage of some of my functions.

All functions should (almost) self contained. Just make sure you have groups.csv
in parent folder.
"""


def try_random_forest():
    """
    Simple example of my data reader and usage of random forest.
    Has unreliably high accuracy compared to amount of code.
    """
    #from alpi.tools.read_data import get_data_and_labels
    from alpi.tools.read_data import get_reordered_data_and_labels
    import numpy as np
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.model_selection import cross_val_score, KFold

    # data, floor labels, group labels, floor names
    data, y_labels, _, _ = get_reordered_data_and_labels(
                                            data_path="../X_train_kaggle.npy",
                                            groups_csv_path="../groups.csv")
    data = data.reshape(data.shape[0], -1)   # flatten last 2 dimensions to 1d

    clf = RandomForestClassifier()
    division_generator = KFold(n_splits=3).split(y_labels)
    scores = cross_val_score(clf, data, y_labels, cv=division_generator)

    scores = np.array(scores)
    print("\nRandom forest accuracy: {:.3f}".format(scores.mean(),
                                                    scores.std()))


def plot_some_random_data():
    """"
    Plot real data, fft and spectogram.
    """
    from alpi.tools.plot_data import plot_5_samples, plot_spectrogram
    from alpi.tools.read_data import get_reordered_data_and_labels
    import numpy as np
    import matplotlib.pyplot as plt

    # data, y_labels, group_labels, class_names
    x, y, g, n = get_reordered_data_and_labels(
                                            data_path="../X_train_kaggle.npy",
                                            groups_csv_path="../groups.csv")
    sample_idx = np.random.randint(0, len(y)-5)  # random sample

    # Plot real space for 5 samples
    plot_5_samples(x, y, g, n, title="Real space", first_item=sample_idx)

    # Plot fft transform for 5 samples
    x_fft = np.fft.fft(x, axis=2)
    plot_5_samples(x_fft, y, g, n, title="Fourier", first_item=sample_idx)

    # Plot spectrograms for 1 sample
    plot_spectrogram(x, y, g, n, title="Spectrogram", first_item=sample_idx)

    plt.show()


def main():
    #try_random_forest()
    #print("\n----\n")
    try_random_forest()
    #plot_some_random_data()


if __name__ == "__main__":
    main()
