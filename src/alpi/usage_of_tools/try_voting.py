import numpy as np
#
# import sklearn.discriminant_analysis
# import sklearn.ensemble
# import sklearn.linear_model
# import sklearn.metrics
# import sklearn.model_selection
# import sklearn.svm
#
#
# from os import path as p2
# from sys import path as p1
# #p1.append(p2.join(p2.dirname(__file__), "GTSRB_subset"))
# p1.append(p2.dirname(p2.dirname(p2.dirname(p2.abspath(__file__)))))
# print("?",p2.dirname(p2.dirname(p2.dirname(p2.abspath(__file__)))) )
# print("!", p1)
# from task1 import splitting, dataloader, features
#
#
#
#
#
# def classify_features(data, clf, features):
#     """ Calculates a classify score with given data, classifier and feature
#     function.
#     Does use predict_proba(), rather than predict().
#     I.e.: class: "[0.0, 0.1, 0.9, 0.0, 0.0 ...]" rather than "2".
#     """
#     from sklearn.model_selection import GroupKFold
#     from alpi.tools.read_data import get_data_and_labels
#
#
#     X = data.x_data
#     y = data.labels
#
#     _, _, groups, _ = get_data_and_labels(
#                                             data_path="../X_train_kaggle.npy",
#                                             groups_csv_path="../groups.csv")
#
#     splits = 4
##
#     #fold = splitting.split_by_group(data, n_splits=10)
#     fold = GroupKFold(n_splits=splits).split(X=X, y=y, groups=groups)
#
#     scores = np.zeros((splits,))
#     for i, (train_inds, test_inds) in enumerate(fold):
#         feature_preds = np.empty((len(features),
#                                   len(test_inds),
#                                   len(data.classes())))
#
#
#         for j, (_name, feat) in enumerate(features):
#             feature_matrix = feat(X)
#             flat = feature_matrix.reshape(feature_matrix.shape[0], -1)
#             x_train = flat[train_inds, ...]
#             y_train = data.labels[train_inds, ...]
#             x_test = flat[test_inds, ...]
#             y_test = data.labels[test_inds, ...]
#
#             print("y_train", set(y_train))
#             print("y_test", set(y_test))
#
#             clf.fit(x_train, y_train)
#             y_pred = clf.predict_proba(x_test)
#             assert y_pred.shape == feature_preds.shape[1:]
#             feature_preds[j,:,:] = y_pred
#
#         sum_over_features = feature_preds.sum(axis=0)
#         arg_max = np.argmax(sum_over_features, axis=1)
#         scores[i] = sklearn.metrics.accuracy_score(y_test, arg_max)
#
#     print("scores", scores)
#     return scores.mean(), scores.std()
#
#
# def run_it(data: dataloader.Data):
#     import alpi.tools.feature_extraction as feature_extraction
#     import warnings
#     warnings.simplefilter("ignore")
#
#     clfs = [
#         ("LDA:", sklearn.discriminant_analysis.LinearDiscriminantAnalysis()),
#     ]
#     feature_extractors = [
#         ("Raw data", feature_extraction.do_nothing),
#         ("fft of all", feature_extraction.fft),
#         ("spectrogram of all", feature_extraction.spectrogram),
#         ("integral of lin.acc.", feature_extraction.integral_of_accerelation),
#         ("norms of ang.vel. & lin.acc.",
#             feature_extraction.norms_of_angvel_and_acc),
#         ("orientation in eluer angles",
#             feature_extraction.orientation_to_eluer_angles)
#     ]
#     print("Voting over all features")
#     for i, (clf_name, clf) in enumerate(clfs):
#         mean, std = classify_features(data, clf, feature_extractors)
#         if i == 0:
#             print("    {}".format(clf_name))
#         print("        : {:.3f}({:.3f})".format(mean, std))
#
# def main():
#     data = dataloader.train_data()
#     run_it(data)
#
# if __name__ == '__main__':
#     main()
#
#
