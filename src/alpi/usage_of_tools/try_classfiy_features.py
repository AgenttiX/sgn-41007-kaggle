import numpy as np
from alpi.tools.read_data import get_reordered_data_and_labels, get_data_and_labels
from sklearn.model_selection import cross_val_score, KFold, GroupKFold
from sklearn.metrics import accuracy_score

# Classifiers
from sklearn.ensemble import RandomForestClassifier
from sklearn.naive_bayes import GaussianNB
from sklearn.svm import SVC
from sklearn.neural_network import MLPClassifier

from sklearn.discriminant_analysis import LinearDiscriminantAnalysis

# Feature extractors
import alpi.tools.feature_extraction as feature_extraction




def classify_features(classifier, feature_matrix, ylabels, groups):
    """ Runs given feature with given classifier.
    """
    # from alpi.read_data import get_reordered_data_and_labels  # <-- or this
    from sklearn.model_selection import cross_val_score, KFold

    flat = feature_matrix.reshape(feature_matrix.shape[0], -1)

    #fold = KFold(n_splits=4).split(ylabels)
    fold = GroupKFold(n_splits=4).split(X=flat, y=ylabels,
                                        groups=groups)
    scores = cross_val_score(classifier, flat, ylabels, cv=fold)

    scores = np.array(scores)

    return scores.mean(), scores.std()

def main():
    # data, floor labels, group labels, floor names
    data, y_labels, groups, _ = get_data_and_labels(
                                            data_path="../X_train_kaggle.npy",
                                            groups_csv_path="../groups.csv")

    clfs = [
            ("SVM: (RBF)", SVC()),
            ("Random Forest", RandomForestClassifier()),
            ("Neural Net", MLPClassifier(alpha=1)),
            ("Naive Bayes", GaussianNB()),
            ]

    data_chunks = lambda X: feature_extraction\
                            .mean_chunks_over_dimension(X, chunks=4, axis=2)

    feature_extractors = [("Raw data", feature_extraction.do_nothing),
                          ("Mean chunks", data_chunks),
                          ("fft", feature_extraction.fft),
                          ("eluer angles", feature_extraction
                           .orientation_to_eluer_angles),
                          ("spectrogram", feature_extraction.spectrogram),
                          ("spectrogram chunks", feature_extraction.spectrogram_means),
                          ("spectrogram maxima", feature_extraction.spectrogram_maxima),
                          ("spectrogram argmaxima", feature_extraction
                           .spectrogram_argmaxima),
                          ("integral", feature_extraction.integral_of_accerelation),
                          ("norms", feature_extraction.norms_of_angvel_and_acc)]

    for clf_name, clf in clfs:
        for i ,(feat_name, feat) in enumerate(feature_extractors):
            feat_mat = feat(data)
            mean, std = classify_features(clf, feat_mat, y_labels, groups)

            if i == 0:
                print("\n{}".format(clf_name))

            print("     {} : {:.3f}({:.3f})".format(feat_name, mean, std))

if __name__ == "__main__":
    main()