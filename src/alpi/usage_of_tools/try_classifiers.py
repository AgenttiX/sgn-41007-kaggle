from alpi.tools.read_data import get_reordered_data_and_labels, get_data_and_labels
import numpy as np
from sklearn.neighbors import KNeighborsClassifier
from sklearn.svm import SVC                                             # Jatkoon --
from sklearn.gaussian_process import GaussianProcessClassifier
from sklearn.gaussian_process.kernels import RBF
from sklearn.tree import DecisionTreeClassifier                         # Jatkoon
from sklearn.ensemble import RandomForestClassifier, AdaBoostClassifier # Jatkoon ++
from sklearn.ensemble import GradientBoostingClassifier
from sklearn.neural_network import MLPClassifier                        # Jatkoon --
from sklearn.naive_bayes import GaussianNB                              # Jatkoon ++
from sklearn.discriminant_analysis import QuadraticDiscriminantAnalysis, LinearDiscriminantAnalysis
from sklearn.linear_model import LogisticRegression


from sklearn.model_selection import cross_val_score, KFold, GroupKFold
import time


def try_em_all():
    clfs = [("Nearest Neighbors", KNeighborsClassifier(3)),
            #("SVM: (Linear)", SVC(kernel="linear", C=0.025)),
            ("SVM: (RBF)", SVC()),
            # slow as fuck
            # ("Gaussian Process", GaussianProcessClassifier(1.0 * RBF(1.0))),
            ("Decision Tree", DecisionTreeClassifier()),
            ("Random Forest", RandomForestClassifier()),
            # slow
            #("GradientBoostingClassifier", GradientBoostingClassifier())
            ("Neural Net", MLPClassifier(alpha=1)),
            # slow
            #("AdaBoost", AdaBoostClassifier()),
            ("Naive Bayes", GaussianNB()),
            ("QDA", QuadraticDiscriminantAnalysis()),
            ("LinearDiscriminantAnalysis", LinearDiscriminantAnalysis()),
            ("LogisticRegression", LogisticRegression())]

    # data, floor labels, group labels, floor names
    data, y_labels, group_labels, class_names = get_reordered_data_and_labels(
                                            data_path="../X_train_kaggle.npy",
                                            groups_csv_path="../groups.csv")
    sh = data.shape
    data = data.reshape(sh[0], sh[1]*sh[2])   # flatten last dimension

    for i, (name, clf) in enumerate(clfs):
        print("Accuracy {:30}: ".format(name), end="")

        start = time.time()

        #fold = KFold(n_splits=3).split(y_labels)
        fold = GroupKFold(n_splits=4).split(X=data, y=y_labels,
                                            groups=group_labels)
        scores = cross_val_score(clf, data, y_labels, cv=fold)

        end = time.time()

        mean = sum(scores) / len(scores)
        print("{:.3f}, ({:.1f} s)".format(mean, end-start))

        print("")


"""
Accuracy Nearest Neighbors             : 0.265, (5.0 s)

Accuracy SVM: (Linear)                 : 0.393, (9.8 s)

Accuracy SVM: (RBF)                    : 0.596, (10.9 s)

Accuracy Gaussian Process              : 0.431, (607.6 s)

Accuracy Decision Tree                 : 0.608, (2.1 s)

Accuracy Random Forest                 : 0.667, (0.1 s)

Accuracy Neural Net                    : 0.504, (3.8 s)

Accuracy AdaBoost                      : 0.417, (23.3 s)

Accuracy Naive Bayes                   : 0.571, (0.2 s)

Accuracy QDA                           : 0.181, (0.4 s)

Accuracy LinearDiscriminantAnalysis    : 0.244, (1.6 s)

Accuracy LogisticRegression            : 0.328, (38.7 s)
"""


def testing1():
    X = np.vstack((np.arange(10,29), np.arange(-10,-29, -1))).T
    print(X)
    random_state = 12883823
    rkf = KFold(n_splits=3)
    for train, test in rkf.split(X):
        print("%s %s" % (train, test))


def main():
    try_em_all()
    # testing1()


if __name__ == "__main__":
    main()
