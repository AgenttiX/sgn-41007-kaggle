import unittest

import numpy as np

from alpi.tools.read_data import get_class_names, get_class_indices, reorder_data


class TestDataReorder(unittest.TestCase):
    """Test that reordering data does not destroy, create or change anything."""
    def __init__(self,*args, **kwargs):
        super(TestDataReorder, self).__init__(*args, **kwargs)
        data_path="X_train_kaggle.npy"
        groups_csv_path="groups.csv"
        self.class_names, self.group_ids, self.num_data = \
                get_class_names(groups_csv_path)
        self.y_labels, self.group_labels = get_class_indices(self.class_names,
                                                             self.num_data,
                                                             self.group_ids,
                                                             groups_csv_path)
        self.X_train = np.load(data_path)
        self.X_train_out, self.y_labels_out, self.group_labels_out = \
            reorder_data(self.X_train, self.y_labels, self.group_labels)

    def test_basic(self):
        self.assertTrue(self.y_labels_out.shape == self.y_labels.shape)
        self.assertTrue(self.group_labels.shape == self.group_labels_out.shape)
        self.assertTrue((self.y_labels <= 10).all()
                        and (self.group_labels <= 35).all())
        self.assertTrue((self.y_labels_out <= 10).all()
                        and (self.group_labels_out <= 35).all())

    def test_reordered_data_is_permutation(self):
        """
        Test if all data is the same after the reordering.

        :return:
        """
        # Using hashes of samples to create a distinct order for data
        hashes_orig = np.empty(len(self.y_labels), dtype=np.int_)
        hashes_new = np.empty(len(self.y_labels), dtype=np.int_)
        for i in range(len(self.y_labels)):
            # Using [i,:,::10] I crop some data out from hash
            hashes_orig[i] = hash(str(self.X_train[i,:,::30]))
            hashes_new[i] = hash(str(self.X_train_out[i,:,::30]))
        ids_orig = hashes_orig.argsort()
        ids_new = hashes_new.argsort()

        # Sort hashes to get both arrays (orig and new) in same order
        hashes_orig = hashes_orig[ids_orig]
        hashes_new = hashes_new[ids_new]

        # Sanity check: No two same hashes allowed:
        self.assertTrue((np.diff(hashes_orig) > 0).all())
        self.assertTrue((np.diff(hashes_new) > 0).all())

        # Test all data is same
        self.assertTrue((hashes_orig - hashes_new == 0).all())
        self.assertTrue((self.y_labels[ids_orig]
                         - self.y_labels_out[ids_new] == 0).all())
        self.assertTrue((self.group_labels[ids_orig]
                         - self.group_labels_out[ids_new] == 0).all())

    def test_that_new_order_is_well_distributed(self):
        sections = 0
        g_old = -1
        for g in self.group_labels_out:
            if g_old > g:
                sections += 1
            assert(g_old != g)
            g_old = g

        largest_interval = 0
        itr_interval_old = 0
        ids_orig_sorted = self.group_labels.argsort()
        sorted_group_labels = self.group_labels[ids_orig_sorted]
        for i in range(1, len(sorted_group_labels)):
            if (sorted_group_labels[i-1] < sorted_group_labels[i]) \
                    or i == len(sorted_group_labels) - 1:
                interval = i - itr_interval_old
                if largest_interval < interval:
                    largest_interval = interval
                itr_interval_old = i

        self.assertTrue(sections+1 == largest_interval)


if __name__ == '__main__':
    unittest.main()
