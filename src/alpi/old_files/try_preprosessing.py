""" Some runs with different preprocessing of data.
"""

def try_random_forest_with_reduced_data():
    """
    Shows how little amount of data is enough for pretty good accuracy.

    Trains random forest with:
        - 1 data point per sample,
        - 10 datapoints per sample and
        - all data points in sample.
    Do that both with
        - cross validifier and
        - manual train-test split.
    """
    from alpi.tools.read_data import get_data_and_labels
    # from alpi.tools.read_data import get_reordered_data_and_labels  # <-- or this
    import numpy as np
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.model_selection import cross_val_score, KFold
    from sklearn.metrics import accuracy_score

    test_size = 500

    # (You can plug your own data reader here)
    # data,  floor labels,  group labels,  floor names
    data0, y_labels0, _, _ = get_data_and_labels(
                                            data_path="../X_train_kaggle.npy",
                                            groups_csv_path="../groups.csv")
    train_data = data0[test_size:,:,:]

    # Train data
    train_y_labels  = y_labels0[test_size:]
    train_1_point   = train_data[:,0,0]
    train_10_points = train_data[:,:,0]
    train_all_points= train_data.copy()
    # Flatten last 2 dimensions to 1d
    train_1_point    = train_1_point.reshape(train_1_point.shape[0], -1)
    train_10_points  = train_10_points.reshape(train_10_points.shape[0], -1)
    train_all_points = train_all_points.reshape(train_all_points.shape[0], -1)

    # Test data
    test_labels     = y_labels0[:test_size]
    test_1_point    = data0[:test_size,0,0]
    test_10_points  = data0[:test_size,:,0]
    test_all_points = data0[:test_size,:,:]
    # Flatten last 2 dimensions on test data
    test_1_point    = test_1_point.reshape(test_1_point.shape[0], -1)
    test_10_points  = test_10_points.reshape(test_10_points.shape[0], -1)
    test_all_points = test_all_points.reshape(test_all_points.shape[0], -1)

    print("Yes, data is really ridiculously small")
    print("     (original:", data0.shape, ")")
    print("     train_1_point.shape:", train_1_point.shape)
    print("     train_10_points.shape:", train_10_points.shape)
    print("     train_all_points.shape:", train_all_points.shape)

    # Use cross validifier:
    if True:
        print("")
        print("Accuracy with cross validifier:")

        clf1 = RandomForestClassifier()
        clf2 = RandomForestClassifier()
        clf3 = RandomForestClassifier()

        scores_1_point    = cross_val_score(clf1, train_1_point, train_y_labels,
                                      cv=KFold(n_splits=3).split(train_y_labels))
        scores_10_points  = cross_val_score(clf2, train_10_points, train_y_labels,
                                      cv=KFold(n_splits=3).split(train_y_labels))
        scores_all_points = cross_val_score(clf3, train_all_points, train_y_labels,
                                      cv=KFold(n_splits=3).split(train_y_labels))

        scores_1_point    = np.array(scores_1_point)
        scores_10_points  = np.array(scores_10_points)
        scores_all_points = np.array(scores_all_points)

        print("    scores_1_point    {:.3f}".format(scores_1_point.mean(),
                                                    scores_1_point.std()))
        print("    scores_10_points  {:.3f}".format(scores_10_points.mean(),
                                                    scores_10_points.std()))
        print("    scores_all_points {:.3f}".format(scores_all_points.mean(),
                                                    scores_all_points.std()))

    # Validify by hand on test data that is not included in training:
    if test_size > 0:
        clf1 = RandomForestClassifier()
        clf2 = RandomForestClassifier()
        clf3 = RandomForestClassifier()

        # Train
        clf1.fit(train_1_point, train_y_labels)
        clf2.fit(train_10_points, train_y_labels)
        clf3.fit(train_all_points, train_y_labels)

        # Prediction accuracy
        a1 = accuracy_score(clf1.predict(test_1_point), test_labels)
        a2 = accuracy_score(clf2.predict(test_10_points), test_labels)
        a3 = accuracy_score(clf3.predict(test_all_points), test_labels)

        print("")
        print("Accuracy with manual train-test division")
        print("     {:.3f}".format(a1))
        print("     {:.3f}".format(a2))
        print("     {:.3f}".format(a3))


def all_3(skip_every_nth=1, test_size=200):
    """
    Trains random forest with:
        - real data,
        - fft and
        - both real and fft.
    """
    import numpy as np
    from alpi.tools.read_data import get_reordered_data_and_labels, get_data_and_labels
    from sklearn.ensemble import RandomForestClassifier
    from sklearn.model_selection import cross_val_score, KFold
    from sklearn.metrics import accuracy_score


    # data, floor labels, group labels, floor names
    data0, y_labels0, group_labels, class_names = get_reordered_data_and_labels()  # _reordered
    # data0, y_labels0, group_labels, class_names = get_data_and_labels()
    data0 = data0[:,:,::skip_every_nth]

    # Train data
    y_labels = y_labels0[test_size:]
    data = data0[test_size:,:,::]
    data_real = data.reshape(data.shape[0], -1)          # flatten last 2 dimensions to 1d
    data_fft = np.fft.fft(data, axis=2)[:,:,:64]
    data_fft = data_fft.reshape(data_fft.shape[0], -1)    # flatten last 2 dimensions to 1d
    data_stack = np.hstack((data_real, data_fft))
    data_stack = data_stack.reshape(data_stack.shape[0], -1)

    # Yes, data is really ridiculously small when you set skip_every_nth=1000
    print("(data0.shape", data0.shape, ")")
    print("data_real.shape", (data_real).shape)
    print("data_fft.shape", (data_fft).shape)
    print("both.shape", np.hstack((data_real, data_fft)).shape)

    clf1 = RandomForestClassifier()
    clf2 = RandomForestClassifier()
    clf3 = RandomForestClassifier()
    scores_real = cross_val_score(clf1, data_real, y_labels,
                                  cv=KFold(n_splits=3).split(y_labels))
    scores_fft = cross_val_score(clf2, data_fft, y_labels,
                                 cv=KFold(n_splits=3).split(y_labels))
    scores_both = cross_val_score(clf3, data_stack, y_labels,
                                  cv=KFold(n_splits=3).split(y_labels))

    scores_real = np.array(scores_real)
    scores_fft = np.array(scores_fft)
    scores_both = np.array(scores_both)
    print("\nRandomForestClassifier")
    print("    Real space {:.3f}".format(scores_real.mean(), scores_real.std()))
    print("    Fourier {:.3f}".format(scores_fft.mean(), scores_fft.std()))
    print("    Both {:.3f}".format(scores_both.mean(), scores_both.std()))

    ####

    if test_size > 0:

        clf1 = RandomForestClassifier()
        clf2 = RandomForestClassifier()
        clf3 = RandomForestClassifier()

        clf1.fit(data_real, y_labels)
        clf2.fit(data_fft, y_labels)
        clf3.fit(data_stack, y_labels)
        test_data0 = data0[:test_size,:,::skip_every_nth]
        test_real = test_data0.reshape(test_data0.shape[0], -1)
        test_fft = np.fft.fft(test_data0, axis=2)[:,:,:64]
        test_fft = test_fft.reshape(test_fft.shape[0], -1)   # flatten last 2 dimensions to 1d
        test_stack = np.hstack((test_real, test_fft))
        test_stack = test_stack.reshape(test_stack.shape[0], -1)

        test_labels = y_labels0[:test_size]
        y_pred1 = clf1.predict(test_real)
        y_pred2 = clf2.predict(test_fft)
        y_pred3 = clf3.predict(test_stack)
        a1 = accuracy_score(y_pred1, test_labels)
        a2 = accuracy_score(y_pred2, test_labels)
        a3 = accuracy_score(y_pred3, test_labels)

        print("Real accuracy of these shits: {:.4f} {:.4f} {:.4f}".format(a1, a2, a3))




def main():
    try_random_forest_with_reduced_data()
    all_3(1, test_size=500)


if __name__ == "__main__":
    main()
