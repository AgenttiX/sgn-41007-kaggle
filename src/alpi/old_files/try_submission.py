import typing as tp
import numpy as np

from sklearn.ensemble import RandomForestClassifier
from sklearn.model_selection import cross_val_score, KFold
from sklearn.metrics import accuracy_score

from alpi.tools.read_data import get_reordered_data_and_labels, get_data_and_labels


def real_and_fourier(skip_every_nth=1, test_size=200):
    """
    Trains random forest with concatenated real + fft data.
    """

    # data, floor labels, group labels, floor names
    #data0, y_labels0, group_labels, class_names = get_reordered_data_and_labels()  # _reordered
    data0, y_labels0, group_labels, class_names = get_data_and_labels()
    data0 = data0[:,:,::skip_every_nth]

    # Train data
    y_labels = y_labels0[test_size:]
    data = data0[test_size:,:,:]
    data_real = data.reshape(data.shape[0], -1)          # flatten last 2 dimensions to 1d
    data_fft = np.fft.fft(data, axis=2)
    data_fft = data_fft.reshape(data_fft.shape[0], -1)    # flatten last 2 dimensions to 1d
    data_stack = np.hstack((data_real, data_fft))
    data_stack = data_stack.reshape(data_stack.shape[0], -1)

    clf3 = RandomForestClassifier()
    scores_both = cross_val_score(clf3, data_stack, y_labels,
                                  cv=KFold(n_splits=3).split(y_labels))

    scores_both = np.array(scores_both)
    print("\nRandomForestClassifier")
    print("    Both {:.3f}".format(scores_both.mean(), scores_both.std()))

    ####
    clf_test = RandomForestClassifier()
    clf_test.fit(data_stack, y_labels)
    if test_size > 0:
        test_data0 = data0[:test_size,:,::skip_every_nth]
    else:
        test_data0 = np.load("X_test_kaggle.npy")
    test_data_flat = test_data0.reshape(test_data0.shape[0], -1)
    test_fft = np.fft.fft(test_data0, axis=2)
    test_fft_flat = test_fft.reshape(test_fft.shape[0], -1)   # flatten last 2 dimensions to 1d
    test_stack = np.hstack((test_data_flat, test_fft_flat))
    test_stack_flat = test_stack.reshape(test_stack.shape[0], -1)

    y_pred3 = clf_test.predict(test_stack_flat)

    if test_size > 0:
        test_labels = y_labels0[:test_size]
        a3 = accuracy_score(y_pred3, test_labels)
        print("Real accuracy of these shits: {:.4f}".format(a3))

    create_submission("sub_alpi_1_randomforest_realfft.csv",
                      y_pred3,
                      class_names)

    # clf3 = RandomForestClassifier()

    # create_submission()


def create_submission(name: str,
                      y_labels: np.ndarray,
                      label_names: tp.List[str]):
    with open(name, "w") as fp:
        fp.write("# Id,Surface\n")
        for i, label in enumerate(y_labels):
            fp.write("%d,%s\n" % (i, label_names[label]))


def main():
    real_and_fourier(1, test_size=0)


if __name__ == "__main__":
    main()
