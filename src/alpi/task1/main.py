
def load_data(splitmethod = "mikas_splitter"):
    """
    :param splitmethod: "mikas_splitter" / "raw"
    :return:
    """

    from task1 import dataloader
    from mika.splitting import split_by_group
    from keras.utils import to_categorical
    from sklearn.model_selection import train_test_split
    import numpy as np


    data = dataloader.train_data()
    X = data.x_data
    y = data.labels

    y = to_categorical(y, num_classes=9)  # dtype='float32'

    if splitmethod == "raw":
        #fold = KFold(n_splits=4,random_state=123).split(X=X,y=y)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    elif splitmethod == "mikas_splitter":
        fold = split_by_group(data, n_splits=4)
        train_inds, test_inds = fold[0]
        X_train = X[train_inds, ...]
        y_train = y[train_inds, ...]
        X_test = X[test_inds, ...]
        y_test = y[test_inds, ...]
    else:
        assert False

    return X_train, X_test, y_train, y_test

def plot_history(history, key='categorical_crossentropy'):
    # https://www.tensorflow.org/tutorials/keras/overfit_and_underfit
    import matplotlib.pyplot as plt
    plt.figure(figsize=(16,10))

    val = plt.plot(history.epoch, history.history['val_'+key],
               '--', label=' Val')
    plt.plot(history.epoch, history.history[key], color=val[0].get_color(),
         label=' Train')

    plt.xlabel('Epochs')
    plt.ylabel(key.replace('_',' ').title())
    plt.legend()

    plt.xlim([0,max(history.epoch)])
    plt.show()


def LSTM(
        reg = 0.01,
        drop = 0.2,
        batch = 256,
        epoch = 100,
        layers = 16
):
    # https://stats.stackexchange.com/questions/274478/understanding-input-shape-parameter-in-lstm-with-keras
    from keras.models import Sequential
    from keras.layers import LSTM, Dense, Dropout, Flatten
    from keras import regularizers
    import numpy as np


    X_train, X_test, y_train, y_test = load_data()
    X_train = np.swapaxes(X_train, 1, 2)
    X_test = np.swapaxes(X_test, 1, 2)

    comment = " 3 LSTM layers"


    data_dim = X_train.shape[2]
    timesteps = X_train.shape[1]
    num_classes = 9

    # expected input data shape: (batch_size, timesteps, data_dim)
    model = Sequential()
    model.add(LSTM(layers, return_sequences=True,
                   input_shape=(timesteps, data_dim),
                   kernel_regularizer=regularizers.l1(reg)))
    #model.add(Dropout(drop))
    model.add(LSTM(layers, return_sequences=True,
                   kernel_regularizer=regularizers.l1(reg)))
    model.add(LSTM(layers, return_sequences=True,
                   kernel_regularizer=regularizers.l1(reg)))
    #model.add(LSTM(layers, return_sequences=True,
    #               kernel_regularizer=regularizers.l1(reg)))
    #model.add(Dropout(drop))
    model.add(LSTM(layers, kernel_regularizer=regularizers.l1(reg)))
    model.add(Dropout(drop))


    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy', 'categorical_crossentropy'])

    history = model.fit(X_train, y_train,
                        batch_size=batch, epochs=epoch,
                        validation_data=(X_test, y_test))

    scores_real = model.evaluate(X_test, y_test, verbose=1)

    np.set_printoptions(formatter={'float': lambda x: "{0: 7.3f}".format(x)})
    out_list = [
        "\nLSTM (" + comment + ")",
        ("reg {}, drop {}, batch {}, epoch {}, layers {}"
            .format(reg, drop, batch, epoch, layers)),
        ("    acc: {}".format(np.array(history.history['acc']))),
        ("    val_acc {}\n".format(np.array(history.history['val_acc']))),
        ]
    #for i, name in enumerate(model.metrics_names):
    #    out_list.append("    Score {} : {:.3f}".format(name, scores_real[i]))

    out_str = "\n".join(out_list)

    print(out_str)

    with open("log.txt", "a") as f:
        f.write(out_str)




def CNN_and_GRU(
        reg = 0.01,
        drop = 0.2,
        batch = 256,
        epoch = 16,
        layers = 32
):
    # https://stats.stackexchange.com/questions/274478/understanding-input-shape-parameter-in-lstm-with-keras
    from keras.models import Sequential, Model, Input
    from keras.layers import GRU, Dense, Dropout, Concatenate, Flatten, Reshape
    from keras.layers.convolutional import Conv1D, MaxPooling2D
    from keras import regularizers
    import numpy as np


    X_train, X_test, y_train, y_test = load_data()
    #X_train = X_train[:,:,:,np.newaxis]  # emulate color channel
    #X_test = X_test[:,:,:,np.newaxis]  # emulate color channel
    X_train = np.swapaxes(X_train, 1, 2)
    X_test = np.swapaxes(X_test, 1, 2)

    comment = "CNN + GRU, var params"

    data_dim = X_train.shape[2]
    timesteps = X_train.shape[1]
    num_classes = 9
    conv_shape = (10,)

    model = Sequential()

    model.add(Conv1D(layers, conv_shape, activation = "relu",
                     input_shape=(timesteps, data_dim),
                     padding = "same",
                     data_format='channels_last',
                     #kernel_regularizer=regularizers.l1(reg*0.1)
                     ))
    model.add(Conv1D(1, conv_shape, activation = "relu", padding = "same",
                     #kernel_regularizer=regularizers.l1(reg*0.1)
                     ))
    model.add(Dropout(drop))

    #model.add(Reshape((timesteps, data_dim)))

    model.add(GRU(output_dim=data_dim, return_sequences=True,
                   #input_shape=(timesteps, data_dim),
                   kernel_regularizer=regularizers.l1(reg)))
    model.add(Dropout(drop))
    model.add(GRU(output_dim=data_dim, kernel_regularizer=regularizers.l1(reg)))
    model.add(Dropout(drop))
    model.add(Dense(num_classes, activation='softmax'))

    #inp = Input(shape=(10, 128,1))
    #model = Model(inputs=inp, outputs=Concatenate([image_model, gru_model]))

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    history = model.fit(X_train, y_train,
                        batch_size=batch, epochs=epoch,
                        validation_data=(X_test, y_test))

    #scores_real = model.evaluate(X_test, y_test, verbose=1)

    np.set_printoptions(formatter={'float': lambda x: "{0: 7.3f}".format(x)})
    out_list = [
        "\nGRU (" + comment + ")",
        ("reg {}, drop {}, batch {}, epoch {}, layers {}"
            .format(reg, drop, batch, epoch, layers)),
        ("    acc: {}".format(np.array(history.history['acc']))),
        ("    val_acc {}\n".format(np.array(history.history['val_acc']))),
        ]
    #for i, name in enumerate(model.metrics_names):
    #    out_list.append("    Score {} : {:.3f}".format(name, scores_real[i]))

    out_str = "\n".join(out_list)

    print(out_str)

    logfile_name = "log_" + str(int(np.log10(epoch))) + ".txt"

    with open(logfile_name, "a") as f:
        f.write(out_str)


def LSTM_by_joonas_modified(
        reg = 0.01,
        drop = 0.4,
        batch = 256,
        epoch = 10,
        layers = 16
):
    # https://stats.stackexchange.com/questions/274478/understanding-input-shape-parameter-in-lstm-with-keras
    from keras.models import Sequential
    from keras.layers import LSTM, Dense, Dropout, Flatten
    from keras import regularizers
    import numpy as np


    X_train, X_test, y_train, y_test = load_data()
    X_train = np.swapaxes(X_train, 1, 2)
    X_test = np.swapaxes(X_test, 1, 2)

    comment = "LSTM_by_joonas_modified"


    data_dim = X_train.shape[2]
    timesteps = X_train.shape[1]
    num_classes = 9

    # expected input data shape: (batch_size, timesteps, data_dim)
    model = Sequential()
    model.add(LSTM(30, return_sequences=False,
                   input_shape=(timesteps, data_dim),
                   kernel_regularizer=regularizers.l1(reg)))

    model.add(Dense(50, activation='sigmoid'))
    model.add(Dropout(drop))
    model.add(Dense(50, activation='sigmoid'))
    model.add(Dropout(drop))

    model.add(Dense(num_classes, activation='softmax'))

    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy', 'categorical_crossentropy'])

    history = model.fit(X_train, y_train,
                        batch_size=batch, epochs=epoch,
                        validation_data=(X_test, y_test))

    np.set_printoptions(formatter={'float': lambda x: "{0: 7.3f}".format(x)})
    out_list = [
        "\nLSTM (" + comment + ")",
        ("reg {}, drop {}, batch {}, epoch {}, layers {}"
            .format(reg, drop, batch, epoch, layers)),
        ("    acc: {}".format(np.array(history.history['acc']))),
        ("    val_acc {}\n".format(np.array(history.history['val_acc']))),
        ]
    #for i, name in enumerate(model.metrics_names):
    #    out_list.append("    Score {} : {:.3f}".format(name, scores_real[i]))

    out_str = "\n".join(out_list)

    print(out_str)

    logfile_name = "log_" + str(int(np.log10(epoch))) + ".txt"
    with open(logfile_name, "a") as f:
        f.write(out_str)

def LSTM_by_joonas():
    # https://stats.stackexchange.com/questions/274478/understanding-input-shape-parameter-in-lstm-with-keras
    from keras.models import Sequential
    from keras.layers import LSTM, Dense, Dropout
    import numpy as np
    from task1.submission import create_submission
    from task1.dataloader import test_data
    from keras import regularizers




    X_train, X_test, y_train, y_test = load_data()
    X_train = np.swapaxes(X_train, 1, 2)
    X_test = np.swapaxes(X_test, 1, 2)

    #X_train = X_train[:, :, 4:]
    #X_test = X_test[:, :, 4:]
    print(X_train.shape)

    data_dim = X_train.shape[2]
    timesteps = X_train.shape[1]
    num_classes = 9

    # expected input data shape: (batch_size, timesteps, data_dim)
    model = Sequential()
    model.add(LSTM(50, return_sequences=False,
                   input_shape=(timesteps, data_dim)))  # returns a sequence of vectors of dimension 32
    model.add(Dropout(0.2))

    #model.add(LSTM(40, return_sequences=True, kernel_regularizer='l1'))  # returns a sequence of vectors of dimension 32
    #model.add(Dropout(0.2))

    #model.add(LSTM(20, return_sequences=False, kernel_regularizer='l1'))  # returns a sequence of vectors of dimension 32
    #model.add(Dropout(0.2))

    model.add(Dense(200, activation='sigmoid'))
    model.add(Dropout(0.2))

    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    model.fit(X_train, y_train,
              batch_size=252, epochs=30,
              validation_data=(X_test, y_test))

    scores_real = model.evaluate(X_test, y_test, verbose=1)
    for i, name in enumerate(model.metrics_names):
        print("    Scores {}".format(name))
        print("        (real)   {:.3f}, ".format(scores_real[i]))
    model.save('testmodel.h5')
    X_real_test = test_data().x_data
    print(X_real_test.shape)
    X_real_test = np.swapaxes(X_real_test, 1, 2)
    print(X_real_test.shape)
    y_pred = model.predict(X_real_test)
    y_pred = np.argmax(y_pred, axis=1)
    print(y_pred.shape)
    create_submission("testsub.csv", y_pred)

def main():
    #LSTM()
    CNN_and_GRU()
    #LSTM_by_joonas()
    #LSTM_by_joonas_modified()


if __name__ == "__main__":
    main()