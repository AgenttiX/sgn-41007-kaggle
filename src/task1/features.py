import numpy as np

import sklearn.preprocessing


# Feature extractors

def reshaped(data: np.ndarray) -> np.ndarray:
    """For task 1 part 4a"""
    if data.ndim != 3:
        raise ValueError("Invalid data")

    return np.reshape(
        data,
        newshape=(data.shape[0], data.shape[1] * data.shape[2])
    )


def normed(data) -> np.ndarray:
    """Get normed data (=normalisoitu)

    :param data: input data (if not given, use self.x_data)
    :return: normed data
    """
    normed = np.zeros_like(data)
    scaler = sklearn.preprocessing.StandardScaler()
    for i in range(data.shape[0]):
        normed[i, ...] = scaler.fit_transform(data[i, ...])
    return normed


def fft(data: np.ndarray) -> np.ndarray:
    """Get FFT transformed x data"""
    return np.abs(np.fft.fft(data, axis=2))


def means(data: np.ndarray) -> np.ndarray:
    """For task 1 part 4b"""
    return np.mean(data, axis=2)


def stdevs(data: np.ndarray) -> np.ndarray:
    """For task 1 part 4c"""
    if data.ndim != 3:
        raise ValueError("Invalid data")
    return np.std(data, axis=2)


def means_stdevs(data: np.ndarray):
    """For task 1 part 4c"""
    return np.concatenate((means(data), stdevs(data)), axis=1)


# Data creators

def white_noise(data: np.ndarray, multiplier: float = 0.01) -> np.ndarray:
    # Count standard deviations for each sensor per sample
    std = np.std(data, axis=2)
    # Compute the averages of these per-sensor standard deviations
    # std_means = np.mean(std, axis=0)
    # print(std_means)

    ret = data + np.random.rand(*data.shape) * std[..., np.newaxis] * multiplier

    if ret.shape != data.shape:
        raise RuntimeError("Output shape should match the input")

    return ret


def gaussian_noise(data: np.ndarray, multiplier: float = 0.01, width: float = 1.0) -> np.ndarray:
    std = np.std(data, axis=2)

    ret = data + np.random.normal(scale=width, size=data.shape) * std[..., np.newaxis] * multiplier

    if ret.shape != data.shape:
        raise RuntimeError("Output shape should match the input")

    return ret


if __name__ == "__main__":
    from task1 import dataloader

    d = dataloader.train_data()

    noisy = white_noise(d.x_data)
    relative_diffs = np.mean(means(d.x_data - noisy) / means(d.x_data), axis=0)
    print(relative_diffs)

    gauss = gaussian_noise(d.x_data)
    relative_gauss_diffs = np.mean(means(d.x_data - gauss) / means(d.x_data), axis=0)
    print(relative_gauss_diffs)
