import numpy as np

import sklearn.discriminant_analysis
import sklearn.ensemble
import sklearn.linear_model
import sklearn.metrics
import sklearn.model_selection
import sklearn.svm

from task1 import splitting, dataloader, features


def classify_linear(x_train, x_test, y_train, y_test):
    """Task 1 part 4"""
    clf = sklearn.discriminant_analysis.LinearDiscriminantAnalysis()

    clf.fit(x_train, y_train)
    y_pred = clf.predict(x_test)

    score = sklearn.metrics.accuracy_score(y_test, y_pred)
    return score


def classify_features(data, clf, feat):
    """ Calculates a classify score with given data, classifier and feature
    function.
    """
    from sklearn.model_selection import cross_val_score

    X = data.x_data
    y = data.labels
    feature_matrix = feat(X)

    flat = feature_matrix.reshape(feature_matrix.shape[0], -1)
    #fold = GroupKFold(n_splits=4).split(X=flat, y=ylabels, groups=groups)
    fold = splitting.split_by_group(data, n_splits=10)
    scores = cross_val_score(clf, flat, y, cv=fold)
    scores = np.array(scores)

    return scores.mean(), scores.std()


def classify(x_train, x_test, y_train, y_test):
    """Task 1 part 5"""
    classifiers = [
        # a)
        sklearn.discriminant_analysis.LinearDiscriminantAnalysis(),
        # b)
        sklearn.svm.SVC(
            kernel="linear",
            gamma="scale"
        ),
        # c)
        sklearn.svm.SVC(
            kernel="rbf",
            gamma="scale"
        ),
        # d)
        sklearn.linear_model.LogisticRegression(
            solver="lbfgs",
            multi_class="auto",  # To silence Scikit warnings (this will be the new default option)
            # max_iter=1000     # Increasing this value appears not to help with ConvergenceWarning
        ),
        # e)
        sklearn.ensemble.RandomForestClassifier(
            n_estimators=100
        )
    ]

    scores = np.zeros((len(classifiers), ))
    for i, clf in enumerate(classifiers):
        clf.fit(x_train, y_train)
        y_pred = clf.predict(x_test)

        score = sklearn.metrics.accuracy_score(y_test, y_pred)
        scores[i] = score

    return scores


def part4(data: dataloader.Data):
    import warnings
    warnings.simplefilter("ignore")

    splits = splitting.split_by_group(data, n_splits=10)

    reshaped = features.reshaped(data.x_data)
    means = features.means(data.x_data)
    means_stdevs = features.means_stdevs(data.x_data)

    scores_reshaped = []
    scores_means = []
    scores_means_stdevs = []

    for train_inds, test_inds in splits:
        # x_train = data.x_data[train_inds, ...]
        y_train = data.labels[train_inds, ...]
        # x_test = data.x_data[test_inds, ...]
        y_test = data.labels[test_inds, ...]

        # a)
        reshaped_train = reshaped[train_inds, ...]
        reshaped_test = reshaped[test_inds, ...]

        score_res = classify_linear(reshaped_train, reshaped_test, y_train, y_test)
        scores_reshaped.append(score_res)

        # b)
        means_train = means[train_inds, ...]
        means_test = means[test_inds, ...]

        score_mean = classify_linear(means_train, means_test, y_train, y_test)
        scores_means.append(score_mean)

        # c)
        mstd_train = means_stdevs[train_inds, ...]
        mstd_test = means_stdevs[test_inds, ...]

        score_mstd = classify_linear(mstd_train, mstd_test, y_train, y_test)
        scores_means_stdevs.append(score_mstd)

        # d)
        # part_4d(data)
        # TODO: d part with FFT, derivative of acceleration, integral of acceleration
        # TODO: for peaks: width, height
        # Taajuusjakauma, kuinka paljon korkeita ja matalia taajuuksia

    a = np.array(scores_reshaped)
    b = np.array(scores_means)
    c = np.array(scores_means_stdevs)
    print("Problem 4")
    print("    a) reshaped : {:.3f}({:.3f})".format(a.mean(), a.std()))
    print("    b) means : {:.3f}({:.3f})".format(b.mean(), b.std()))
    print("    c) means & stdevs : {:.3f}({:.3f})".format(a.mean(), c.std()))


def part_4d(data: dataloader.Data):
    import alpi.tools.feature_extraction as feature_extraction
    import warnings
    warnings.simplefilter("ignore")

    clfs = [
        ("LDA:", sklearn.discriminant_analysis.LinearDiscriminantAnalysis())
    ]
    feature_extractors = [
        ("Raw data", feature_extraction.do_nothing),
        ("orientation in euler angles",
            feature_extraction.orientation_to_eluer_angles),
        ("fft of all", feature_extraction.fft),
        ("spectrogram all", feature_extraction.spectrogram),
        #("spectrogram mean", feature_extraction.spectrogram_means),
        #("spectrogram maxima", feature_extraction.spectrogram_maxima),
        #("spectrogram argmaxima", feature_extraction.spectrogram_argmaxima),
        ("integral of lin.acc.", feature_extraction.integral_of_accerelation),
        ("norms of ang.vel. & lin.acc.",
            feature_extraction.norms_of_angvel_and_acc),
        ("norm with StandardScaler", features.normed),
    ]
    print("    d)")
    for clf_name, clf in clfs:
        for i, (feature_name, feat) in enumerate(feature_extractors):

            mean, std = classify_features(data, clf, feat)
            if i == 0:
                print("      {}".format(clf_name))
            print("          {} : {:.3f}({:.3f})".format(feature_name, mean, std))


def part5(data: dataloader.Data):
    splits = splitting.split_by_group(data, n_splits=4)
    scores = []

    for train_inds, test_inds in splits:
        x_train, x_test, y_train, y_test = data.get_x_y(train_inds, test_inds)

        reshaped_train = features.reshaped(x_train)
        reshaped_test = features.reshaped(x_test)

        iter_scores = classify(reshaped_train, reshaped_test, y_train, y_test)
        scores.append(iter_scores)

    scores_arr = np.array(scores)
    print("LDA, SVC (linear), SVC (rbf), Logistic regression (lbfgs), Random forest")
    print(scores_arr)

def part_5_rewritten(data: dataloader.Data):
    """
    Implement problem5 in task 1.
    Anaconda has very old versions of scikit, so again, I made this function
    separate from original "part5()".
    The code is almost same as part_4d, but now there's more classifiers.
    """
    import alpi.tools.feature_extraction as feature_extraction
    import warnings
    import time
    warnings.simplefilter("ignore")

    clfs = [
        ("LDA", sklearn.discriminant_analysis
            .LinearDiscriminantAnalysis()),
        ("svm.SVC linear:", sklearn.svm
            .SVC(kernel="linear")),  # v0.20: gamma="scale"
        ("svm.SVC rbf:", sklearn.svm
            .SVC(kernel="rbf")),  # v0.20: gamma="scale"
        ("LogisticRegression:", sklearn.linear_model
            .LogisticRegression(solver="lbfgs")),  # v0.20: multi_class="auto"
        ("RandomForestClassifier:", sklearn.ensemble
            .RandomForestClassifier(n_estimators=100)),
    ]
    feature_extractors = [
        ("Reshaped data", feature_extraction.do_nothing),
        ("Means", features.means),
        ("Means & stdevs", features.means_stdevs),
        #("orientation in euler angles",
        #    feature_extraction.orientation_to_eluer_angles),
        ("fft of all", feature_extraction.fft),
        ("spectrogram of all", feature_extraction.spectrogram),
        ("integral of lin.acc.", feature_extraction.integral_of_accerelation),
        #("norms of ang.vel. & lin.acc.",
        #    feature_extraction.norms_of_angvel_and_acc),
    ]
    print("\n5.")
    for clf_name, clf in clfs:
        for i, (feature_name, feat) in enumerate(feature_extractors):
            start = time.time()
            mean, std = classify_features(data, clf, feat)
            end = time.time()
            if i == 0:
                print("    {}".format(clf_name))
            print("        {} : {:.3f}({:.3f}), {:.1f} s".format(feature_name, mean,
                                                           std, end-start))


def main():
    data = dataloader.train_data()
    part4(data)
    part_4d(data)
    #part5(data)
    part_5_rewritten(data)


if __name__ == "__main__":
    main()
