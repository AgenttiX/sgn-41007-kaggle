import typing as tp
import numpy as np

import sklearn.model_selection

from task1 import dataloader


def split_by_group(data: dataloader.Data, n_splits: int = 1, test_size: float = 0.2) \
        -> tp.List[tp.Tuple[np.ndarray, np.ndarray]]:
    """Split samples in the data by their groups

    "Assign all samples of each block to train, validation or test set"
    Similar to sklearn.model_selection.GroupKFold but with splitting of
    individual groups if the label has only one group.
    Returns a list of tuples (sample_ids_train, sample_ids_test)
    """
    ids_by_group = data.sample_ids_by_group()
    # groups_list = list(ids_by_group.keys())

    unique_groups = data.unique_groups()    # The groups that have to be split
    assignable_groups = set(data.groups_dict().keys()) - unique_groups

    if not assignable_groups.isdisjoint(unique_groups):
        raise RuntimeError("Individually splittable groups should not be included in the primary splitting")

    assignable_groups_list = list(assignable_groups)

    splits = []

    # Handle those groups the labels of which have several groups
    splitter = sklearn.model_selection.ShuffleSplit(n_splits=n_splits, test_size=test_size)

    for group_inds_train, group_inds_test in splitter.split(assignable_groups_list):
        samples_train = set()
        samples_test = set()
        for group_ind in group_inds_train:
            samples_train.update(ids_by_group[assignable_groups_list[group_ind]])

        for group_ind in group_inds_test:
            samples_test.update(ids_by_group[assignable_groups_list[group_ind]])

        if not samples_train.isdisjoint(samples_test):
            raise RuntimeError("There should not be same samples in training and testing:", samples_train.intersection(samples_test))

        splits.append((samples_train, samples_test))

    # Handle those groups the labels of which have only one group
    unique_splitter = sklearn.model_selection.ShuffleSplit(n_splits=n_splits, test_size=test_size)

    for group in unique_groups:
        samples_of_group = list(ids_by_group[group])

        for i, (sample_inds_train, sample_inds_test) in enumerate(unique_splitter.split(samples_of_group)):
            for sample_ind in sample_inds_train:
                splits[i][0].add(samples_of_group[sample_ind])
            for sample_ind in sample_inds_test:
                splits[i][1].add(samples_of_group[sample_ind])

    split_arrays = []

    for samples_train, samples_test in splits:
        if not samples_train.isdisjoint(samples_test):
            raise RuntimeError("There should not be same samples in training and testing:", samples_train.intersection(samples_test))

        samples_train_arr = np.array(list(samples_train))
        samples_test_arr = np.array(list(samples_test))

        np.random.shuffle(samples_train_arr)
        np.random.shuffle(samples_test_arr)

        if samples_train_arr.size + samples_test_arr.size != data.x_data.shape[0]:
            raise RuntimeError("Training + test sample count should match the total sample count")

        split_arrays.append((samples_train_arr, samples_test_arr))

    return split_arrays


if __name__ == "__main__":
    data = dataloader.train_data()

    import time
    start_time = time.perf_counter()
    splits = split_by_group(data, n_splits=10)
    print(time.perf_counter() - start_time)
    print()

    for split in splits:
        # print(split)
        print(split[0].size, split[1].size, split[0].size + split[1].size)
        print(split[0].size / data.x_data.shape[0], split[1].size / data.x_data.shape[0])
        print()
