import typing as tp

import numpy as np

def rnd_forest_and_spectrogram():
    """
    Utilize best found classifier feature combo: random forest and
    spectrogram.
    """
    import sklearn.ensemble
    import alpi.tools.feature_extraction as feature_extraction
    from alpi.tools.read_data import get_data_and_labels
    from task1 import splitting, dataloader, features

    # Train data
    train_X, train_y, _, class_names = get_data_and_labels(
        data_path="../data/X_train_kaggle.npy",
        groups_csv_path="../data/groups.csv")
    #data = dataloader.train_data()
    #train_X1 = data.x_data
    #train_y1 = data.labels

    # Best found combo: random forest and spectrogram
    clf = sklearn.ensemble.RandomForestClassifier(n_estimators=100)
    feat = feature_extraction.spectrogram

    train_features = feat(train_X)
    train_flat = train_features.reshape(train_features.shape[0], -1)
    clf.fit(train_flat, train_y)

    test_X = np.load("../data/X_test_kaggle.npy")
    test_features = feat(test_X)
    test_flat = test_features.reshape(test_features.shape[0], -1)

    test_pred = clf.predict(test_flat)

    create_submission("sub_task1_alpi2_randomforest_spectrogram_v2.csv",
                      test_pred, class_names)

    # Score with data loader: 0.02579, score with get_data_and_labels 0.60844


def create_submission(
        path: str,
        y_labels: np.ndarray,
        label_names: tp.List[str]=None) -> None:
    """
    Create a submission file from the given y values.

    Note, label_names must be in correct order! Default order is the occurence
    oreder in groups.csv, which is what 'get_data_and_labels()' returns.

    If you use dataloader(), please use its own label encoder inversion function
    instead!

    :param path:        e.g. "submission.py"
    :param y_labels:    array of predicted labels, e.g. [5,4,7,4,2,5,1...]
    :param label_names: List of corresponding label names.
    :return:
    """

    if label_names is None:
        label_names = ('soft_pvc', 'concrete', 'hard_tiles_large_space',
                       'carpet', 'fine_concrete', 'soft_tiles', 'tiled',
                       'hard_tiles', 'wood')

    with open(path, "w") as fp:
        fp.write("# Id,Surface\n")
        for i, label in enumerate(y_labels):
            fp.write("%d,%s\n" % (i, label_names[label]))

def main():
    rnd_forest_and_spectrogram()


if __name__ == "__main__":
    main()