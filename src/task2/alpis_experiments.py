def load_data(splitmethod = "mikas_splitter"):
    """
    :param splitmethod: "mikas_splitter" / "raw"
    :return:
    """

    from task1 import dataloader
    from mika.splitting import split_by_group
    from keras.utils import to_categorical
    from sklearn.model_selection import train_test_split


    data = dataloader.train_data()
    X = data.x_data
    y = data.labels
    #X = X[:,:,:,np.newaxis]

    y = to_categorical(y, num_classes=9)  # dtype='float32'

    if splitmethod == "raw":
        #fold = KFold(n_splits=4,random_state=123).split(X=X,y=y)
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2)
    elif splitmethod == "mikas_splitter":
        fold = split_by_group(data, n_splits=4)
        train_inds, test_inds = fold[0]
        X_train = X[train_inds, ...]
        y_train = y[train_inds, ...]
        X_test = X[test_inds, ...]
        y_test = y[test_inds, ...]
    else:
        assert False

    return X_train, X_test, y_train, y_test


def stackoverflow_example_with_our_data():
    # https://stats.stackexchange.com/questions/274478/understanding-input-shape-parameter-in-lstm-with-keras
    from keras.models import Sequential
    from keras.layers import LSTM, Dense, Dropout
    import numpy as np
    from task1.submission import create_submission
    from task1.dataloader import test_data



    X_train, X_test, y_train, y_test = load_data()
    X_train = np.swapaxes(X_train, 1, 2)
    X_test = np.swapaxes(X_test, 1, 2)

    #X_train = X_train[:, :, 4:]
    #X_test = X_test[:, :, 4:]
    print(X_train.shape)

    data_dim = X_train.shape[2]
    timesteps = X_train.shape[1]
    num_classes = 9

    # expected input data shape: (batch_size, timesteps, data_dim)
    model = Sequential()
    model.add(LSTM(50, return_sequences=False,
                   input_shape=(timesteps, data_dim)))  # returns a sequence of vectors of dimension 32
    model.add(Dropout(0.5))

    #model.add(LSTM(40, return_sequences=True, kernel_regularizer='l1'))  # returns a sequence of vectors of dimension 32
    #model.add(Dropout(0.2))

    #model.add(LSTM(20, return_sequences=False, kernel_regularizer='l1'))  # returns a sequence of vectors of dimension 32
    #model.add(Dropout(0.2))

    model.add(Dense(200, activation='sigmoid'))
    model.add(Dropout(0.5))

    model.add(Dense(200, activation='sigmoid'))
    model.add(Dropout(0.5))

    model.add(Dense(num_classes, activation='softmax'))
    model.compile(loss='categorical_crossentropy',
                  optimizer='rmsprop',
                  metrics=['accuracy'])

    model.fit(X_train, y_train,
              batch_size=30, epochs=100,
              validation_data=(X_test, y_test))

    scores_real = model.evaluate(X_test, y_test, verbose=1)
    for i, name in enumerate(model.metrics_names):
        print("    Scores {}".format(name))
        print("        (real)   {:.3f}, ".format(scores_real[i]))
    model.save('testmodel.h5')
    X_real_test = test_data().x_data
    print(X_real_test.shape)
    X_real_test = np.swapaxes(X_real_test, 1, 2)
    print(X_real_test.shape)
    y_pred = model.predict(X_real_test)
    y_pred = np.argmax(y_pred, axis=1)
    print(y_pred.shape)
    create_submission("testsub.csv", y_pred)


def main():
    stackoverflow_example_with_our_data()


if __name__ == "__main__":
    main()