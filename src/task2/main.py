
def load_data(split_test_groups=True, separate_groups=True):
    """
    :param splitmethod: "mikas_splitter" / "raw"
    :return:
    """

    from keras.utils import to_categorical
    from sklearn.model_selection import train_test_split

    from alpi.tools.read_data import get_data_and_labels
    from sklearn.model_selection import GroupKFold, KFold

    X, y, groups, class_names = get_data_and_labels(
        data_path="../data/X_train_kaggle.npy",
        groups_csv_path="../data/groups.csv")
    y = to_categorical(y, num_classes=9)
    if split_test_groups:
        if separate_groups:
            fold = GroupKFold(n_splits=4).split(X=X, y=y, groups=groups)
        else:
            fold = KFold(n_splits=4).split(X=X, y=y, groups=groups)
        train_ids, test_ids = next(fold)

        X_train = X[train_ids]
        X_test = X[test_ids]
        y_train = y[train_ids]
        y_test = y[test_ids]

    else:
        X_train = X
        y_train = y
        X_test = None
        y_test = None

    return X_train, X_test, y_train, y_test, class_names

def model_by_joonas(net_name, reg=1e-10, test_split=True):
    # https://stats.stackexchange.com/questions/274478/understanding-input-shape-parameter-in-lstm-with-keras
    from keras.models import Sequential, load_model

    from keras.layers import LSTM, Dense, Dropout, Conv1D
    from keras import regularizers
    import numpy as np
    from os import path
    from sklearn.metrics import accuracy_score

    X_train, X_test, y_train, y_test, class_names = load_data(
            split_test_groups=test_split)
    X_train = np.swapaxes(X_train, 1, 2)
    X_train = X_train[:, :, 4:]

    if test_split:
        X_test = np.swapaxes(X_test, 1, 2)
        X_test = X_test[:, :, 4:]

    #print(X_train.shape)
    if path.isfile(net_name + '.h5'):
        print("Model already trained!")
        model = load_model(net_name+".h5")
    else:

        data_dim = X_train.shape[2]
        timesteps = X_train.shape[1]
        num_classes = 9

        # expected input data shape: (batch_size, timesteps, data_dim)
        model = Sequential()

        model.add(Conv1D(20, kernel_size=20, input_shape=(timesteps, data_dim), kernel_regularizer=regularizers.l1(reg)))
        model.add(LSTM(100, return_sequences=False, kernel_regularizer=regularizers.l1(reg),
                       ))  # returns a sequence of vectors of dimension 32

        model.add(Dense(200, activation='sigmoid', kernel_regularizer=regularizers.l1(reg)))

        model.add(Dense(100, activation='sigmoid', kernel_regularizer=regularizers.l1(reg)))

        model.add(Dense(num_classes, activation='softmax'))
        model.compile(loss='categorical_crossentropy',
                      optimizer='rmsprop',
                      metrics=['accuracy'])

        if test_split:
            validation_data = (X_test, y_test)
        else:
            validation_data = None

        model.fit(X_train, y_train,
                  batch_size=30, epochs=50,
                  validation_data=validation_data)

    if test_split:
        scores_real = model.evaluate(X_test, y_test, verbose=1)
        for i, name in enumerate(model.metrics_names):
            print("    Score model {} {:.3f}".format(name, scores_real[i]))

        #scores_real = model.evaluate(X_test, y_test, verbose=1)
        y_pred = model.predict(X_test)
        y_pred_labels = np.argmax(y_pred, axis=1)
        y_test_labels = np.argmax(y_test, axis=1)
        score = accuracy_score(y_pred_labels, y_test_labels)
        print("    Score raw labels {:.3f}".format(score))

    model.save(net_name + '.h5')

def run_trained_net(net_name):
    import numpy as np
    from keras.models import load_model
    from task1.submission import create_submission

    from alpi.tools.read_data import get_data_and_labels

    _, _, _, class_names = get_data_and_labels(
        data_path="../data/X_train_kaggle.npy",
        groups_csv_path="../data/groups.csv")


    model = load_model(net_name+".h5")

    X_real_test = np.load("../data/X_test_kaggle.npy")
    #print(X_real_test.shape)
    X_real_test = np.swapaxes(X_real_test, 1, 2)
    X_real_test = X_real_test[:, :, 4:]
    #print(X_real_test.shape)
    y_pred = model.predict(X_real_test)
    y_pred = np.argmax(y_pred, axis=1)
    #print(y_pred.shape)
    create_submission(net_name+".csv", y_pred, class_names)

def main():

    # Task 2a
    model_by_joonas("model_reg_0.00", 0.00)
    run_trained_net("model_reg_0.00")

    # Task 2b
    model_by_joonas("model_reg_1e-6", 1e-6)
    run_trained_net("model_reg_1e-6")
    model_by_joonas("model_reg_1e-10", 1e-10)
    run_trained_net("model_reg_1e-10")

    # Task 2c
    model_by_joonas("model_reg_1e-10_final", 1e-10, test_split=False)
    run_trained_net("model_reg_1e-10_final")

if __name__ == "__main__":
    main()
