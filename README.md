# sgn-41007-kaggle

Kaggle competition entry for Tampere University [of Technology] course SGN-41007 Pattern Recognition and Machine Learning, group 45

Tobias Halfar, Joonas Latukka, Mika Mäki & Alpi Tolvanen, 2019

[![pipeline status](https://gitlab.com/AgenttiX/sgn-41007-kaggle/badges/master/pipeline.svg)](https://gitlab.com/AgenttiX/sgn-41007-kaggle/commits/master)
[![coverage report](https://gitlab.com/AgenttiX/sgn-41007-kaggle/badges/master/coverage.svg)](https://gitlab.com/AgenttiX/sgn-41007-kaggle/commits/master)
